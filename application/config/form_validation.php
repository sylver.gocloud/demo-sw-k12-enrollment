<?php


$config = array(
    'enrollment_profile_preschool' => array(
	array(
		'field' => 'child_fname',
		'label' => 'Child First name',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'child_lname',
		'label' => 'Child Last Name',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'child_mname',
		'label' => 'Childs Middle Name',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	), 
	array(
		'field' => 'child_nickname',
		'label' => 'Childs Nickname',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'b_month',
		'label' => 'Birth month',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),
		array(
		'field' => 'b_day',
		'label' => 'Day Of Birth',
		'rules' => 'required|datechecker_2|trim|htmlspecialchars|strip_tags'
	),  
		array(
		'field' => 'b_year',
		'label' => 'Year of Birth',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	), 
	array(
		'field' => 'gender',
		'label' => 'Childs Gender',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_placeofbirth',
		'label' => 'Child\'s Place of Birth',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_nationality',
		'label' => 'Child\'s Nationality',
		'rules' => 'text_all|required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_religous',
		'label' => 'Religous Affilation',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_city_address',
		'label' => 'Childs City Address',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_telno',
		'label' => 'Contact Number',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'email',
		'label' => 'Email Address',
		'rules' => 'required|valid_email|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_adresshowlong',
		'label' => 'How Long Has Child Been Living',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'school_last_attended',
		'label' => 'School Last Attended',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'school_level_completed',
		'label' => 'School Level Completed',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'school_address',
		'label' => 'School Address',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_name',
		'label' => 'Father\'s Name',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_age',
		'label' => 'Father\'s Age',
		'rules' => 'required|numeric|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'father_relaffil',
		'label' => 'Father\'s Religous Affilation',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),	
	array(
		'field' => 'father_citizenship',
		'label' => 'Father\'s Citizenship',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_educ',
		'label' => 'Father\'s Educational Attainment',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	), 
	array(
		'field' => 'father_talent',
		'label' => 'Father\'s Hobies and Talent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_occup',
		'label' => 'Father\'s Occupation',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_office_address',
		'label' => 'Father\'s Office Address',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_office_tel',
		'label' => 'Father\'s Office Tel',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_name',
		'label' => 'Mother\'s Name',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'mother_age',
		'label' => 'Mother\'s Age',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	), 
	array(
		'field' => 'mother_relaffil',
		'label' => 'Mother\'s Religous Affilation',
		'rules' => 'text_all|required|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'mother_citizenship',
		'label' => 'Mother\'s Citizenship',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_educ',
		'label' => 'Mother\'s Educational Attainment',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_talent',
		'label' => 'Mother\'s Hobbies and Talent',
		'rules' => 'trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_occup',
		'label' => 'Mother\'s Occupation',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'mother_office_address',
		'label' => 'Mother\'s Office Address',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_office_tel',
		'label' => 'Mother\'s Office Tel',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'guardian_name',
		'label' => 'Guardians Name',
		'rules' => 'required|required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'guardian_relation',
		'label' => 'Guardian Relation',
		'rules' => 'required|required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'guardian_address',
		'label' => 'Guardian\'s Address',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'guardian_contact_num',
		'label' => 'Guardian\'s Contact Number',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'guardian_reason',
		'label' => 'Reason For Having Guardian',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'parent_status',
		'label' => 'Parent\'s Marriage Status',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'status_how_long',
		'label' => 'For how long',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'right_over_child',
		'label' => 'Right over Child',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'position_of_child',
		'label' => 'Child\'s Position',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'is_child_adopted',
		'label' => 'Is Child Adopted',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'age_of_adoption',
		'label' => 'Age Of Adpoption',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_aware_adopted',
		'label' => 'Child Aware Adopted',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_presently_pregnant',
		'label' => 'Is Mother Presently Pregnant',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'mother_pregnancy_due_date',
		'label' => 'Mother\'s Pregnance Due Date',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_deaths',
		'label' => 'Family Deaths',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_deaths_relation',
		'label' => 'Relation',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_accidents',
		'label' => 'Family Related Accidents',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_accidents_relation',
		'label' => 'Relation to Family Accidents',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'what',
		'label' => 'what',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'when',
		'label' => 'when',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers1',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers2',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers3',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'hhmembers4',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers5',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers6',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage1',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage2',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'hhmembersage3',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage4',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage5',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage6',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'language_at_home',
		'label' => 'Languages Spoken at Home',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'family_activities',
		'label' => 'Family Activities',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_activities_frequent',
		'label' => 'Family Activities',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'tv_time',
		'label' => 'TV Time Spent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'tv_whom',
		'label' => 'TV Spen with Whom',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'radio_time',
		'label' => 'Radio Time Spent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'radio_whom',
		'label' => 'Radio Time Spent With Whom',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'computergames_time',
		'label' => 'Computer Games Time Spent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'computergames_whom',
		'label' => 'Computer Games Time Spent with Whom',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_responsibilities',
		'label' => 'Child Responsibilites',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_responsibilities_what',
		'label' => 'What are you\'re Child Responsibilites',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'child_play_group',
		'label' => 'Child Play Group',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_play_group_frequent',
		'label' => 'Child Frequent Playgroup Frequent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'other_interest',
		'label' => 'Other Interest',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'age_first_walked',
		'label' => 'Age First Walked',
		'rules' => 'required|numeric|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'age_first_talked',
		'label' => 'Age First Talked',
		'rules' => 'required|numeric|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'w_for_urinate',
		'label' => 'Word used for urination',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'w_for_bowel',
		'label' => 'Word used for bowel movement',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'w_usual_time',
		'label' => 'Usual Time',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'child_dress',
		'label' => 'Child Dress Self',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'child_undress',
		'label' => 'Child Undress Self',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'hand_orientation',
		'label' => 'Hand Orientation',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'self_feed',
		'label' => 'Child Self Feed',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'stayinplace',
		'label' => 'Stay In Place',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'joinothereating',
		'label' => 'Join Family when eating',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'food_preferences',
		'label' => 'Food Preferences',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'drink_from_bottle',
		'label' => 'Drink From Bottle',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'toilet_trained',
		'label' => 'Toilet Trained',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	)	
    ),    
	
	/*+-------------------------------------------------+
	* | Enrollment form validation for secondary        |
	* +-------------------------------------------------+
	*/
	
	'enrollment_profile_gradeschool' => array(
	array(
		'field' => 'child_fname',
		'label' => 'Child First name',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'child_lname',
		'label' => 'Child Last Name',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'child_mname',
		'label' => 'Childs Middle Name',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	), 
	array(
		'field' => 'child_nickname',
		'label' => 'Childs Nickname',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'b_month',
		'label' => 'Birth month',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),
		array(
		'field' => 'b_day',
		'label' => 'Day Of Birth',
		'rules' => 'required|datechecker_2|trim|htmlspecialchars|strip_tags'
	),  
		array(
		'field' => 'b_year',
		'label' => 'Year of Birth',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	), 
	array(
		'field' => 'gender',
		'label' => 'Childs Gender',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_placeofbirth',
		'label' => 'Child\'s Place of Birth',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_nationality',
		'label' => 'Child\'s Nationality',
		'rules' => 'text_all|required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_religous',
		'label' => 'Religous Affilation',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_city_address',
		'label' => 'Childs City Address',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_telno',
		'label' => 'Contact Number',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'email',
		'label' => 'Email Address',
		'rules' => 'required|valid_email|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_adresshowlong',
		'label' => 'How Long Has Child Been Living',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'school_last_attended',
		'label' => 'School Last Attended',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'school_level_completed',
		'label' => 'School Level Completed',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'school_address',
		'label' => 'School Address',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_name',
		'label' => 'Father\'s Name',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_age',
		'label' => 'Father\'s Age',
		'rules' => 'required|numeric|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'father_relaffil',
		'label' => 'Father\'s Religous Affilation',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),	
	array(
		'field' => 'father_citizenship',
		'label' => 'Father\'s Citizenship',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_educ',
		'label' => 'Father\'s Educational Attainment',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	), 
	array(
		'field' => 'father_talent',
		'label' => 'Father\'s Hobies and Talent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_occup',
		'label' => 'Father\'s Occupation',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_office_address',
		'label' => 'Father\'s Office Address',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_office_tel',
		'label' => 'Father\'s Office Tel',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_name',
		'label' => 'Mother\'s Name',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'mother_age',
		'label' => 'Mother\'s Age',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	), 
	array(
		'field' => 'mother_relaffil',
		'label' => 'Mother\'s Religous Affilation',
		'rules' => 'text_all|required|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'mother_citizenship',
		'label' => 'Mother\'s Citizenship',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_educ',
		'label' => 'Mother\'s Educational Attainment',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_talent',
		'label' => 'Mother\'s Hobbies and Talent',
		'rules' => 'trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_occup',
		'label' => 'Mother\'s Occupation',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'mother_office_address',
		'label' => 'Mother\'s Office Address',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_office_tel',
		'label' => 'Mother\'s Office Tel',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'guardian_name',
		'label' => 'Guardians Name',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'guardian_relation',
		'label' => 'Guardian Relation',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'guardian_address',
		'label' => 'Guardian\'s Address',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'guardian_contact_num',
		'label' => 'Guardian\'s Contact Number',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'guardian_reason',
		'label' => 'Reason For Having Guardian',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'parent_status',
		'label' => 'Parent\'s Marriage Status',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'status_how_long',
		'label' => 'For how long',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'right_over_child',
		'label' => 'Right over Child',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'position_of_child',
		'label' => 'Child\'s Position',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'is_child_adopted',
		'label' => 'Is Child Adopted',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'age_of_adoption',
		'label' => 'Age Of Adpoption',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_aware_adopted',
		'label' => 'Child Aware Adopted',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_presently_pregnant',
		'label' => 'Is Mother Presently Pregnant',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'mother_pregnancy_due_date',
		'label' => 'Mother\'s Pregnance Due Date',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_deaths',
		'label' => 'Family Deaths',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_deaths_relation',
		'label' => 'Relation',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_accidents',
		'label' => 'Family Related Accidents',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_accidents_relation',
		'label' => 'Relation to Family Accidents',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'what',
		'label' => 'what',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'when',
		'label' => 'when',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers1',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers2',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers3',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'hhmembers4',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers5',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers6',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage1',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage2',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'hhmembersage3',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage4',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage5',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage6',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'language_at_home',
		'label' => 'Languages Spoken at Home',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'family_activities',
		'label' => 'Family Activities',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_activities_frequent',
		'label' => 'Family Activities',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'tv_time',
		'label' => 'TV Time Spent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'tv_whom',
		'label' => 'TV Spen with Whom',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'radio_time',
		'label' => 'Radio Time Spent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'radio_whom',
		'label' => 'Radio Time Spent With Whom',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'computergames_time',
		'label' => 'Computer Games Time Spent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'computergames_whom',
		'label' => 'Computer Games Time Spent with Whom',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_responsibilities',
		'label' => 'Child Responsibilites',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_responsibilities_what',
		'label' => 'What are you\'re Child Responsibilites',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'child_play_group',
		'label' => 'Child Play Group',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_play_group_frequent',
		'label' => 'Child Frequent Playgroup Frequent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'other_interest',
		'label' => 'Other Interest',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'sec_study_time',
		'label' => 'Study time',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
		array(
		'field' => 'sec_study_whom',
		'label' => 'With whom',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
		array(
		'field' => 'sec_child_pastime',
		'label' => 'Child Pastime',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
		array(
		'field' => 'sec_child_htd',
		'label' => 'Child Hate to Do',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
		array(
		'field' => 'sec_child_music',
		'label' => 'Child Music',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
		array(
		'field' => 'sec_child_hfrus',
		'label' => 'Handle frustration',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
		array(
		'field' => 'sec_child_curfew',
		'label' => 'Child Curfew',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
		array(
		'field' => 'sec_misbehave_others',
		'label' => 'Misbehave Others',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
		array(
		'field' => 'sec_rhle',
		'label' => 'House Rule to reinforce',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
		array(
		'field' => 'al_tghome',
		'label' => 'Allow to go home independently',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	)
    ),
	
	/*+-------------------------------------------------+
	* | Enrollment form validation for intermediate     |
	* +-------------------------------------------------+
	*/
	
	'enrollment_profile_highschool' => array(
		array(
		'field' => 'child_fname',
		'label' => 'Child First name',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'child_lname',
		'label' => 'Child Last Name',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'child_mname',
		'label' => 'Childs Middle Name',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	), 
	array(
		'field' => 'child_nickname',
		'label' => 'Childs Nickname',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'b_month',
		'label' => 'Birth month',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),
		array(
		'field' => 'b_day',
		'label' => 'Day Of Birth',
		'rules' => 'required|datechecker_2|trim|htmlspecialchars|strip_tags'
	),  
		array(
		'field' => 'b_year',
		'label' => 'Year of Birth',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	), 
	array(
		'field' => 'gender',
		'label' => 'Childs Gender',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_placeofbirth',
		'label' => 'Child\'s Place of Birth',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_nationality',
		'label' => 'Child\'s Nationality',
		'rules' => 'text_all|required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_religous',
		'label' => 'Religous Affilation',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_city_address',
		'label' => 'Childs City Address',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_telno',
		'label' => 'Contact Number',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'email',
		'label' => 'Email Address',
		'rules' => 'required|valid_email|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_adresshowlong',
		'label' => 'How Long Has Child Been Living',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'school_last_attended',
		'label' => 'School Last Attended',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'school_level_completed',
		'label' => 'School Level Completed',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'school_address',
		'label' => 'School Address',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_name',
		'label' => 'Father\'s Name',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_age',
		'label' => 'Father\'s Age',
		'rules' => 'required|numeric|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'father_relaffil',
		'label' => 'Father\'s Religous Affilation',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),	
	array(
		'field' => 'father_citizenship',
		'label' => 'Father\'s Citizenship',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_educ',
		'label' => 'Father\'s Educational Attainment',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	), 
	array(
		'field' => 'father_talent',
		'label' => 'Father\'s Hobies and Talent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_occup',
		'label' => 'Father\'s Occupation',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_office_address',
		'label' => 'Father\'s Office Address',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_office_tel',
		'label' => 'Father\'s Office Tel',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_name',
		'label' => 'Mother\'s Name',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'mother_age',
		'label' => 'Mother\'s Age',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	), 
	array(
		'field' => 'mother_relaffil',
		'label' => 'Mother\'s Religous Affilation',
		'rules' => 'text_all|required|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'mother_citizenship',
		'label' => 'Mother\'s Citizenship',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_educ',
		'label' => 'Mother\'s Educational Attainment',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_talent',
		'label' => 'Mother\'s Hobbies and Talent',
		'rules' => 'trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_occup',
		'label' => 'Mother\'s Occupation',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'mother_office_address',
		'label' => 'Mother\'s Office Address',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_office_tel',
		'label' => 'Mother\'s Office Tel',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'guardian_name',
		'label' => 'Guardians Name',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'guardian_relation',
		'label' => 'Guardian Relation',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'guardian_address',
		'label' => 'Guardian\'s Address',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'guardian_contact_num',
		'label' => 'Guardian\'s Contact Number',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'guardian_reason',
		'label' => 'Reason For Having Guardian',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'parent_status',
		'label' => 'Parent\'s Marriage Status',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'status_how_long',
		'label' => 'For how long',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'right_over_child',
		'label' => 'Right over Child',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'position_of_child',
		'label' => 'Child\'s Position',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'is_child_adopted',
		'label' => 'Is Child Adopted',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'age_of_adoption',
		'label' => 'Age Of Adpoption',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_aware_adopted',
		'label' => 'Child Aware Adopted',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_presently_pregnant',
		'label' => 'Is Mother Presently Pregnant',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'mother_pregnancy_due_date',
		'label' => 'Mother\'s Pregnance Due Date',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_deaths',
		'label' => 'Family Deaths',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_deaths_relation',
		'label' => 'Relation',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_accidents',
		'label' => 'Family Related Accidents',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_accidents_relation',
		'label' => 'Relation to Family Accidents',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'what',
		'label' => 'what',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'when',
		'label' => 'when',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers1',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers2',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers3',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'hhmembers4',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers5',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers6',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage1',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage2',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'hhmembersage3',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage4',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage5',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage6',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'language_at_home',
		'label' => 'Languages Spoken at Home',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'family_activities',
		'label' => 'Family Activities',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_activities_frequent',
		'label' => 'Family Activities',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'tv_time',
		'label' => 'TV Time Spent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'tv_whom',
		'label' => 'TV Spen with Whom',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'radio_time',
		'label' => 'Radio Time Spent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'radio_whom',
		'label' => 'Radio Time Spent With Whom',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'computergames_time',
		'label' => 'Computer Games Time Spent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'computergames_whom',
		'label' => 'Computer Games Time Spent with Whom',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_responsibilities',
		'label' => 'Child Responsibilites',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_responsibilities_what',
		'label' => 'What are you\'re Child Responsibilites',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'child_play_group',
		'label' => 'Child Play Group',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_play_group_frequent',
		'label' => 'Child Frequent Playgroup Frequent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'other_interest',
		'label' => 'Other Interest',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
			'field' => 'hs_watch',
			'label' => '',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
			'field' => 'hs_boardgames',
			'label' => '',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
			'field' => 'hs_comp',
			'label' => '',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
			'field' => 'hs_reading',
			'label' => '',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
			'field' => 'hs_other_pt',
			'label' => '',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
			'field' => 'hs_spend_study',
			'label' => '',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
			'field' => 'hs_first_mens',
			'label' => '',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
			'field' => 'hs_signal_mens',
			'label' => '',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'al_tghome',
		'label' => 'Allow to go home independently',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	)
    ),
	
	/*+-------------------------------------------------+
	* | Validation rules for Registration default		  |
	* +-------------------------------------------------+
	*/
	'enrollment_profile' => array(
			array(
		'field' => 'child_fname',
		'label' => 'Child First name',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'child_lname',
		'label' => 'Child Last Name',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'child_mname',
		'label' => 'Childs Middle Name',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	), 
	array(
		'field' => 'child_nickname',
		'label' => 'Childs Nickname',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'b_month',
		'label' => 'Birth month',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),
		array(
		'field' => 'b_day',
		'label' => 'Day Of Birth',
		'rules' => 'required|datechecker_2|trim|htmlspecialchars|strip_tags'
	),  
		array(
		'field' => 'b_year',
		'label' => 'Year of Birth',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'gender',
		'label' => 'Childs Gender',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_placeofbirth',
		'label' => 'Child\'s Place of Birth',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_nationality',
		'label' => 'Child\'s Nationality',
		'rules' => 'text_all|required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_religous',
		'label' => 'Religous Affilation',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_city_address',
		'label' => 'Childs City Address',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_telno',
		'label' => 'Contact Number',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'email',
		'label' => 'Email Address',
		'rules' => 'required|valid_email|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_adresshowlong',
		'label' => 'How Long Has Child Been Living',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'school_last_attended',
		'label' => 'School Last Attended',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'school_level_completed',
		'label' => 'School Level Completed',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'school_address',
		'label' => 'School Address',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_name',
		'label' => 'Father\'s Name',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_age',
		'label' => 'Father\'s Age',
		'rules' => 'required|numeric|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'father_relaffil',
		'label' => 'Father\'s Religous Affilation',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),	
	array(
		'field' => 'father_citizenship',
		'label' => 'Father\'s Citizenship',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_educ',
		'label' => 'Father\'s Educational Attainment',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	), 
	array(
		'field' => 'father_talent',
		'label' => 'Father\'s Hobies and Talent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_occup',
		'label' => 'Father\'s Occupation',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_office_address',
		'label' => 'Father\'s Office Address',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_office_tel',
		'label' => 'Father\'s Office Tel',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_name',
		'label' => 'Mother\'s Name',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'mother_age',
		'label' => 'Mother\'s Age',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	), 
	array(
		'field' => 'mother_relaffil',
		'label' => 'Mother\'s Religous Affilation',
		'rules' => 'text_all|required|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'mother_citizenship',
		'label' => 'Mother\'s Citizenship',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_educ',
		'label' => 'Mother\'s Educational Attainment',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_talent',
		'label' => 'Mother\'s Hobbies and Talent',
		'rules' => 'trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_occup',
		'label' => 'Mother\'s Occupation',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'mother_office_address',
		'label' => 'Mother\'s Office Address',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_office_tel',
		'label' => 'Mother\'s Office Tel',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'guardian_name',
		'label' => 'Guardians Name',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'guardian_relation',
		'label' => 'Guardian Relation',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'guardian_address',
		'label' => 'Guardian\'s Address',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'guardian_contact_num',
		'label' => 'Guardian\'s Contact Number',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'guardian_reason',
		'label' => 'Reason For Having Guardian',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'parent_status',
		'label' => 'Parent\'s Marriage Status',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'status_how_long',
		'label' => 'For how long',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'right_over_child',
		'label' => 'Right over Child',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'position_of_child',
		'label' => 'Child\'s Position',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'is_child_adopted',
		'label' => 'Is Child Adopted',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'age_of_adoption',
		'label' => 'Age Of Adpoption',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_aware_adopted',
		'label' => 'Child Aware Adopted',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_presently_pregnant',
		'label' => 'Is Mother Presently Pregnant',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'mother_pregnancy_due_date',
		'label' => 'Mother\'s Pregnance Due Date',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_deaths',
		'label' => 'Family Deaths',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_deaths_relation',
		'label' => 'Relation',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_accidents',
		'label' => 'Family Related Accidents',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_accidents_relation',
		'label' => 'Relation to Family Accidents',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'what',
		'label' => 'what',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'when',
		'label' => 'when',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers1',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers2',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers3',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'hhmembers4',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers5',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers6',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage1',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage2',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'hhmembersage3',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage4',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage5',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage6',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'language_at_home',
		'label' => 'Languages Spoken at Home',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'family_activities',
		'label' => 'Family Activities',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_activities_frequent',
		'label' => 'Family Activities',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'tv_time',
		'label' => 'TV Time Spent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'tv_whom',
		'label' => 'TV Spen with Whom',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'radio_time',
		'label' => 'Radio Time Spent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'radio_whom',
		'label' => 'Radio Time Spent With Whom',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'computergames_time',
		'label' => 'Computer Games Time Spent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'computergames_whom',
		'label' => 'Computer Games Time Spent with Whom',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_responsibilities',
		'label' => 'Child Responsibilites',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_responsibilities_what',
		'label' => 'What are you\'re Child Responsibilites',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'child_play_group',
		'label' => 'Child Play Group',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_play_group_frequent',
		'label' => 'Child Frequent Playgroup Frequent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'other_interest',
		'label' => 'Other Interest',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	)
	/*,  
	array(
		'field' => 'sibling[firstname][]',
		'label' => 'Sibling Firstname',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
		array(
		'field' => 'sibling[age][]',
		'label' => 'Sibling Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),
		array(
		'field' => 'sibling[lastname][]',
		'label' => 'Sibling Lastname',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	)*/
    ),
	
	/*+-------------------------------------------------+
	* | Validation rules for Registration default - email and name lang required		  |
	* +-------------------------------------------------+
	*/
	'enrollment_profile_simple' => array(
			array(
		'field' => 'child_fname',
		'label' => 'Child First name',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'child_lname',
		'label' => 'Child Last Name',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'child_mname',
		'label' => 'Childs Middle Name',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	), 
	array(
		'field' => 'child_nickname',
		'label' => 'Childs Nickname',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'b_month',
		'label' => 'Birth month',
		'rules' => 'trim|htmlspecialchars|strip_tags'
	),
		array(
		'field' => 'b_day',
		'label' => 'Day Of Birth',
		'rules' => 'datechecker_2|trim|htmlspecialchars|strip_tags'
	),  
		array(
		'field' => 'b_year',
		'label' => 'Year of Birth',
		'rules' => 'trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'gender',
		'label' => 'Childs Gender',
		'rules' => 'trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_placeofbirth',
		'label' => 'Child\'s Place of Birth',
		'rules' => 'trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_nationality',
		'label' => 'Child\'s Nationality',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_religous',
		'label' => 'Religous Affilation',
		'rules' => 'trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_city_address',
		'label' => 'Childs City Address',
		'rules' => 'trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_telno',
		'label' => 'Contact Number',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'email',
		'label' => 'Email Address',
		'rules' => 'required|valid_email|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_adresshowlong',
		'label' => 'How Long Has Child Been Living',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'school_last_attended',
		'label' => 'School Last Attended',
		'rules' => 'trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'school_level_completed',
		'label' => 'School Level Completed',
		'rules' => 'trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'school_address',
		'label' => 'School Address',
		'rules' => 'trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_name',
		'label' => 'Father\'s Name',
		'rules' => 'trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_age',
		'label' => 'Father\'s Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'father_relaffil',
		'label' => 'Father\'s Religous Affilation',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),	
	array(
		'field' => 'father_citizenship',
		'label' => 'Father\'s Citizenship',
		'rules' => 'trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_educ',
		'label' => 'Father\'s Educational Attainment',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	), 
	array(
		'field' => 'father_talent',
		'label' => 'Father\'s Hobies and Talent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_occup',
		'label' => 'Father\'s Occupation',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_office_address',
		'label' => 'Father\'s Office Address',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_office_tel',
		'label' => 'Father\'s Office Tel',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_name',
		'label' => 'Mother\'s Name',
		'rules' => 'trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'mother_age',
		'label' => 'Mother\'s Age',
		'rules' => 'trim|htmlspecialchars|strip_tags'
	), 
	array(
		'field' => 'mother_relaffil',
		'label' => 'Mother\'s Religous Affilation',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'mother_citizenship',
		'label' => 'Mother\'s Citizenship',
		'rules' => 'trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_educ',
		'label' => 'Mother\'s Educational Attainment',
		'rules' => 'trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_talent',
		'label' => 'Mother\'s Hobbies and Talent',
		'rules' => 'trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_occup',
		'label' => 'Mother\'s Occupation',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'mother_office_address',
		'label' => 'Mother\'s Office Address',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_office_tel',
		'label' => 'Mother\'s Office Tel',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'guardian_name',
		'label' => 'Guardians Name',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'guardian_relation',
		'label' => 'Guardian Relation',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'guardian_address',
		'label' => 'Guardian\'s Address',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'guardian_contact_num',
		'label' => 'Guardian\'s Contact Number',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'guardian_reason',
		'label' => 'Reason For Having Guardian',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'parent_status',
		'label' => 'Parent\'s Marriage Status',
		'rules' => 'trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'status_how_long',
		'label' => 'For how long',
		'rules' => 'trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'right_over_child',
		'label' => 'Right over Child',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'position_of_child',
		'label' => 'Child\'s Position',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'is_child_adopted',
		'label' => 'Is Child Adopted',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'age_of_adoption',
		'label' => 'Age Of Adpoption',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_aware_adopted',
		'label' => 'Child Aware Adopted',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_presently_pregnant',
		'label' => 'Is Mother Presently Pregnant',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'mother_pregnancy_due_date',
		'label' => 'Mother\'s Pregnance Due Date',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_deaths',
		'label' => 'Family Deaths',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_deaths_relation',
		'label' => 'Relation',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_accidents',
		'label' => 'Family Related Accidents',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_accidents_relation',
		'label' => 'Relation to Family Accidents',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'what',
		'label' => 'what',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'when',
		'label' => 'when',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers1',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers2',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers3',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'hhmembers4',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers5',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers6',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage1',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage2',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'hhmembersage3',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage4',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage5',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage6',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'language_at_home',
		'label' => 'Languages Spoken at Home',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'family_activities',
		'label' => 'Family Activities',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_activities_frequent',
		'label' => 'Family Activities',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'tv_time',
		'label' => 'TV Time Spent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'tv_whom',
		'label' => 'TV Spen with Whom',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'radio_time',
		'label' => 'Radio Time Spent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'radio_whom',
		'label' => 'Radio Time Spent With Whom',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'computergames_time',
		'label' => 'Computer Games Time Spent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'computergames_whom',
		'label' => 'Computer Games Time Spent with Whom',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_responsibilities',
		'label' => 'Child Responsibilites',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_responsibilities_what',
		'label' => 'What are you\'re Child Responsibilites',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'child_play_group',
		'label' => 'Child Play Group',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_play_group_frequent',
		'label' => 'Child Frequent Playgroup Frequent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'other_interest',
		'label' => 'Other Interest',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	)
	/*,  
	array(
		'field' => 'sibling[firstname][]',
		'label' => 'Sibling Firstname',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
		array(
		'field' => 'sibling[age][]',
		'label' => 'Sibling Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),
		array(
		'field' => 'sibling[lastname][]',
		'label' => 'Sibling Lastname',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	)*/
    ),
	
	/*====================================================================
		Enrollment validation form PRIMARY primary Primary
	=======================================================================*/
	'enrollment_profile_primary' => array(
			array(
		'field' => 'child_fname',
		'label' => 'Child First name',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'child_lname',
		'label' => 'Child Last Name',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'child_mname',
		'label' => 'Childs Middle Name',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	), 
	array(
		'field' => 'child_nickname',
		'label' => 'Childs Nickname',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'b_month',
		'label' => 'Birth month',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),
		array(
		'field' => 'b_day',
		'label' => 'Day Of Birth',
		'rules' => 'required|datechecker_2|trim|htmlspecialchars|strip_tags'
	),  
		array(
		'field' => 'b_year',
		'label' => 'Year of Birth',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'gender',
		'label' => 'Childs Gender',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_placeofbirth',
		'label' => 'Child\'s Place of Birth',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_nationality',
		'label' => 'Child\'s Nationality',
		'rules' => 'text_all|required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_religous',
		'label' => 'Religous Affilation',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_city_address',
		'label' => 'Childs City Address',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_telno',
		'label' => 'Contact Number',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'email',
		'label' => 'Email Address',
		'rules' => 'required|valid_email|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_adresshowlong',
		'label' => 'How Long Has Child Been Living',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'school_last_attended',
		'label' => 'School Last Attended',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'school_level_completed',
		'label' => 'School Level Completed',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'school_address',
		'label' => 'School Address',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_name',
		'label' => 'Father\'s Name',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_age',
		'label' => 'Father\'s Age',
		'rules' => 'required|numeric|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'father_relaffil',
		'label' => 'Father\'s Religous Affilation',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),	
	array(
		'field' => 'father_citizenship',
		'label' => 'Father\'s Citizenship',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_educ',
		'label' => 'Father\'s Educational Attainment',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	), 
	array(
		'field' => 'father_talent',
		'label' => 'Father\'s Hobies and Talent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_occup',
		'label' => 'Father\'s Occupation',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_office_address',
		'label' => 'Father\'s Office Address',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_office_tel',
		'label' => 'Father\'s Office Tel',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_name',
		'label' => 'Mother\'s Name',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'mother_age',
		'label' => 'Mother\'s Age',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	), 
	array(
		'field' => 'mother_relaffil',
		'label' => 'Mother\'s Religous Affilation',
		'rules' => 'text_all|required|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'mother_citizenship',
		'label' => 'Mother\'s Citizenship',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_educ',
		'label' => 'Mother\'s Educational Attainment',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_talent',
		'label' => 'Mother\'s Hobbies and Talent',
		'rules' => 'trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_occup',
		'label' => 'Mother\'s Occupation',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'mother_office_address',
		'label' => 'Mother\'s Office Address',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_office_tel',
		'label' => 'Mother\'s Office Tel',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'guardian_name',
		'label' => 'Guardians Name',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'guardian_relation',
		'label' => 'Guardian Relation',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'guardian_address',
		'label' => 'Guardian\'s Address',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'guardian_contact_num',
		'label' => 'Guardian\'s Contact Number',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'guardian_reason',
		'label' => 'Reason For Having Guardian',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'parent_status',
		'label' => 'Parent\'s Marriage Status',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'status_how_long',
		'label' => 'For how long',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'right_over_child',
		'label' => 'Right over Child',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'position_of_child',
		'label' => 'Child\'s Position',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'is_child_adopted',
		'label' => 'Is Child Adopted',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'age_of_adoption',
		'label' => 'Age Of Adpoption',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_aware_adopted',
		'label' => 'Child Aware Adopted',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_presently_pregnant',
		'label' => 'Is Mother Presently Pregnant',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'mother_pregnancy_due_date',
		'label' => 'Mother\'s Pregnance Due Date',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_deaths',
		'label' => 'Family Deaths',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_deaths_relation',
		'label' => 'Relation',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_accidents',
		'label' => 'Family Related Accidents',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_accidents_relation',
		'label' => 'Relation to Family Accidents',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'what',
		'label' => 'what',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'when',
		'label' => 'when',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers1',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers2',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers3',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'hhmembers4',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers5',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers6',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage1',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage2',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'hhmembersage3',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage4',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage5',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage6',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'language_at_home',
		'label' => 'Languages Spoken at Home',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'family_activities',
		'label' => 'Family Activities',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_activities_frequent',
		'label' => 'Family Activities',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'tv_time',
		'label' => 'TV Time Spent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'tv_whom',
		'label' => 'TV Spen with Whom',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'radio_time',
		'label' => 'Radio Time Spent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'radio_whom',
		'label' => 'Radio Time Spent With Whom',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'computergames_time',
		'label' => 'Computer Games Time Spent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'computergames_whom',
		'label' => 'Computer Games Time Spent with Whom',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_responsibilities',
		'label' => 'Child Responsibilites',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_responsibilities_what',
		'label' => 'What are you\'re Child Responsibilites',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'child_play_group',
		'label' => 'Child Play Group',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_play_group_frequent',
		'label' => 'Child Frequent Playgroup Frequent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'other_interest',
		'label' => 'Other Interest',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	/*
	array(
		'field' => 'sibling[firstname][]',
		'label' => 'Sibling Firstname',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
		array(
		'field' => 'sibling[age][]',
		'label' => 'Sibling Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),
		array(
		'field' => 'sibling[lastname][]',
		'label' => 'Sibling Lastname',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),*/
		array(
		'field' => 'pt_watch',
		'label' => 'Past Time watching',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
		array(
		'field' => 'pt_boardgames',
		'label' => 'Past time board games',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
		array(
		'field' => 'pt_comp',
		'label' => 'Past Time Computer',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
		array(
		'field' => 'pt_reading',
		'label' => 'Past Time Reading',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
		array(
		'field' => 'pt_sports',
		'label' => 'Past Time sports',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
		array(
		'field' => 'pe_dmilk',
		'label' => 'Drink milk from the bottle',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
		array(
		'field' => 'pe_study',
		'label' => 'Regular study time',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	)
	
	
    ),
	
	/*+-------------------------------------------------+
	* | Validation rules for part 2 of enrollment form  |
	* +-------------------------------------------------+
	*/
	
	'developmental_history' =>array(
		array(
			'field' => 'length_of_preg',
			'label' => 'Length Of Pregnancy',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'form_of_del',
			'label' => 'Form Of Delivery',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'complications',
			'label' => 'Complications On Pregnancy',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'go_to_bed',
			'label' => 'Child go to Bed',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'get_up_bed',
			'label' => 'Child wake up',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'sleeping_dist',
			'label' => 'Sleeping Disturbances',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'own_room',
			'label' => 'Own Room',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'room_shares',
			'label' => 'Shares Room with',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'wet_bed',
			'label' => 'Child wets Bed',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'observable_difficulties',
			'label' => 'Observable Difficulties',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'observable_difficulties_since_when',
			'label' => 'Observable Difficulties Since When',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'observable_difficulties_action_taken',
			'label' => 'Observable Difficulties Actions Taken',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'speech_problems',
			'label' => 'Speech Problems',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'speech_problems_since_when',
			'label' => 'Speech Problems Since When',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'speech_problems_actions_taken',
			'label' => 'Speech Problems actions Taken',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'hearing_last_checked',
			'label' => 'Hearing Last Checked',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'hearing_problems',
			'label' => 'Hearing Problems',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'hearing_problems_since_when',
			'label' => 'Hearing Problems Since when',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'hearing_problems_actions_taken',
			'label' => 'Hearing problems actions taken',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'vision_last_checked',
			'label' => 'Vision last Checked',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'vision_specify',
			'label' => 'Specify Child Vision Problems',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'hinder_childs_learning',
			'label' => 'Learning Process or general Development',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'traumatic_experiences',
			'label' => 'Any Traumatic Experiences',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'age_trauma',
			'label' => 'Age Experienced Trauma',
			'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'trauma_reaction',
			'label' => 'Childs Reaction to Trauma',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'how_trauma_affected_child',
			'label' => 'How Trauma Affected Child',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'special_fears',
			'label' => 'Special Fears',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'when_what_triggered_fear',
			'label' => 'Triggered Fear',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'how_do_you_handle',
			'label' => 'How Did You Handle Childs Fears',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
		)
	),
	'healthhistory'=>array(
		array(
			'field' => 'past_illness',
			'label' => 'Any Past Illness',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'frequent_colds',
			'label' => 'Frequent Colds',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'frequent_headaches',
			'label' => 'Frequet Headaches',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'frequent_stomachaches',
			'label' => 'Frequent Stomach Aches',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'frequent_dizziness',
			'label' => 'Frequent Dizziness',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'frequent_vommiting',
			'label' => 'Frequent Vommiting',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'major_injury',
			'label' => 'Major Injury',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'major_operations',
			'label' => 'Major Operations',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'major_ailment',
			'label' => 'Majot Ailment',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'medication_child_taking',
			'label' => 'Medication Child is taking',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'medication_allergy',
			'label' => 'Alergies to medication',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'child_eating_habbit',
			'label' => 'Child\'s Eating Habbit',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'dietary_restrictions',
			'label' => 'Dietary Restrictions',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'school_help_eating_habbit',
			'label' => 'School Help eating Habbit',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'methods_of_discp',
			'label' => 'Methods Of Discipline',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'who_handles_discp_home',
			'label' => 'Handles Discipline at Home',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'how_effective_discp',
			'label' => 'How Effective is the discipline',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'rod_discp',
			'label' => 'Discipline Using Rod',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'allow_teacher_use_rod',
			'label' => 'Do you allow the teacher to use Rod',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'what_form_of_discp_use',
			'label' => 'What form of discipline to use',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'school_supp_discp',
			'label' => 'How can school support mode of discipline',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'other_info_about_child',
			'label' => 'Other Information about Child',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'medication_prec',
			'label' => 'Ailment Precaution',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'past_illness',
			'label' => 'Any Past Illness',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'past_illness',
			'label' => 'Any Past Illness',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'past_illness',
			'label' => 'Any Past Illness',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		),//added
		array(
			'field' => 'medication_physc_nme',
			'label' => 'Physicians Name',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'medication_physc_con',
			'label' => 'Physicians Contact No.',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'medication_bt',
			'label' => 'Blood Type',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		),
		array(
			'field' => 'auth_pickup_bt',
			'label' => 'Poeple Authorized to pickup.',
			'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
		)
		
	)
);