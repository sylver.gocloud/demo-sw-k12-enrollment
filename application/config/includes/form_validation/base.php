<?php

$developmentalhistory =[
	array(
		'field' => 'length_of_preg',
		'label' => 'Length Of Pregnancy',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'form_of_del',
		'label' => 'Form Of Delivery',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'complications',
		'label' => 'Complications On Pregnancy',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'go_to_bed',
		'label' => 'Child go to Bed',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'get_up_bed',
		'label' => 'Child wake up',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'sleeping_dist',
		'label' => 'Sleeping Disturbances',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'own_room',
		'label' => 'Own Room',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'room_shares',
		'label' => 'Shares Room with',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'wet_bed',
		'label' => 'Child wets Bed',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'observable_difficulties',
		'label' => 'Observable Difficulties',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'observable_difficulties_since_when',
		'label' => 'Observable Difficulties Since When',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'observable_difficulties_action_taken',
		'label' => 'Observable Difficulties Actions Taken',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'speech_problems',
		'label' => 'Speech Problems',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'speech_problems_since_when',
		'label' => 'Speech Problems Since When',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'speech_problems_actions_taken',
		'label' => 'Speech Problems actions Taken',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'hearing_last_checked',
		'label' => 'Hearing Last Checked',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'hearing_problems',
		'label' => 'Hearing Problems',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'hearing_problems_since_when',
		'label' => 'Hearing Problems Since when',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'hearing_problems_actions_taken',
		'label' => 'Hearing problems actions taken',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'vision_last_checked',
		'label' => 'Vision last Checked',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'vision_specify',
		'label' => 'Specify Child Vision Problems',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'hinder_childs_learning',
		'label' => 'Learning Process or general Development',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'traumatic_experiences',
		'label' => 'Any Traumatic Experiences',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'age_trauma',
		'label' => 'Age Experienced Trauma',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'trauma_reaction',
		'label' => 'Childs Reaction to Trauma',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'how_trauma_affected_child',
		'label' => 'How Trauma Affected Child',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'special_fears',
		'label' => 'Special Fears',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'when_what_triggered_fear',
		'label' => 'Triggered Fear',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'how_do_you_handle',
		'label' => 'How Did You Handle Childs Fears',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	)
];

$healthhistory = [
	array(
		'field' => 'past_illness',
		'label' => 'Any Past Illness',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'frequent_colds',
		'label' => 'Frequent Colds',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'frequent_headaches',
		'label' => 'Frequet Headaches',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'frequent_stomachaches',
		'label' => 'Frequent Stomach Aches',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'frequent_dizziness',
		'label' => 'Frequent Dizziness',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'frequent_vommiting',
		'label' => 'Frequent Vommiting',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'major_injury',
		'label' => 'Major Injury',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'major_operations',
		'label' => 'Major Operations',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'major_ailment',
		'label' => 'Majot Ailment',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'medication_child_taking',
		'label' => 'Medication Child is taking',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'medication_allergy',
		'label' => 'Alergies to medication',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'child_eating_habbit',
		'label' => 'Child\'s Eating Habbit',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'dietary_restrictions',
		'label' => 'Dietary Restrictions',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'school_help_eating_habbit',
		'label' => 'School Help eating Habbit',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'methods_of_discp',
		'label' => 'Methods Of Discipline',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'who_handles_discp_home',
		'label' => 'Handles Discipline at Home',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'how_effective_discp',
		'label' => 'How Effective is the discipline',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'rod_discp',
		'label' => 'Discipline Using Rod',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'allow_teacher_use_rod',
		'label' => 'Do you allow the teacher to use Rod',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'what_form_of_discp_use',
		'label' => 'What form of discipline to use',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'school_supp_discp',
		'label' => 'How can school support mode of discipline',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'other_info_about_child',
		'label' => 'Other Information about Child',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'medication_prec',
		'label' => 'Ailment Precaution',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'past_illness',
		'label' => 'Any Past Illness',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'past_illness',
		'label' => 'Any Past Illness',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'past_illness',
		'label' => 'Any Past Illness',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),//added
	array(
		'field' => 'medication_physc_nme',
		'label' => 'Physicians Name',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'medication_physc_con',
		'label' => 'Physicians Contact No.',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'medication_bt',
		'label' => 'Blood Type',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'auth_pickup_bt',
		'label' => 'Poeple Authorized to pickup.',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	)	
];