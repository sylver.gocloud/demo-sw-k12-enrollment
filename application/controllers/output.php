<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Output Extends CI_Controller
{
	
	function get_section_data()
	{
		$this->load->model(array('M_sections'));
		$id = intval($this->input->get('id'));
		$data = $this->M_sections->get_section_by_grade_level($id);


		if($data == 'NOBLOCK')
		{
			$template = '<tr><td colspan="4" style="text-align:center;font-size:15px;font-weight:bold;color:#f00">No Block has been Set For this grade level please continue. (Click next button)</td></tr>';
			$template .= '<input type="hidden" value="noblock" name="section">';
		
		}elseif($data)
		{
			$template = '';
			
			if(!empty($data))
			{	
				foreach ($data as $data)
				{
					$template .= '<tr><td class="center"><input type="radio" name="section" title="'.$data->block_name.'" class="student_block" value="'.$data->id.'" required><small class="error">Please choose a block.</small></td>
									  <td class="center">'.$data->block_name.'</td>
									  <td class="center">'.$data->block_desc.'</td>
									  <td class="center">max load</td>
								  </tr>';
				}
			}else
			{
				$template .= '<tr>
								<td colspan="4"><input type="radio" name="section" value="0"> No Block To Choose From</td>
							  </tr>';
			}
			
		}elseif($data == FALSe)
		{
		
		}
		
		echo $template;
	}





}
