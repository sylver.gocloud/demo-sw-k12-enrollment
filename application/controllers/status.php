<?php


class Status Extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->layout_view = 'application_no_side_bar';
		$this->load->library(array('authlink','hs'));
	}
	public function index()
	{
		show_404();
	}
	
	public function success($auth = FALSE)
	{
		$this->disable_browser_cache = TRUE;
		if($auth !== FALSE)
		{
			if($this->authlink->verify_authlink($auth) == TRUE && $this->session->userdata('enrollment_finished') == FALSE)
			{
				$this->load->model('M_school_settings');
				$this->view_data['message'] = $this->M_school_settings->get_custom_message('enrollment_success');
				$this->view_data['system_message'] = $this->session->flashdata('system_message');
				$this->view_data['link'] = $this->hs->encrypt(intval(time()));
				$this->session->set_userdata('enrollment_finished',TRUE);
			}else{
				redirect('enrollment');
			}
		}else{
			$this->_msg('n','Please Fill Up the required information.','enrollment');
		}
	}
	
	public function error($auth)
	{
		$this->disable_browser_cache = TRUE;
		if($this->authlink->verify_authlink($auth) == TRUE && $this->session->userdata('enrollment_finished') == FALSE)
		{
			$data = $this->session->flashdata('vars');
			$this->view_data['ticket'] = $data->ticket;
			$this->session->set_userdata('enrollment_finished',TRUE);
		}else{
			redirect();
		}
	}
	
	public function good($auth)
	{
		// $this->disable_browser_cache = TRUE;
		// if($this->authlink->verify_authlink($auth) == TRUE && $this->session->userdata('enrollment_finished') == FALSE)
		// {
				$this->session->set_userdata('enrollment_finished',TRUE);
		// }else{
			// redirect();
		// }
	}
	
	
	public function verified($auth = '')
	{
		$this->footer = FALSE;
		$this->layout_view = 'application_no_side_bar';
		$this->disable_browser_cache = TRUE;
		if($this->authlink->verify_authlink($auth) == TRUE && $this->session->userdata('enrollment_finished') == FALSE)
		{
			$data = $this->session->flashdata('vars');
			$this->view_data['name'] = @$data->fullname;
			$this->view_data['email'] = @$data->email;
			$this->session->set_userdata('enrollment_finished',TRUE);
		}else{
			redirect();
		}
	}
	
	public function expired($auth)
	{
		$this->disable_browser_cache = TRUE;
		if($this->authlink->verify_authlink($auth) == TRUE && $this->session->userdata('enrollment_finished') == FALSE)
		{
			$data = $this->session->flashdata('vars');
			$this->view_data['name'] = ($data !== FALSE ? isset($data->fullname) ? $data->fullname : NULL : NULL);
			$this->session->set_userdata('enrollment_finished',TRUE);
		}else{
			redirect();
		}
	}
	
	public function saved()
	{
		$this->disable_browser_cache = TRUE;
		$this->footer = FALSE;
	
	
	
	}
}