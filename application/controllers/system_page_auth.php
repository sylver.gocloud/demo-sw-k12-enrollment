<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class System_page_auth extends Systemlogin_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->layout_view = 'bootstrap_three';
		if($this->session->userdata('system_logged_in') == TRUE AND strtolower($this->uri->segment(2)) !== 'logout')
		{
			redirect('welcome');
		}
		$this->load->model('M_system_users');
		$this->load->library(array('form_validation','token'));
		$this->load->helper(array('url','form'));
		$this->token->set_token();
		session_start();

		$this->view_data['system_message'] = $this->_msg();
	}
	
	public function index()
	{
		redirect();
	}
	
	private function _captcha()
	{
		$this->load->helper('captcha');
		$system_counted_hash = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
		$vals = array(
			'word'	 	 => str_replace('0','', substr(md5(rand(1,100).$system_counted_hash),3,rand(2,5))),
			'img_path'	 => './captcha/',
			'img_url'	 => base_url().'auth/captcha',
			'font_path'	 => './assets/fonts/4.ttf',
			'img_width'	 => 300,
			'img_height' => 55,
			'expiration' => 7200
			);
		
		$cap = create_captcha($vals);
		$image['word'] = $cap['word'];
		$image['image'] ='<image src="'.base_url().'captcha/'.$cap['time'].'.jpg" class="img-responsive"/>';
		$image['link'] = './captcha/'.$cap['time'].'.jpg';

		return (object)$image;
	}

	public function login()
	{
		$this->load->helper('file');//load helper file
		delete_files('./captcha');// delete all images in captcha folder
		$captcha = $this->_captcha(); //generate captcha
		$this->session->set_flashdata('image_url',$captcha->link); // save path of generated captcha to session
		$this->view_data['form_token'] = $this->token->get_token();//generate form token
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'username', 'required|trim|htmlspecialchars');
		$this->form_validation->set_rules('password', 'password', 'required|trim|htmlspecialchars');
		$this->form_validation->set_rules('fit', ' ', 'required|trim|htmlspecialchars');
		$this->form_validation->set_rules('captcha', 'captcha', 'required|trim|htmlspecialchars');
		
		$this->view_data['captcha_image'] = $captcha->image;
		$this->view_data['username'] = $username = isset($_SESSION['username']) ? $_SESSION['username'] : NULL;

		if($this->input->post('login_me'))
		{
			//get captcha link from session
			$captcha_image_link = $this->session->flashdata('image_url');
	
			// check if XSF or XXS filtering
			if($this->token->validate_token($this->input->post('fit',TRUE)))
			{
				//run form validation
				if($this->form_validation->run() !== FALSE)
				{
					//hash the word
					$captcha_word = md5(strtolower($this->session->userdata('word')));
					$sent_captcha_text = md5(strtolower($this->input->post('captcha')));
					
					if($captcha_word == $sent_captcha_text)
					{
						$verify = $this->M_system_users->verify_user($this->input->post());
						if($verify->status === true)
						{
							// vd($this->session->userdata);
							// die('x');
							$this->logger('System login','System User: {'.$username.'} successfully logged in.');

							$this->token->destroy_token();//destroy token
							$this->session->set_userdata('word',''); //destroy captcha from session
							// unlink($captcha_image_link);//delete captcha image from file
							$this->_msg($verify->code,$verify->msg,'enrollment'); // redirect success
						}else
						{
							$this->logger('login','User: {'.$username.'} Failed Login Attempt.');
							$this->token->destroy_token();//destroy token
							$this->session->set_userdata('word','');//destroy captcha from session
							// unlink($captcha_image_link);//delete captcha image from file
							// $this->_msgbootstrap('e','Invalid username/password combination.','auth/login/'.$theUserType);// redirect failed
							$this->_msg($verify->code, $verify->msg, 'system_page_auth/login');
						}
					}else{
						$this->token->destroy_token();
						$this->session->set_userdata('word','');
						// unlink($captcha_image_link);
						$this->_msg('e','<strong>Wrong</strong> Captcha Code entered.','system_page_auth/login');
					}
				}
			}else{
				$this->token->destroy_token();
				$this->session->set_userdata('word','');
				// unlink($captcha_image_link);
				$this->_msg('e','Invalid username/password combination.','system_page_auth/login');
			}
		}else{
			$this->session->set_userdata('word',$captcha->word);
		}
	}

	/*12-6-12 Login authentication for user*/
	function xlogin($userType = FALSE)
	{
		$this->config->load('acl_config');
		$this->view_data['system_message']= $this->_msg();
		$userType = strtolower(trim(htmlspecialchars($userType)));
		$allowed_login_types = $this->config->item('roles');

		if(in_array($userType,$allowed_login_types))
		{
			$this->load->helper('file');//load helper file
			delete_files('./captcha');// delete all images in captcha folder
			$captcha = $this->_captcha(); //generate captcha
			$this->session->set_flashdata('image_url',$captcha->link); // save path of generated captcha to session
			$this->view_data['form_token'] = $this->token->get_token();//generate form token
			$this->load->library('form_validation');
			$this->form_validation->set_rules('username', 'username', 'required|trim|htmlspecialchars');
			$this->form_validation->set_rules('password', 'password', 'required|trim|htmlspecialchars');
			$this->form_validation->set_rules('fit', ' ', 'required|trim|htmlspecialchars');
			$this->form_validation->set_rules('captcha_text', 'captcha', 'required|trim|htmlspecialchars');
			
			$this->view_data['userType'] = strtolower($userType);
			$this->view_data['captcha_image'] = $captcha->image;
			$this->view_data['username'] = isset($_SESSION['username']) ? $_SESSION['username'] : NULL;
			$this->view_data['roless'] = $allowed_login_types;
			$this->view_data['selected'] = $userType;
			
			if($this->input->post('backstage_login'))
			{
				$post_usertype = $this->input->post('usertype') == FALSE ? $userType : $this->input->post('usertype');
				$theUserType =  $userType == $post_usertype ? $userType : $this->input->post('usertype');
				$_SESSION['username'] = $username = $this->input->post('username');
				//get captcha link from session
				$captcha_image_link = $this->session->flashdata('image_url');
		
				// check if XSF or XXS filtering
				if($this->token->validate_token($this->input->post('fit',TRUE)))
				{
					//run form validation
					if($this->form_validation->run() !== FALSE)
					{
						//hash the word
						$captcha_word = md5(strtolower($this->session->userdata('word')));
						$sent_captcha_text = md5(strtolower($this->input->post('captcha_text')));
						
						if($captcha_word == $sent_captcha_text)
						{
							if($this->M_users->verify_user($this->input->post(),$theUserType))
							{

								$fname = $this->session->userdata('e_f_name');
								$lname = $this->session->userdata('e_l_name');
								$this->logger('login','User: {'.$username.','.$fname.','.$lname.'} successfully logged in.');

								$this->token->destroy_token();//destroy token
								$this->session->set_userdata('word',''); //destroy captcha from session
								unlink($captcha_image_link);//delete captcha image from file
								$this->_msgbootstrap('s','Login Successfull. Welcome ',$theUserType); // redirect success
							}else
							{
								$this->logger('login','User: {'.$username.'} Failed Login Attempt.');
								$this->token->destroy_token();//destroy token
								$this->session->set_userdata('word','');//destroy captcha from session
								unlink($captcha_image_link);//delete captcha image from file
								$this->_msgbootstrap('e','Invalid username/password combination.','auth/login/'.$theUserType);// redirect failed
							}
						}else{
							$this->token->destroy_token();
							$this->session->set_userdata('word','');
							unlink($captcha_image_link);
							$this->_msgbootstrap('e','wrong Captcha Code entered.','auth/login/'.$theUserType);
						}
					}
				}else{
					$this->token->destroy_token();
					$this->session->set_userdata('word','');
					unlink($captcha_image_link);
					$this->_msgbootstrap('e','Invalid username/password combination.','auth/login/'.$theUserType);
				}
			}else{
				$this->session->set_userdata('word',$captcha->word);
			}
		}else{

			// pd($userType);
			redirect();
		}
	}
	
	function login_process()
	{
		$this->config->load('acl_config');
		$this->view_data['system_message']= $this->_msg();

		$this->load->helper('file');//load helper file
		delete_files('./captcha');// delete all images in captcha folder
		$captcha = $this->_captcha(); //generate captcha
		$this->session->set_flashdata('image_url',$captcha->link); // save path of generated captcha to session
		$this->view_data['form_token'] = $this->token->get_token();//generate form token
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'username', 'required|trim|htmlspecialchars');
		$this->form_validation->set_rules('password', 'password', 'required|trim|htmlspecialchars');
		$this->form_validation->set_rules('fit', ' ', 'required|trim|htmlspecialchars');
		$this->form_validation->set_rules('captcha_text', 'captcha', 'required|trim|htmlspecialchars');
		
		$this->view_data['userType'] = strtolower($userType);
		$this->view_data['captcha_image'] = $captcha->image;
		
		if($this->input->post('backstage_login'))
		{
			//get captcha link from session
			$captcha_image_link = $this->session->flashdata('image_url');
	
			// check if XSF or XXS filtering
			if($this->token->validate_token($this->input->post('fit',TRUE)))
			{
				//run form validation
				if($this->form_validation->run() !== FALSE)
				{
					//hash the word
					$captcha_word = md5(strtolower($this->session->userdata('word')));
					$sent_captcha_text = md5(strtolower($this->input->post('captcha_text')));
					

					if($captcha_word == $sent_captcha_text)
					{
						if($this->M_users->verify_user($this->input->post(),$userType))
						{
							$this->token->destroy_token();//destroy token
							$this->session->set_userdata('word',''); //destroy captcha from session
							unlink($captcha_image_link);//delete captcha image from file
							$usertype = str_replace('-','_',$userType);
							$this->_msgbootstrap('s','Login Successfull, Welcome',$usertype); // redirect success
						}else
						{
							$this->token->destroy_token();//destroy token
							$this->session->set_userdata('word','');//destroy captcha from session
							unlink($captcha_image_link);//delete captcha image from file
							$this->_msgbootstrap('e','Invalid username/password Combination','auth/login/'.$userType);// redirect failed
						}
					}else{
						$this->token->destroy_token();
						$this->session->set_userdata('word','');
						unlink($captcha_image_link);
						$this->_msgbootstrap('e','wrong Captcha Code Entered','auth/login/'.$userType);
					}
				}
			}else{
				$this->token->destroy_token();
				$this->session->set_userdata('word','');
				unlink($captcha_image_link);
				$this->_msgbootstrap('e','Invalid username/password Combination','auth/login/'.$userType);
			}
		}else{
			$this->session->set_userdata('word',$captcha->word);
		}
	}
	
  function logout()
  {
		$message = $this->_msg();
		
		$userdata = array(
			'userid' => '',
			'username' => '',
			'userType' => '',
			'e_f_name' => '',
			'e_l_name' => '',
			'e_m_name' => '',
			'logged_in' => FALSE,
			'system_userid' 		=> '',
			'system_username'  => '',
			'system_useremail'  => '',
			'system_fullname'  => '',
			'system_logged_in' => FALSE
		);
		
		$this->session->set_userdata($userdata);

		if($message)
		{
			$this->_msg('s',$message,'system_page_auth/login');
		}else{
			$this->_msg('n','You have successfully <strong>logged out.</strong> Thank you for trying our demo school system.','system_page_auth/login');
		}
  }
}