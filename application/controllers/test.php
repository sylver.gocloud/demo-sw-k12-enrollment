<?php

class Test Extends MY_Controller
{


	public function index()
	{
		if($this->input->post())
		{
			$this->form_validation->set_rules('nat','','required|check_student_nationality');
			
			if($this->form_validation->run()!== FALSE)
			{
				pp($this->input->post());
			
			}else{
			
				echo validation_errors();
			}
		
		
		}
	}
	
	private function _accronymizer($string = FALSE)
	{
		if($string !== FALSE)
		{
			$clean_string = htmlspecialchars(strip_tags($string));
			$number_of_words = str_word_count($clean_string,1);
			$accronym = array_map(function($x)
			{
				if(strlen($x) > 1)
				{
					return $x[0];
				}
			},$number_of_words);
			return strtoupper(implode('',$accronym));
		}else{
			return NULL;
		}
	}

	
	public function testemail($fullname = 'Rhustum L Evaristo',$link = 'http://smallworld.enrollment.151272810036809.info/confirm/validate/38_9972/NDFmMzllMzNjNDZ/2007045c48d4331750e5f240f7d2c5a2304f8066.aspx',$email_add = 'auxrad_spieluhr@yahoo.com')
	{
		$school_name = ucwords($this->school_name);
		$school_name_title = $this->_accronymizer($this->school_name).'@DONOTREPLY.com';
		
		$data['school_name'] = $school_name;
		$data['link'] = $link;
		$data['name'] = $fullname;
		$data['acc'] = $this->_accronymizer($this->school_name);
		
		$message = $this->load->view('email_layouts/resend_confirmation_link',$data,TRUE);
		
		$this->load->library('email');
		
		$this->email->initialize(array( 
		   'mailtype' => 'html', 
		)); 
		
		$this->email->from($school_name_title,$school_name);
		$this->email->to($email_add);
		$this->email->subject('Online Enrollment: Resend Verification Link');
		$this->email->message($message);
		$this->email->send();
	}





}