<?php

class MY_Controller Extends CI_Controller
{
	
	var $school_name = FALSE;
	var $school_address = FALSE;
	var $school_telephone = FALSE;
	var $school_email = FALSE;
	var $school_tag_name = FALSE;
	
	var $current_grading_period = FALSE; // returns id of current grading period
	var $current_school_year = FALSE; // returns id of current school year
	var $schoolyear_date = FALSE; // returns school year format of 0000 - 0000
	var $gperiods_current; // Returns the gp_desc of current grading period
	var $current_ay; // Returns the gp_desc of current grading period
	var $disable_browser_cache = FALSE;
	protected $offline_status = FALSE;
	
	protected $layout_view = 'application_no_side_bar';
  protected $content_view ='';
  protected $view_data = array();
	protected $footer = TRUE;
	protected $header = TRUE;
	protected $student_link;
	
	public function __construct()
	{
		parent::__construct();
		$this->_check_online_status();
		$this->_set_config_items();
		
		$this->disable_browser_cache($this->disable_browser_cache);
		$this->load->model(array('M_school_settings','M_gperiods','M_school_years'));
		
		$this->current_grading_period 	= $this->M_gperiods->get_id_of_current_gperiod();
		$this->current_school_year		= $this->M_school_years->get_id_of_current_sy();
		$this->schoolyear_date			= $this->M_school_years->get_current_sy_format();
		$this->gperiods_current			= $this->M_gperiods->get_current_gperiods_value('gp_desc');
		$this->current_ay				= $this->M_school_years->get_set_school_year();
		$this->output->enable_profiler(FALSE);

		//check system page login if demo
		$this->secure_system_page();
		
		$id = 1;
		$query = $this->M_school_settings->get_all_school_setting($id);

		$row = @$query[0];
		$this->school_name = isset($row->school_name) ? $row->school_name : 'Schoolname Not Set';
		$this->school_address = isset($row->school_address) ? $row->school_address : '';
		$this->school_telephone = isset($row->school_telephone) ? $row->school_telephone : '';
		$this->school_email = isset($row->school_email) ? $row->school_email : '';
		$this->school_tag_name = isset($row->school_tag_name) ? $row->school_tag_name : '';

	}
	
	public function _output($output)
    {
	
		$this->view_data['footer'] = $this->footer;
        if($this->content_view !== FALSE && empty($this->content_view)) $this->content_view = $this->router->class . '/' . $this->router->method;
        
        $yield = file_exists(APPPATH . 'views/' . $this->content_view . EXT) ? $this->load->view($this->content_view, $this->view_data, TRUE) : FALSE ;
        
        if($this->layout_view)
            echo $this->load->view('layouts/' . $this->layout_view, array('yield' => $yield), TRUE);
        else
            echo $yield;
			echo $output;
    }
	
	protected function _msg($type = FALSE,$message = FALSE,$redirect = FALSE,$flashdata_name = 'system_message')
	{
		$type = strtolower($type);
		switch($type)
		{
		 	case $type == 'e':
				$template = "<div data-alert class='alert-box alert'><i class='icon-exclamation-sign'></i> ".htmlspecialchars(strip_tags($message))." <a href='#' class='close'>&times;</a></div>";
			break;
			case $type == 's':
				$template = "<div data-alert class='alert-box success'><i class='icon-ok'></i> ".htmlspecialchars(strip_tags($message))."</div>";
			break;
			case $type == 'n':
				$template = "<div class='alert-box'><i class='icon-info-sign'></i> ".htmlspecialchars(strip_tags($message))."</div>";
			break;
			case $type == 'p':
				$template = $message;
			break;
			case $type == FALSE;
				$template = htmlspecialchars(strip_tags($message));
			break;
		}
		
		if($type AND $message AND $redirect)
		{
			$this->session->set_flashdata($flashdata_name,$template);
			redirect($redirect);
		}elseif($type AND $message AND $redirect == FALSE){
			return $template;
		}
		
		if($redirect == FALSE AND $message == FALSE AND $redirect == FALSE)
		{
			return $this->session->flashdata($flashdata_name);
		}
	}
	
	private function disable_browser_cache($x)
	{
		if($x == TRUE)
		{
			header("Last-Modified: " . gmdate( "D, j M Y H:i:s" ) . " GMT"); // Date in the past 
			header("Expires: " . gmdate( "D, j M Y H:i:s", time() ) . " GMT"); // always modified 
			header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1 
			header("Cache-Control: post-check=0, pre-check=0", FALSE); 
			header("Pragma: no-cache");
		}
	}
	
	protected function _set_config_items()
	{
		$sql = 'SELECT * FROM backend_settings';
		$query = $this->db->query($sql);
		$data= $query->num_rows() >=1 ? $query->row() : FALSE;
	
		if($data !== FALSE)
		{
			$this->config->set_item('student_url', $data->student_link);
			$this->student_link =$data->student_link;
		}
	}
	protected function _check_online_status()
	{	
		$this->load->model('M_school_settings');
		$check = $this->M_school_settings->check_online_status();
		
		if($check->status == FALSE)
		{
	
			$this->layout_view = 'application_no_side_bar';
			$this->offline_status = TRUE;
			$this->view_data['message'] =  $this->M_school_settings->get_custom_message('offline_enrollment');
			if(strtolower($this->router->class) == 'offline')
			{
				
			}else{
				redirect('offline');
			}
		}else{
			$this->offline_status = FALSE;
			return NULL;
		}
	}

	/**
	 * Secure system page if demo
	 */
	public function secure_system_page()
	{
		if(ENVIRONMENT === "demo"){
			if(isset($this->session->userdata['system_logged_in']) && $this->session->userdata['system_logged_in'] === TRUE){

				$this->load->model('M_system_users');
				$u = $this->M_system_users->get_user($this->session->userdata['system_userid']);
				$expiration = $u->expiration ? "(Exp:".date('M d, Y g:i a',strtotime($u->expiration)).")" : "";
				$this->load->helper(['time']);
				
				//check if expired
				if($u->expiration){
					if(_check_time_expired($u->expiration)){

						$this->session->set_flashdata('system_message','You demo account has already expired. Thank you for using our demo school system.');
						redirect('system_page_auth/logout');
					}
				}
				
				$this->view_data['logo_message'] = "<p><i class='icon icon-user'></i>&nbsp; <strong>".$u->username."</strong></p><span style='font-size:10pt;font-family:arial narrow' ><i class='icon icon-tag'></i>This is a demo account <span style='color:red;font-weight:bold'>{$expiration}</span></span>";
				$this->view_data['demo_account'] = TRUE;

			}else{
				$this->_msg('e','Please consider to re-login','system_page_auth/login');
			}
		}
	}	
}

class Systemlogin_Controller Extends CI_Controller
{
	
	var $school_name = FALSE;
	var $school_address = FALSE;
	var $school_telephone = FALSE;
	var $school_email = FALSE;
	var $school_tag_name = FALSE;
	
	var $current_grading_period = FALSE; // returns id of current grading period
	var $current_school_year = FALSE; // returns id of current school year
	var $schoolyear_date = FALSE; // returns school year format of 0000 - 0000
	var $gperiods_current; // Returns the gp_desc of current grading period
	var $current_ay; // Returns the gp_desc of current grading period
	var $disable_browser_cache = FALSE;
	protected $offline_status = FALSE;
	
	protected $layout_view = 'application_no_side_bar';
  protected $content_view ='';
  protected $view_data = array();
	protected $footer = TRUE;
	protected $header = TRUE;
	
	public function __construct()
	{
		parent::__construct();
		$this->_check_online_status();
		$this->_set_config_items();
		
		$this->disable_browser_cache($this->disable_browser_cache);
		$this->load->model(array('M_school_settings','M_gperiods','M_school_years'));
		
		$this->current_grading_period 	= $this->M_gperiods->get_id_of_current_gperiod();
		$this->current_school_year		= $this->M_school_years->get_id_of_current_sy();
		$this->schoolyear_date			= $this->M_school_years->get_current_sy_format();
		$this->gperiods_current			= $this->M_gperiods->get_current_gperiods_value('gp_desc');
		$this->current_ay				= $this->M_school_years->get_set_school_year();
		$this->output->enable_profiler(FALSE);

		$id = 1;
		$query = $this->M_school_settings->get_all_school_setting($id);

		$row = @$query[0];
		$this->school_name = isset($row->school_name) ? $row->school_name : 'Schoolname Not Set';
		$this->school_address = isset($row->school_address) ? $row->school_address : '';
		$this->school_telephone = isset($row->school_telephone) ? $row->school_telephone : '';
		$this->school_email = isset($row->school_email) ? $row->school_email : '';
		$this->school_tag_name = isset($row->school_tag_name) ? $row->school_tag_name : '';

	}
	
	public function _output($output)
    {
	
		$this->view_data['footer'] = $this->footer;
        if($this->content_view !== FALSE && empty($this->content_view)) $this->content_view = $this->router->class . '/' . $this->router->method;
        
        $yield = file_exists(APPPATH . 'views/' . $this->content_view . EXT) ? $this->load->view($this->content_view, $this->view_data, TRUE) : FALSE ;
        
        if($this->layout_view){
        		$this->view_data['yield'] = $yield;
        		$this->view_data['system_message'] = $this->_msg();
            echo $this->load->view('layouts/' . $this->layout_view, $this->view_data, TRUE);
        }else{
            echo $yield;
        }
			echo $output;
    }
	
	/**
	* function for formatting and redirecting message, using flashmessage
	* @param string $type e,s,n,p 
	* @param string $message the message you want to be sent 
	* @param string $redirect the url segment i.e controller/method 
	* @param string $var_name uses a different name for flashdata 
	* @return string formatted string with css and html
	*/
	public function _msg($type = FALSE,$message = FALSE,$redirect = FALSE,$var_name = 'system_message')
	{
		$type = strtolower($type);
		switch($type)
		{
		 	case $type === 'e':
				$template = "<div data-alert class='alert alert-danger'><i class='icon-exclamation-sign'></i> ".$message."</div>";
			break;
			case $type === 's':
				$template = "<div data-alert class='alert alert-success'><i class='icon-ok'></i> ".$message."</div>";
			break;
			case $type === 'n':
				$template = "<div class='alert alert-warning'><i class='icon-info-sign'></i> ".$message."</div>";
			break;
			case $type === 'p':
				$template = $message;
			break;
			case $type === FALSE;
				$template = $message;
			break;
		}
		
		if($type AND $message AND $redirect)
		{
			$this->session->set_flashdata($var_name,$template);
			redirect($redirect);
		}elseif($type AND $message AND $redirect == FALSE){
			return $template;
		}
		
		if($redirect == FALSE AND $message == FALSE AND $redirect == FALSE)
		{
			return $this->session->flashdata($var_name);
		}
	}
	
	private function disable_browser_cache($x)
	{
		if($x == TRUE)
		{
			header("Last-Modified: " . gmdate( "D, j M Y H:i:s" ) . " GMT"); // Date in the past 
			header("Expires: " . gmdate( "D, j M Y H:i:s", time() ) . " GMT"); // always modified 
			header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1 
			header("Cache-Control: post-check=0, pre-check=0", FALSE); 
			header("Pragma: no-cache");
		}
	}
	
	protected function _set_config_items()
	{
		$sql = 'SELECT * FROM backend_settings';
		$query = $this->db->query($sql);
		$data= $query->num_rows() >=1 ? $query->row() : FALSE;
	
		if($data !== FALSE)
		{
			$this->config->set_item('student_url', $data->student_link);
		}
	}
	protected function _check_online_status()
	{	
		$this->load->model('M_school_settings');
		$check = $this->M_school_settings->check_online_status();
		
		if($check->status == FALSE)
		{
	
			$this->layout_view = 'application_no_side_bar';
			$this->offline_status = TRUE;
			$this->view_data['message'] =  $this->M_school_settings->get_custom_message('offline_enrollment');
			if(strtolower($this->router->class) == 'offline')
			{
				
			}else{
				redirect('offline');
			}
		}else{
			$this->offline_status = FALSE;
			return NULL;
		}
	}	

	protected function logger($filename = '',$message,$time = 'H:i:s A')
	{
		$path = './system_logs/';
		$ip = $this->input->ip_address();
		if(file_exists($path))
		{
			$this->load->library('user_agent');
			$this->load->helper('file');

			$browser_data = 'BRWSR:'.$this->agent->browser().'|OS:'.$this->agent->platform();


			$filename = $filename.'_'.date('m_d_Y');
			
			write_file($path.$filename.'.txt', '['.date($time).'|'.$browser_data.'|'.$ip.'] '.$message."\n",'a+');	
		
		}
	}
}