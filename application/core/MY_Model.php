<?php

class MY_Model Extends CI_Model
{
	protected $c_user;
	protected $c_gp;
	protected $c_sy;
	private $ci;
	public function __construct()
	{
		parent::__construct();
		$this->ci =& get_instance();
		$this->c_user = $this->get_user();
		$this->c_sy = $this->get_sy();
		$this->c_gp = $this->get_gp();
	}
	
	private function get_sy()
	{
		/*
		* id,sy_from,sy_to,is_set
		*/
		$sql = "SELECT * FROM school_years s WHERE s.is_set = 'yes' limit 1";
		$query = $this->db->query($sql);
		return $query->num_rows() >= 1 ? $query->row() : FALSE;
	}
	
	private function get_gp()
	{
		/*
		* gp_id,gp_code,gp_desc,gp_stat
		*/
		$sql = "SELECT gp_id,gp_code,gp_desc,gp_stat FROM grading_periods g WHERE g.is_set = 'yes' limit 1";
		$query = $this->db->query($sql);
		return $query->num_rows() >= 1 ? $query->row() : FALSE;
	}
	
	
	private function get_user()
	{
		$data['usertype'] = $this->ci->session->userdata('userType');
		$data['userid'] = $this->ci->session->userdata('userid');
		return (object)$data;
	}
	
	
	public function insert($table, $data){
		$this->db->insert($table, $data);
		$this->db->affected_rows() > 0 ? TRUE: FALSE;
	}
	
	
	
	
	
	
	
	

}