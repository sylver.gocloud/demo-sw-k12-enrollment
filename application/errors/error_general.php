<?
	$config =& get_config();
	$base_url = isset($config['base_url']) ? $config['base_url'] == '' ? NULL : $config['base_url'] : NULL;
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>An Error Occured.</title>
		<link rel="stylesheet" href="<?=$base_url;?>assets/css/error/bootstrap/bootstrap.min.css"/>
		<link rel="stylesheet" href="<?=$base_url;?>assets/css/error/bootstrap/bootstrap-theme.min.css"/>
		<link rel="stylesheet" href="<?=$base_url;?>assets/css/error/error_404.css"/>
	</head>
	<body>
		<div id="wrap">
			<!-- Begin page content -->
			<div class="container">
					<div class="page-header">
						<h1><?=$heading;?></h1>
					</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="alert alert-info">
					<?if(!empty($message)):?>
						<?=$message;?>
					<?endif;?>
					</div>
					<a href="#" onClick="parent.history.back(); return false;" class="btn btn-success"><b>Go Back.</b></a>
				</div>
			</div>
		</div>

		<div id="footer" class="shadow">
			<div class="container">
				<p class="text-muted credit">Copy &copy; GoCloudAsia 2013</p>
			</div>
		</div>
	</body>
</html>