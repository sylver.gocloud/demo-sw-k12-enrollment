<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('style_url'))
{
	function style_url($uri = '')
	{
		$CI =& get_instance();
		return $CI->config->base_url('assets/css/'.$uri.'.css');
	}
}

if ( ! function_exists('script_url'))
{
	function script_url($uri = '')
	{
		$CI =& get_instance();
		return $CI->config->base_url('assets/js/'.$uri.'.js');
	}
}

if ( ! function_exists('image_url'))
{
	function image_url($image_name = '')
	{
		$CI =& get_instance();
		return $CI->config->base_url('assets/img/'.$image_name);
	}
}

if ( ! function_exists('array_url'))
{
	function array_url($route = '')
	{
		if(is_array($route))
		{
			$data = implode('/',$route);
		}else{
			$data = $route;
		}
		$CI =& get_instance();
		return $data;
	}
}


/*
* autmatic link for student portal
*/
if ( ! function_exists('student_portal_link'))
{
	function student_portal_link()
	{
		$CI =& get_instance();
		$url = $CI->config->item('student_url');
		return $url;
	}
}









