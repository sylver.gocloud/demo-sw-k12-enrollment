<?php
	
	
function money($data)
{
	if(is_float($data))
	{
		if(preg_match('/^\d+\.\d+$/',$data) == TRUE){
			return $data;
		}else{
			return $data.'.00';
		}
	}else{
		return $data.'.00';
	}
}


function random_user_id()
{
	$CI =& get_instance();
	$idNo = $CI->current_ay->sy_to.'-'.rand(1000,9999).'-'.substr(uniqid(md5(rand(10000,99999))),0,4);
	$query = $CI->db->query('SELECT username FROM users where username = ?',array($idNo));

	if($query->num_rows() >= 1)
	{
		random_user_id();
	}else{
		return $idNo;
	}
}