<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class MY_Form_validation extends CI_Form_validation 
{

	public function __construct($rules = array())
	{
		parent::__construct($rules);
		$this->CI->lang->load('MY_form_validation');
	}
	
	public function alpha_dash_space($str)
	{
		return ( ! preg_match("/^([-a-z0-9_ ])+$/i", $str)) ? FALSE : TRUE;
	}
	
	public function alpha_space($str)
	{
		return ( ! preg_match("/^([a-z ])+$/i", $str)) ? FALSE : TRUE;
	}
	
	public function alphanum_space($str)
	{
		return ( ! preg_match("/^([a-z0-9 ])+$/i", $str)) ? FALSE : TRUE;
	}
	
	public function text_all($str)
	{
		return ( ! preg_match("/^([-a-z0-9_#,?.'#!&-:;\/\\\\[:space:]])+$/i", $str)) ? FALSE : TRUE;
	}
	
	public function enumeration($str)
	{
		return ( ! preg_match("/^([a-z0-9, ])+$/i", $str)) ? FALSE : TRUE;
	}
	
	public function check_block($str)
	{
		if(!$this->required($str) OR $str == 'NOBLOCK')
		{
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	public function check_student_nationality($x)
	{
		$x = strtolower($x);
		if($this->alpha($x))
		{
			$nats = array('filipino',
						  'filipina',
						  'pilipino',
						  'pilipina',
						  'philippine',
						  'fil',
						  'pinoy',
						  'pinay',
						  'philippines',
						  'phil',
						  'fhil',
						  'pnoy');

			if(in_array($x,$nats))
			{
				return 'filipino';
			}else{
				if($this->CI->input->post('nationality_ssp_number')	AND 
				   $this->CI->input->post('nationality_visa_status')AND
				   $this->CI->input->post('nationality_auth_stay')	AND
				   $this->CI->input->post('nationality_passport_no')AND
				   $this->CI->input->post('nationality_icard_no')	AND
				   $this->CI->input->post('nationality_date_issued'))
				  {
						return TRUE;
				  }else{
						return FALSE;
				  }
				}
		}else{
			return FALSE;
		}
	}
	
	function datechecker_2($str)
	{
	
		
		$year = $this->CI->input->post('b_year');
		$month = $this->CI->input->post('b_month');
		$day = $str;
		
		$date = $month.'/'.$day.'/'.$year;
		// pd($date);
		return $this->datechecker($date);
	}
	
	function datechecker($date_from_input)
	{
		//Date From Input must be m/d/Y
	
		$date = DateTime::createFromFormat('m/d/Y', $date_from_input);
		if($date !== FALSE)
		{
			$d_e = explode('/',$date->format('m/d/Y'));

			if($date AND checkdate($d_e[0],$d_e[1],$d_e[2]))
			{
				return TRUE;
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
	}
}