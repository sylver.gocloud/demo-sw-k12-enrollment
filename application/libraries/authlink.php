<?php
	
	
class Authlink{
	
		private $CI;
		private $token_return;
		private $random_word = 'The Big Brown Fox Jumped Over The Lazy Dog savy';
		
		public function __construct()
		{
			$this->CI =& get_instance();
		}

		private function authlink_creator()
		{
			return	sha1(md5(rand(1000,9999).sha1(md5(rand(1000,9999))))).sha1(sha1(rand(1000,9999).md5(sha1(rand(1000,9999)))));
		}
		
		private function salt_creator($data_to_salt)
		{
			return sha1(sha1($data_to_salt).sha1(md5($data_to_salt.'asdfHhHgJHGjfdhgdsjftJHsd87f6&U%^*^%')).'asdfKJH7687^HdhfnjhgU&^Hu7hdfshnkJHNK');
		}
		
		private function substr_creator($data)
		{
			return $this->salt_creator(substr($data,intval(strlen($data)/ 2)));
		}
		
		public function generate_authlink($entropy = FALSE)
		{
			if($entropy !== FALSE)
			{
				foreach(range(0,7) as $range)
				{
					$hash = $this->authlink_creator().$this->salt_creator($entropy.md5($range.uniqid).$this->random_word.rand(100000,999999));
				}
				$authlink = $hash;
			}else{
				foreach(range(0,7) as $range)
				{
					$hash = $this->authlink_creator().$this->salt_creator($this->random_word.rand(100000,999999));
				}
				$authlink = $hash;
			}
			$this->CI->session->set_userdata('authlink',$authlink);	
			return substr(md5($this->random_word.rand(100000,999999)),0,10).$authlink;
		}
		
		public function verify_authlink($auth_from_link)
		{	
			return $this->substr_creator($this->CI->session->userdata('authlink')) == $this->substr_creator(substr($auth_from_link,10)) ? TRUE : FALSE;	
		}
		
		public function clear_authlink()
		{
			$this->CI->session->set_userdata('authlink','');
		}
		
		public function hash_make($x)
		{
			foreach(range(0,7) as $num)
			{
				$data = sha1(sha1($num.md5($num.$this->random_word)).sha1($this->random_word).md5($x)).'.aspx';
			}
			return $data;
		}
		
		public function check_hash_make($i,$a)
		{
			return $this->hash_make($i) == $a ? TRUE : FALSE;
		}
}