<?php

Class Create_captcha
{	
	private $image_data;
	private $session_name = 'captcha';
	private $CI;
	private $path ='captcha';
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->helper(array('captcha','file'));
		$this->CI->load->library(array('session'));
	}
	
	//main method for creating captcha
	public function _captcha()
	{
		//hash for the captcha word
		$system_counted_hash = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
		
		//captcha image config
		$vals = array(
			'word'	 => substr(sha1(rand(1,100).$system_counted_hash),3,rand(3,5)),
			'img_path'	 => './'.$this->path.'/',
			'img_url'	 => base_url().'auth/captcha',
			'font_path'	 => './assets/fonts/4.ttf',
			'img_width'	 => 150,
			'img_height' => 40,
			'expiration' => 28800
			);
		
		//create our captcha image
		$cap = create_captcha($vals);
		
		//store needed data
		$image['word'] = $cap['word'];
		$image['image'] ='<image src="'.base_url().$this->path.'/'.$cap['time'].'.jpg"/ width="300" height:50;>';
		$image['link'] = './captcha/'.$cap['time'].'.jpg';
		$image['cap'] = $cap['time'].'.jpg';
		
		//set image data to object
		$this->image_data = $image;
		
		//create session
		$this->set_session();
	}
	
	//if you want to set session name on the fly
	/*
		$this->create_captcha->sessionname('name_of_new_captcha_session')
							 ->_captcha();
	
	*/
	public function sessionname($session_name = 'captcha')
	{
		$this->session_name = $session_name;
		return $this;
	}
	
	//return data to views for using
	public function data()
	{
		return (object)$this->image_data;
	}
	
	// set session for word
	// set captcha word on the session
	private function set_session()
	{
		//reset the session
		$this->CI->session->set_userdata($this->session_name,'');
		
		//set the current captcha word
		$this->CI->session->set_userdata($this->session_name,$this->image_data['word']);
	}
	
	//get session, get the captcha data saved on the session
	public function get_session()
	{
		return $this->CI->session->userdata($this->session_name);
	}
	
	
	//reset session
	public function reset()
	{
		$this->CI->session->set_userdata($this->session_name,'');
	}
	
	
	//validate the input and session captcha
	//first param the user input
	//second param delete image from captcha directory
	public function validate($input,$delete = FALSE)
	{
		
		$session_cap = $this->get_session();
		if($session_cap)
		{
			//get base image path
			// since we are in apps libries directory we have to get it from our base index
			$path_to_iamge = FCPATH.$this->path.DIRECTORY_SEPARATOR;
			
			//delete all files in captcha directory
			if($delete == TRUE)
			{
				delete_files($path_to_iamge);
			}
			// pd(strtolower($input) .' '. strtolower($session_cap));
			//return if captcha session is equal to input
			return strtolower($input) == strtolower($session_cap);
		}else{
			return FALSE;
		}
	}
}