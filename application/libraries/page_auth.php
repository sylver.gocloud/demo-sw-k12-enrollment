<?php

class Page_auth
{
	private $CI;
	public function __construct()
	{
		$this->CI =& get_instance();
	}
	// redirects page to specified controller only
	public function secure_page($redirect_to = FALSE)
	{
		if($this->CI->session->userdata('logged_in') === TRUE)
		{
			if($this->CI->uri->segment(1) !== $this->CI->session->userdata('userType'))
			{
				redirect($this->CI->session->userdata('userType'));
			}
		}else
		{
			if($redirect_to == FALSE){
				redirect('auth/login/'.$this->CI->uri->segment(1));
			}else{
				redirect($redirect_to);
			}
			
		}		
	}
	
	
	public function secure_welcome_page()
	{
		if($this->CI->session->userdata('logged_in') === TRUE)
		{
			redirect($this->CI->session->userdata('userType'));
		}
	}
	
	public function secure_output_page()
	{
		if($this->CI->session->userdata('logged_in') === FALSE)
		{
			show_404();
		}
	}
	
	
	
}