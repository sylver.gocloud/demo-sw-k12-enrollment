<?php

class token
{
	private $ci;
	public function __construct()
	{
		$this->ci =& get_instance();		$this->set_token();
	}
	public function set_token()
	{	
		if($this->get_token() == NULL OR $this->get_token() == FALSE OR $this->get_token() == '')
		{
			$this->ci->session->set_userdata('form_token',$this->random_hash());
		}
	}
	
	public function random_hash()
	{
		$string = base64_encode(time().rand(1000,9999).sha1(rand(1000,9999))); 
		return substr(hash('sha256',$string).sha1($string).sha1($string),0,20);
	}		public function reset()	{		$this->ci->session->set_userdata('form_token',FALSE);	}
	
	public function get_token()
	{
		return $this->ci->session->userdata('form_token');
	}
	
	public function validate_token($token)
	{
		return $this->ci->session->userdata('form_token') == $token ? TRUE : FALSE;
	}

	public function destroy_token()
	{
		$this->ci->session->set_userdata('form_token','');
	}
}