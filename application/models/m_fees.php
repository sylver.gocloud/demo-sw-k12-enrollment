<?php
	
	
class M_fees Extends CI_Model
{
	private $__table ='fees';
	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_all_fees($id ='')
	{
		if($id == ''){
			$query = $this->db->get($this->__table);
		}else{
			$query = $this->db->select(array('fee_id','fee_desc','fee_stat','fee_name','fee_rate'))->where('fee_id',$id)->get($this->__table);
		}		
		return $query->num_rows() > 0 ? $query->result() : FALSE;
		
	}
	
	public function fetch_fees($limit,$start)
	{
        $this->db->limit($limit, $start);
        $query = $this->db->select(array('fee_id','fee_desc','fee_stat','fee_name','fee_rate'))->get($this->__table);
		return $query->num_rows() >= 1 ? $query->result() : FALSE;
	}
	
	public function count_all_fees()
	{
		return $this->db->count_all($this->__table);
	}
	
	public function add_fees($data)
	{
		if($this->verify_data($data,2) == FALSE)
		{
			$data['fee_created'] = NOW;
			$this->db->insert($this->__table,$data);
			return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
			
		}else{
			return array('status'=>'exist');
		}
		
	}
	
	public function update_fees($data,$id)
	{
		if($this->verify_data($data,2) == FALSE)
		{
			$data['fee_updated'] = NOW;
			$this->db->set($data)->where('fee_id',$id)->update($this->__table);
			return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
		}else
		{
			return array('status' => 'exist');
		}
	}
	
	/* verify_data
	 * @param array
	 * @param int
	*  verifies if data entered by user is existing already in the database
	*  $strict level 1,2
	*  level 1 = if one field has existing data in database returns true else false
	*  level 2 = if all field has existing data in database returns true else false
	*  12/11/2012
	*/
	public function verify_data($data,$strict_level)
	{		
			if($strict_level == 1)
			{
				$query = $this->db->or_where($data)->get($this->__table);
			}elseif($strict_level == 2){
				$query = $this->db->where($data)->get($this->__table);
			}
			return $query->num_rows() > 0 ? TRUE : FALSE;
	}
}
