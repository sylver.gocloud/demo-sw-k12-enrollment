<?php

class M_id_creator Extends MY_Model
{
	private $table = 'student_id_counter';
	
	public function get_id($level)
	{
		if($this->get_last_id($level) == FALSE)
		{
			$this->get_id($level);
		}else{
			return $this->get_last_id($level);
		}
	}
	
	public function search_id($id,$level)
	{
		$table = $this->table;
		$year = $this->c_sy->sy_from;
		
		$sql = "SELECT max(id_num) as max_num 
				FROM {$table} 
				WHERE grade_level = ? 
				AND id_year = ?
				AND id_num = ?";
		$query = $this->db->query($sql,array($level,$year,$id));
		
		return $query->num_rows() >=1 ? TRUE : FALSE;
	}
	
	public function get_last_id($grade_level = FALSE)
	{
		if($grade_level !== FALSE AND is_numeric($grade_level))
		{
			$table = $this->table;
			$year = $this->c_sy->sy_from;
			$sql = "SELECT max(id_num) as max_num FROM {$table} WHERE grade_level = ? AND id_year = ?";
			
			$query = $this->db->query($sql,array($grade_level,$year));
			
			if($query->num_rows() >=1)
			{
				$result = $query->row()->max_num;
				if($result !== NULL)
				{
					return $result + 1;
				}else{
					return 1;
				}
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
	}
	
	public function get_assigned_category($level)
	{
		$table = 'levels';
		
		$query = $this->db->select('level_code_for_id')
						  ->where('level_id',$level)
						  ->limit(1)
						  ->get($table);
						  
		return $query->num_rows() >= 1 ? $query->row()->level_code_for_id : FALSE;
	}
	
	public function get_new_id($level = '')
	{
		$num = $this->get_id($level);
		$cat = $this->get_assigned_category($level);
		$year = $this->c_sy->sy_from;
		
		
		if($num AND $cat AND $year)
		{
			$data['num'] = $num;
			$data['cat'] = $cat;
			$data['year'] = $year;
			
			return (object)$data;
		}else{
			return FALSE;
		}
	}
	
	/*
		======= FOR COMPRESSED METHOD ON CREATING ID ================
	*/
	
	//smallworld format
	// [year]-[CATEGORY][NUMBER]
	
	public function create_id($level_id)
	{
		$cat = $this->get_assigned_category($level_id);
		$year = $this->c_sy->sy_from;
		
		$insert['grade_level'] = $level_id;
		$insert['id_year'] = $year;
		$insert['id_cat'] = $cat;
		$insert['id_num'] = 0;
		
		$this->db->insert($this->table,$insert);
		
		if($this->db->affected_rows() >= 1)
		{
			$id = $this->db->insert_id();
			$complete = substr($year,2).'-'.$cat.$id;
			
			$this->db->set('id_complete',$complete)
					 ->where('id',$id)
					 ->update($this->table);
					 
			return $this->db->affected_rows() >= 1 ? $complete : FALSE;
		}else{
			return FALSE;
		}
	}
	
	
}