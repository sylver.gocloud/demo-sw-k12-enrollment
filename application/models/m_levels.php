<?php
class M_levels extends CI_Model {
	
	private $__table = 'levels';
	public function get_all_levels($id=FALSE)
	{
		if($id == FALSE){
			$query = $this->db->get($this->__table);
		}else{
			$query = $this->db->where('level_id',$id)->get($this->__table);
		}
		
		return $query->num_rows() >= 1 ? $query->result() : FALSE;
	}
	/*
		query to fetch all grade level for pagnation
		tomex
		11/12/11/2012
	*/	
	public function fetch_grade_level($limit, $start)
	{
        $this->db->limit($limit, $start);
        $query = $this->db->select(array('level_id','level_desc','level_stat','level_code'))->get($this->__table);
		return $query->num_rows() >= 1 ? $query->result() : FALSE;
	}
	
	/*
		query to update level_stat to inactive
		tomex
		11/12/11/2012
	*/
	public function destroy($id){
		$data['level_updated'] = NOW;
		$data['level_stat'] = 'inactive';
		$this->db->set($data)->where('level_id',$id)->update($this->__table);
		return $this->db->affected_rows() > 0 ? TRUE : FALSE;
	}
	
	/* verify_data
	 * @param array
	 * @param int
	*  verifies if data entered by user is existing already in the database
	*  $strict level 1,2
	*  level 1 = if one field has existing data in database returns true else false
	*  level 2 = if all field has existing data in database returns true else false
	*  12/11/2012
	*/
	public function verify_data($data,$strict_level)
	{		
			if($strict_level == 1)
			{
				$query = $this->db->or_where($data)->get($this->__table);
			}elseif($strict_level == 2){
				$query = $this->db->where($data)->get($this->__table);
			}
			return $query->num_rows() > 0 ? TRUE : FALSE;
	}
	
	
	public function get_grade_level($level_id){
		$q = $this->db->where('level_id', $level_id)->get('levels');
		return $q->num_rows() >= 1 ? $q->row() : FALSE;
	}
	
	public function get_specific_coloumn($data)
	{	
		$query = $this->db->select($data)->get($this->__table);
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}
	
	public function get_all_levels_array()
	{
		$query = $this->db->select(array('level_id','level_desc','level_code'))
						  ->where('level_stat','active')
						  ->get($this->__table);
		if($query->num_rows() > 0)
		{
			$result[''] ='-- Choose A Level --';
			foreach($query->result() as $q)
			{
				$result[$q->level_id] = $q->level_desc;
			}
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public function check_for_level_cat($id,$type)
	{
	
		$type = array_map('strtolower',$type);
		
		$q = $this->db->select('level_category')
					  ->where('level_id',$id)
					  ->where_in('level_category',$type)
					  ->get('levels');
					  
		if($q->num_rows() >=1)
		{
			$q = strtolower($q->row()->level_category);
			return in_array($q,$type) ? TRUE : FALSE;
		}else{
			return FALSE;
		}
	}
}
?>