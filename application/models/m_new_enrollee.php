<?php
	
class m_new_enrollee Extends MY_Model
{
	private $last_insert_id;
	private $CI;
	private $__table = 'enrollments';
	private $__seftable = 'student_enrollment_fees';
	
	public function __construct()
	{
		parent::__construct();
		$this->CI =& get_instance();
		$this->CI->load->model('m_level_fees','mlfees');
	}
	
	
	/*
		Insert new enrollee to enrollments table
		with verify time and code for verification
	*/
	public function enroll_new_student($data)
	{
		$sy = $this->c_sy;
		unset($data['password']);
		$data['e_sy_from'] = $sy->sy_from;
		$data['e_sy_to'] = $sy->sy_to;
		$data['e_ay_id'] = $sy->id;
		$data['e_created_at'] = NOW;
		$this->db->insert($this->__table,$data);
		$this->last_insert_id = $this->db->insert_id();
		return $this->db->affected_rows() > 0 ? TRUE : FALSE;	
	}
	
	public function get_fees_set_for_current_level($level_id,$enroll_id)
	{
		$l = $this->db->select('payment_group')->limit(1)->where('level_id',$level_id)->get('levels');
	
		if($l->num_rows() >= 1)
		{
			$payment_group_id = $l->row()->payment_group;
			if($payment_group_id !== 0)
			{
				$select = $this->db->where('group_id',$payment_group_id)
							       ->get('payment_group_fees');
			
				
				if($select->num_rows() >= 1)
				{
					foreach($select->result() as $k => $v)
					{
						if($v->fee_type == 'misc')
						{
							$student_fee[$k]['is_misc_fee'] = 1;
							$student_fee[$k]['is_cca_fee'] = 0;
							$student_fee[$k]['is_tuition_fee'] = 0;
						}elseif($v->fee_type == 'tuit')
						{
							$student_fee[$k]['is_tuition_fee'] = 1;
							$student_fee[$k]['is_cca_fee'] = 0;
							$student_fee[$k]['is_misc_fee'] = 0;
						
						}elseif($v->fee_type == 'cca')
						{
							$student_fee[$k]['is_cca_fee'] = 1;
							$student_fee[$k]['is_misc_fee'] = 0;
							$student_fee[$k]['is_tuition_fee'] = 0;
						}
						
						
						$student_fee[$k]['sef_enrollment_id'] = $enroll_id;
						$student_fee[$k]['sef_fee_name'] = $v->fee_name;
						$student_fee[$k]['sef_fee_rate'] = $v->fee_price;
						$student_fee[$k]['sef_schoolyear_id'] = $this->c_sy->id;
						$student_fee[$k]['sef_gperiod_id'] = $this->c_gp->gp_id;
						$student_fee[$k]['sef_created'] = NOW;

					}

						$this->db->insert_batch('student_enrollment_fees',$student_fee);
						
						if($this->db->affected_rows() >= 1)
						{
							$this->_check_enrollee_nationality($level_id,$enroll_id);
							return TRUE;
						}else{
							return FALSE;
						}
				
				}else{
					return TRUE;
				}
			}else{
				return TRUE;
			}
		}else{
			return TRUE;
		}
	}
	
	private function _check_enrollee_nationality($lid,$eid)
	{
		// get enrolee nationality
		$q = $this->db->select('nationality')
				 ->join('enrollments e','e.e_profile_id = p.profile_id')
				 ->where('e_id',$eid)
				 ->limit(1)
				 ->get('profiles p');
		
		
		if($q->num_rows() >=1 )
		{
			$nat= $q->row()->nationality;		
				
			//if nationality is filipino no foreign fee applied
			if(strtolower($nat) == 'filipino')
			{
				return TRUE;
			}else{
				//get and insert foreign fees
				$ff = 'INSERT INTO student_enrollment_fees (sef_enrollment_id,
															sef_fee_name,
															sef_fee_rate,
															sef_gperiod_id,
															sef_schoolyear_id,
															is_elective_fee
															)
						SELECT ?,ff.fee_name,ff.fee_rate,?,?,1
						FROM foreign_fee ff
						WHERE ff.level_id = ?';
				
				$this->db->query($ff,array($eid,$this->c_gp->gp_id,$this->c_sy->id,$lid));
				
				return TRUE;
			}
		}else{
			return TRUE;
		}
	}
	
	
	/*
		Get all other fees assigned for a grade level then insert to student enrollment fees
		this is important so that if a fee is changed, the student will not be affected
		because they have their own record of the previous fees assigned to them
	*/
	
	/*
	public function get_fees_set_for_current_level($level_id,$enroll_id,$gperiod,$sy)
	{
		//get all selected fees assigned for grade level
		$data = $this->CI->mlfees->get_sel_fees($level_id);
		
		if(!empty($data))
		{
			$count = 0;
			foreach($data as $fees)
			{
				$s_e_fees[$count]['sef_enrollment_id'] =$enroll_id;
				$s_e_fees[$count]['sef_fee_name'] = $fees->fee_name;
				$s_e_fees[$count]['sef_fee_rate'] = $fees->fee_rate;
				$s_e_fees[$count]['sef_gperiod_id'] = $gperiod;
				$s_e_fees[$count]['sef_schoolyear_id'] = $sy;
				$s_e_fees[$count]['sef_created'] = NOW;
				$s_e_fees[$count]['is_other_fee'] = 1;
				$count++;
			}
			$this->db->insert_batch($this->__seftable,$s_e_fees);
			if($this->db->affected_rows() >= 1)
			{
				//set tuition fee and misc fee for the current level
				if($this->set_tuition_fees_set_for_current_level($level_id,$enroll_id,$gperiod,$sy) == TRUE)
				{
					return TRUE;
				}else{
					log_message('error','unable to set fees for current level id '.$enroll_id);
					return FALSE;
				}
			}
		}else
		{
			return TRUE;
		}
	}
	*/
	/*
		function that inserts tuition to enrollee
	*/
	
	/*
	public function set_tuition_fees_set_for_current_level($level_id,$enroll_id,$gperiod,$sy)
	{
		
		// Selects all tuition fee that is active.
		
		$sql = "SELECT f.fee_name,f.fee_rate
				FROM fees f 
				WHERE f.is_tuition_fee = 1
				AND f.grade_level = ?
				AND f.fee_stat = 'active' ";
				
		$query = $this->db->query($sql,array($level_id,));
		$data = $query->num_rows() >= 1 ? $query->result() : FALSE;
		
		//format tuition fees for entry
		if(!empty($data))
		{
			$count = 0;
			foreach($data as $fees)
			{
				$s_e_fees[$count]['sef_enrollment_id'] =$enroll_id;
				$s_e_fees[$count]['sef_fee_name'] = $fees->fee_name;
				$s_e_fees[$count]['sef_fee_rate'] = $fees->fee_rate;
				$s_e_fees[$count]['sef_gperiod_id'] = $gperiod;
				$s_e_fees[$count]['sef_schoolyear_id'] = $sy;
				$s_e_fees[$count]['sef_created'] = NOW;
				$s_e_fees[$count]['is_tuition_fee'] = 1;
				$count++;
			}
			//insert tuition fees
			$this->db->insert_batch($this->__seftable,$s_e_fees);
			
			// if successfully inserted tuition fee
			// call method insert_miscellaneous_fee()
			// this method will insert misc fee for the student
			
			if($this->db->affected_rows() >= 1 )
			{
				//call method to insert misc fee
				if($this->_insert_miscellaneous_fee($enroll_id,$sy,$level_id) == TRUE)
				{
					return TRUE;
				}else{
					log_message('error','Unable to insert Miscellaneous fees. ID'.$enroll_id);
					return FALSE;
				}
			}else{
				log_message('error','Unable to insert Tuition fees. ID'.$enroll_id);
				return FALSE;
			}
		}else{
			return FALSE;
		}
	}
	*/
	/*
		Inserts misc fees to students, called after inserting the tuition fee,
		at method set_tuition_fees_set_for_current_level();
	*/
	/*
	private function _insert_miscellaneous_fee($id,$sy,$level_id)
	{
		$sql = "SELECT f.fee_name,f.fee_rate
				FROM fees f 
				WHERE f.is_misc_fee = 1
				AND f.grade_level = ?
				AND f.fee_stat = 'active' ";
				
		$query = $this->db->query($sql,array($level_id,));
		$data = $query->num_rows() >= 1 ? $query->result() : FALSE;
		
		if(!empty($data))
		{
			$count = 0;
			foreach($data as $fees){
				$s_e_fees[$count]['sef_enrollment_id'] =$id;
				$s_e_fees[$count]['sef_fee_name'] = $fees->fee_name;
				$s_e_fees[$count]['sef_fee_rate'] = $fees->fee_rate;
				$s_e_fees[$count]['sef_schoolyear_id'] = $sy;
				$s_e_fees[$count]['sef_created'] = NOW;
				$s_e_fees[$count]['is_misc_fee'] = 1;
				$count++;
			}
			$this->db->insert_batch($this->__seftable,$s_e_fees);
			if($this->db->affected_rows() >= 1)
			{
				if($this->_insert_cca_fee($id,$sy,$level_id) == TRUE)
				{
					return TRUE;
				}else{
					log_message('error','Unable to insert CCA fees. ID'.$enroll_id);
					return FALSE;
				}
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
	}
	*/
	/*
		Inserts CCA fees to students, called after inserting the misc fee,
		at method _insert_miscellaneous_fee();
	*/
	/*
	private function _insert_cca_fee($id,$sy,$level_id)
	{
		$sql = "SELECT f.fee_name,f.fee_rate
				FROM fees f 
				WHERE f.is_cca_fee = 1
				AND f.grade_level = ?
				AND f.fee_stat = 'active' ";
				
		$query = $this->db->query($sql,array($level_id,));
		$data = $query->num_rows() >= 1 ? $query->result() : FALSE;
		
		if(!empty($data))
		{
			$count = 0;
			foreach($data as $fees){
				$s_e_fees[$count]['sef_enrollment_id'] =$id;
				$s_e_fees[$count]['sef_fee_name'] = $fees->fee_name;
				$s_e_fees[$count]['sef_fee_rate'] = $fees->fee_rate;
				$s_e_fees[$count]['sef_schoolyear_id'] = $sy;
				$s_e_fees[$count]['sef_created'] = NOW;
				$s_e_fees[$count]['is_cca_fee'] = 1;
				$count++;
			}
			$this->db->insert_batch($this->__seftable,$s_e_fees);
			//call method to insert misc fee
				if($this->db->affected_rows() >= 1)
				{
					return TRUE;
				}else{
					return FALSE;
				}
		}else{
			return FALSE;
		}
	}
	*/
	public function add_further_profile_value($data,$id,$type='')
	{
		if($type == 'dev')
		{
			$input['dhc_pregnancy_length'] = $data['length_of_preg'];
			$input['dhc_delivery_form'] = $data['form_of_del'];
			$input['dhc_compications'] = $data['complications'];
			$input['dhc_sleep_time'] = $data['go_to_bed'];
			$input['dhc_wakup_time'] = $data['get_up_bed'];
			// $data['dhc_sleeping_disturbance'] = $data['sleeping_dist'];
			$input['dhc_sleeping_disturbance_what'] = $data['sleeping_dist'];
			$input['dhc_child_own_room'] = $data['own_room'];
			$input['dhc_room_shares_who'] = $data['room_shares'];
			$input['dhc_child_wet_bed'] = $data['wet_bed'];
			// $data['dhc_defect_growth'] = $data['observable_difficulties'];
			$input['dhc_defect_specify'] = $data['observable_difficulties'];
			$input['dhc_defect_when'] = $data['observable_difficulties_since_when'];
			$input['dhc_defect_actions'] = $data['observable_difficulties_action_taken'];
			// $data['dhc_speech_problems'] = $data['speech_problems'];
			$input['dhc_speech_problem_what'] = $data['speech_problems'];
			$input['dhc_speech_problem_when'] = $data['speech_problems_since_when'];
			$input['dhc_speech_problem_actions'] = $data['speech_problems_actions_taken'];
			$input['dhc_hearing_last_check'] = $data['hearing_last_checked'];
			// $data['dhc_hearing_problems'] = $data['hearing_problems'];
			$input['dhc_hearing_problems_what'] = $data['hearing_problems'];
			$input['dhc_hearing_problems_when'] = $data['hearing_problems_since_when'];
			$input['dhc_hearing_problem_actions'] = $data['hearing_problems_actions_taken'];
			$input['dhc_vision_checked'] = $data['vision_last_checked'];
			// $data['dhc_sight_problem'] = $data['vision_specify'];
			$input['dhc_sight_problems_what'] = $data['vision_specify'];
			$input['dhc_hinder_learning_process'] = $data['hinder_childs_learning'];
			// $data['ce_trauma_exp'] = $data[''];
			$input['ce_trauma_exp_what'] = $data['traumatic_experiences'];
			$input['ce_trauma_exp_age'] = $data['age_trauma'];
			$input['ce_trauma_exp_reaction'] = $data['trauma_reaction'];
			$input['ce_trauma_exp_affected'] = $data['how_trauma_affected_child'];
			// $data['ce_special_fears'] = $data['special_fears'];
			$input['ce_special_fears_what'] = $data['special_fears'];
			$input['ce_special_fears_when'] = $data['when_what_triggered_fear'];
			$input['ce_special_fears_handle'] = $data['how_do_you_handle'];
			
			$input['enrollment_step'] = 'Developmental history and childhood experiences';
		}
			elseif($type == 'hlth')
		{
			$input['hhc_past_illness'] = $data['past_illness'];
			$input['hhc_frequent_colds'] = $data['frequent_colds'];
			$input['hhc_frequent_headaches'] = $data['frequent_headaches'];
			$input['hhc_frequent_stomachaches'] = $data['frequent_stomachaches'];
			$input['hhc_dizziness'] = $data['frequent_dizziness'];
			$input['hhc_vomitting'] = $data['frequent_vommiting'];
			// $input['hhc_major_injury'] = $data['major_injury'];
			$input['hhc_major_injury_specify'] = $data['major_injury'];
			// $input['hhc_major_operations'] = $data['major_operations'];
			$input['hhc_major_operations_specify'] = $data['major_operations'];
			// $input['hhc_diagnosed'] = $data['major_ailment'];
			$input['hhc_diagnosed_specify'] = $data['major_ailment'];
			$input['hhc_medication'] = $data['medication_child_taking'];
			$input['hhc_alergic_med'] = $data['medication_allergy'];
			$input['hhc_general_eating'] = $data['child_eating_habbit'];
			$input['hhc_food_restrictions'] = $data['dietary_restrictions'];
			$input['hhc_school_help_eating'] = $data['school_help_eating_habbit'];
			$input['d_method_of_discipline'] = $data['methods_of_discp'];
			$input['d_handles_discipline'] = $data['who_handles_discp_home'];
			$input['d_discipline_effective'] = $data['how_effective_discp'];
			// $input['d_discipline_rod'] = $data['rod_discp'];
			$input['d_discipline_rod_why'] = $data['rod_discp'];
			// $input['d_teacher_use_rod'] = $data[''];
			$input['d_teacher_use_rod_why'] = $data['allow_teacher_use_rod'];
			$input['d_suggest_discipline'] = $data['what_form_of_discp_use'];
			$input['d_school_help_discipline'] = $data['school_supp_discp'];
			$input['d_provide_information'] = $data['other_info_about_child'];
			
			$input['phsc_name'] = $data['medication_physc_nme'];
			$input['phsc_cont'] = $data['medication_physc_con'];
			$input['c_blood_type'] = $data['medication_bt'];
			$input['ailment_prec'] = $data['medication_prec'];
			$input['auth_pu_ppl'] = $data['auth_pickup_bt'];
			
			$input['enrollment_step'] = 'Health history and discipline';
		}
		
		
		$this->db->set($input)->where('profile_id',$id)->update('profiles');
		return $this->db->affected_rows() >= 1 ? TRUE : FALSE;
	}
	
	public function count_id($data,$grade_level)
	{
		$input['id_cat'] = $data->cat;
		$input['id_year'] = $data->year;
		$input['id_num']= $data->num;
		$input['id_complete'] = $data->full;
		$input['grade_level'] = $grade_level;
		$query = $this->db->insert('student_id_counter',$input);
		
		if($this->db->affected_rows() >=1)
		{
			return TRUE;
		}else{
			log_message('error','ID '.$data->full.' Was not inserted to database ID counter');
		}
	}
	
	public function get_last_input_id()
	{
		return $this->last_insert_id;
	}
	
	public function finish_enrollment($data,$eid)
	{
		$data['unfinished'] = 0;
		$this->db->set($data)->where('e_id',$eid)->update('enrollments');
		return $this->db->affected_rows() >= 1 ? TRUE : FALSE;
	}
	
	public function get_recent_insert_profile($id)
	{
		$sql = '
		SELECT 
		present_address,
		contact_no,
		email,
		how_long_living_present_address,
		last_school_name,
		last_school_level,
		last_school_address,
		fathername,
		father_age,
		father_religion,
		father_nationality,
		father_educ_attain,
		father_hobby_talent,
		father_occupation,
		father_office_add,
		father_office_num,
		mothername,
		mother_age,
		mother_religion,
		mother_nationality,
		mother_educ_attain,
		mother_hobby_talent,
		mother_occupation,
		mother_office_add,
		mother_office_num,
		guardian_name,
		relationship,
		guardian_address,
		guardian_contact_no,
		guardian_reason,
		parent_status,
		parent_status_how_long,
		parent_rights,
		parents_rights_who
		FROM profiles p
		WHERE p.profile_id = ?
		LIMIT 1';
		
		$q = $this->db->query($sql,array($id));
		
		return $q->num_rows() >= 1 ? $q->row() : FALSE;
	}
	
	
	public function save_data_for_resume($data_for_temp_enrollment)
	{
		
		$daft_punk = (object)$data_for_temp_enrollment;	
		
		$secure_key_for_resume_enrollment = substr(sha1($daft_punk->e_profile_id.''.$daft_punk->enrollment_id.''.$daft_punk->emailaddress),4,8);
		
		$update['resume_security_code'] = $secure_key_for_resume_enrollment;
		
		$this->db->set($update)->where('e_id',$daft_punk->enrollment_id)->update('enrollments');
		
		$return['code'] = $secure_key_for_resume_enrollment;
		
		if($this->db->affected_rows() >= 1)
		{
			$return['status'] = TRUE;
		}else{
			$return['status'] = FALSE;
		}
		
		
		// vd($return);
		
		return (object)$return;
	}
	
	public function get_resume_credentials_for_resume_enrollment($input)
	{
		if($input)
		{
			$email = $input['resume']['email'];
			$code = $input['resume']['security_code'];
			
			$sql = 'SELECT p.profile_id,e.e_id,e.e_grade_level as level_id
					FROM enrollments e
					LEFT JOIN profiles p ON e.e_profile_id = p.profile_id
					WHERE p.email = ?
					AND resume_security_code = ?
					AND e.e_ay_id = ?
					LIMIT 1';
					
			$exec_q = $this->db->query($sql,array($email,$code,$this->c_sy->id));
			return $exec_q->num_rows() >= 1 ? $exec_q->row() : FALSE;
		}else{
			return FALSE;
		}
	}
	
	public function get_student_resume_data($eid,$pid)
	{
		$sql = "SELECT * 
				FROM enrollments e,profiles p
				WHERE e.e_id = ?
				AND p.profile_id = ?";
				
		$q= $this->db->query($sql,array($eid,$pid));
		
		if($q->num_rows() >= 1)
		{
			// pd($q->result());
			foreach($q->result()[0] as $k => $v)
			{
				$data[$k] = $v;
			}
			
			return $data;
			// pd($data);
		}else{
			return $false;
		}
	}
	
	public function save_enrollment_for_later($data,$profile_id)
	{
	
		// pd($data);
		
		$bd[] = $data["b_year"];
		$bd[] = $data["b_month"];
		$bd[] = $data["b_day"];
		$raw_dob = implode('-',$bd);
		$dob = dateTime::createFromFormat('Y-m-d',$raw_dob)->format('Y-m-d');

		$input["firstname"] = $data["child_fname"];
		$input["middlename"] = $data["child_mname"];
		$input["lastname"] = $data["child_lname"];
		$input["nickname"] = $data["child_nickname"];
		$input["name_ext"] = $data["child_name_ext"];
		$input["pob"] = $data["child_placeofbirth"];
		$input["dob"] = $dob;

		$input["religion"] = $data["child_religous"];
		$input["nationality"] = $data["child_nationality"];
		$input["present_address"] = $data["child_city_address"];
		$input["contact_no"] = $data["child_telno"];
		$input["how_long_living_present_address"] = $data["child_adresshowlong"];
		$input["last_school_name"] = $data["school_last_attended"];
		$input["last_school_level"] = $data["school_level_completed"];
		$input["last_school_address"] = $data["school_address"];
		$input["fathername"] = $data["father_name"];
		$input["father_age"] = $data["father_age"];
		$input["father_religion"] = $data["father_relaffil"];
		$input["father_nationality"] = $data["father_citizenship"];
		$input["father_educ_attain"] = $data["father_educ"];
		$input["father_hobby_talent"] = $data["father_talent"];
		$input["father_occupation"] = $data["father_occup"];
		$input["father_office_add"] = $data["father_office_address"];
		$input["father_office_num"] = $data["father_office_tel"];
		$input["mothername"] = $data["mother_name"];
		$input["mother_age"] = $data["mother_age"];
		$input["mother_religion"] = $data["mother_relaffil"];
		$input["mother_nationality"] = $data["mother_citizenship"];
		$input["mother_educ_attain"] = $data["mother_educ"];
		$input["mother_hobby_talent"] = $data["mother_talent"];
		$input["mother_occupation"] = $data["mother_occup"];
		$input["mother_office_add"] = $data["mother_office_address"];
		$input["mother_office_num"] = $data["mother_office_tel"];
		$input["guardian_name"] = $data["guardian_name"];
		$input["relationship"] = $data["guardian_relation"];
		$input["guardian_address"] = $data["guardian_address"];
		$input["guardian_contact_no"] = $data["guardian_contact_num"];
		$input["guardian_reason"] = $data["guardian_reason"];
		$input["parent_status"] = $data["parent_status"];
		$input["parent_status_how_long"] = $data["status_how_long"];
		$input["child_pos"] = $data["position_of_child"];
		$input["parent_rights"] = $data["right_over_child"];
		$input["parents_rights_who"] = $data["right_over_child_whom"];
		$input["child_adopted"] = $data["is_child_adopted"];
		$input["child_age_adopted"] = $data["age_of_adoption"];
		$input["child_aware_adopted"] = $data["child_aware_adopted"];
		$input["mother_present_pregnant"] = $data["mother_presently_pregnant"];
		$input["mother_due_date"] = $data["mother_pregnancy_due_date"];
		$input["family_deaths"] = $data["family_deaths"];
		$input["family_deaths_child_relation"] = $data["family_deaths_relation"];
		$input["family_accidents"] = $data["family_accidents"];
		$input["family_accidents_child_relation"] = $data["family_accidents_relation"];
		$input["what"] = $data["what"];
		$input["when"] = $data["when"];
		$input["other_household_member1"] = $data["hhmembers1"];
		$input["other_household_age1"] = $data["hhmembersage1"]; 
		$input["other_household_member2"] = $data["hhmembers2"];
		$input["other_household_age2"] = $data["hhmembersage2"];
		$input["other_household_member3"] = $data["hhmembers3"];
		$input["other_household_age3"] = $data["hhmembersage3"];
		$input["other_household_member4"] = $data["hhmembers4"];
		$input["other_household_age4"] = $data["hhmembersage4"];
		$input["other_household_member5"] = $data["hhmembers5"];
		$input["other_household_age5"] = $data["hhmembersage5"];
		$input["other_household_member6"] = $data["hhmembers6"];
		$input["other_household_age6"] = $data["hhmembersage6"];
		$input["languages_spoken_at_home"] = $data["language_at_home"];
		$input["family_activities"] = $data["family_activities"];
		$input["family_activities_frequent"] = $data["family_activities_frequent"];
		$input["time_spent_on_tv"] = $data["tv_time"];
		$input["tv_whom"] = $data["tv_whom"];
		$input["time_spent_on_radio"] = $data["radio_time"];
		$input["radio_whom"] = $data["radio_whom"];
		$input["time_spent_on_pc"] = $data["computergames_time"];
		$input["pc_whom"] = $data["computergames_whom"];
		$input["child_responsibilty"] = $data["child_responsibilities"];
		$input["responsibility_what"] = $data["child_responsibilities_what"];
		$input["child_playgroup"] = $data["child_play_group"];
		$input["child_playgroup_frequent"] = $data["child_play_group_frequent"];
		$input["child_interest"] = $data["other_interest"];




		// for preschool
		$input['preschool_age_first_walked'] = isset($data['age_first_walked']) ? $data['age_first_walked']: NULL;
		$input['preschool_age_first_talked'] = isset($data['age_first_talked']) ? $data['age_first_talked']: NULL;
		$input['preschool_w_for_urinate'] = isset($data['w_for_urinate']) ? $data['w_for_urinate']: NULL;
		$input['preschool_w_for_bowel'] = isset($data['w_for_bowel']) ? $data['w_for_bowel']: NULL;
		$input['preschool_w_usual_time'] = isset($data['w_usual_time']) ? $data['w_usual_time']: NULL;
		$input['preschool_child_dress'] = isset($data['child_dress']) ? $data['child_dress']: NULL;
		$input['preschool_child_undress'] = isset($data['child_undress']) ? $data['child_undress']: NULL;
		$input['preschool_hand_orientation'] = isset($data['hand_orientation']) ? $data['hand_orientation']: NULL;
		$input['preschool_self_feed'] = isset($data['self_feed']) ? $data['self_feed']: NULL;
		$input['preschool_stayinplace'] = isset($data['stayinplace']) ? $data['stayinplace']: NULL;
		$input['preschool_joinothereating'] = isset($data['joinothereating'])? $data['joinothereating']: NULL;
		$input['preschool_food_preferences'] = isset($data['food_preferences'])? $data['food_preferences']: NULL;
		$input['preschool_drink_from_bottle'] = isset($data['drink_from_bottle'])? $data['drink_from_bottle']: NULL;
		$input['preschool_toilet_trained'] = isset($data['toilet_trained'])? $data['toilet_trained']: NULL;
		$input['preschool_pasttime_watch'] = isset($data['pt_watch'])? $data['pt_watch']: NULL;
		$input['preschool_pasttime_boardgames'] = isset($data['pt_boardgames'])? $data['pt_boardgames']: NULL;
		$input['preschool_pasttime_comp'] = isset($data['pt_comp'])? $data['pt_comp']: NULL;
		$input['preschool_pasttime_reading'] = isset($data['pt_reading'])? $data['pt_reading']: NULL;
		$input['preschool_pasttime_sports'] = isset($data['pt_sports'])? $data['pt_sports']: NULL;
		$input['preschool_milkbottle'] = isset($data['pe_dmilk'])? $data['pe_dmilk']: NULL;
		$input['preschool_studytime'] = isset($data['pe_study'])? $data['pe_study']: NULL;
		
		
		//secondary
		$input['secondary_study_time'] = isset($data['sec_study_time'])? $data['sec_study_time '] : NULL;
		$input['secondary_study_whom'] = isset($data['sec_study_whom'])? $data['sec_study_whom '] : NULL;
		$input['secondary_child_pastime'] = isset($data['sec_child_pastime'])? $data['sec_child_pastime '] : NULL;
		$input['secondary_child_htd'] = isset($data['sec_child_htd'])? $data['sec_child_htd '] : NULL;
		$input['secondary_child_music'] = isset($data['sec_child_music'])? $data['sec_child_music '] : NULL;
		$input['secondary_child_hfrus'] = isset($data['sec_child_hfrus'])? $data['sec_child_hfrus '] : NULL;
		$input['secondary_child_curfew'] = isset($data['sec_child_curfew'])? $data['sec_child_curfew '] : NULL;
		$input['secondary_talkingl'] = isset($data['sec_talkingl'])? $data['sec_talkingl '] : NULL;
		$input['secondary_answering'] = isset($data['sec_answering'])? $data['sec_answering '] : NULL;
		$input['secondary_slamdoor'] = isset($data['sec_slamdoor'])? $data['sec_slamdoor '] : NULL;
		$input['secondary_misbehave_others'] = isset($data['sec_misbehave_others'])? $data['sec_misbehave_others '] : NULL;
		$input['secondary_rhle'] = isset($data['sec_rhle'])? $data['sec_rhle '] : NULL;
		$input['go_home_independently'] = isset($data['al_tghome'])? $data['al_tghome '] : NULL;
		
		
		
		$input['intermediate_watch'] = isset($data['hs_watch']) ? $data['hs_watch'] : NULL;
		$input['intermediate_boardgames'] = isset($data['hs_boardgames']) ? $data['hs_boardgames'] : NULL;
		$input['intermediate_comp'] = isset($data['hs_comp']) ? $data['hs_comp'] : NULL;
		$input['intermediate_reading'] = isset($data['hs_reading']) ? $data['hs_reading'] : NULL;
		$input['intermediate_other_pt'] = isset($data['hs_other_pt']) ? $data['hs_other_pt'] : NULL;
		$input['intermediate_spend_study'] = isset($data['hs_spend_study']) ? $data['hs_spend_study'] : NULL;
		$input['intermediate_first_mens'] = isset($data['hs_first_mens']) ? $data['hs_first_mens'] : NULL;
		$input['intermediate_signal_mens'] = isset($data['hs_signal_mens']) ? $data['hs_signal_mens'] : NULL;
		
		$input['f_ssp_status'] = isset($data['nationality_ssp_number']) ? $data['nationality_ssp_number'] : ' ';
		$input['f_visa_status'] =isset( $data['nationality_visa_status']) ? $data['nationality_visa_status'] : ' ';
		$input['f_authorized_stay'] = isset($data['nationality_auth_stay']) ? $data['nationality_auth_stay'] : ' ';
		$input['f_passport_no'] = isset($data['nationality_passport_no']) ? $data['nationality_passport_no'] : ' ';
		$input['f_i_card_no'] = isset($data['nationality_icard_no']) ? $data['nationality_icard_no'] : ' ';
		$input['f_date_if_issue'] = isset($data['nationality_date_issued']) ? $data['nationality_date_issued'] : ' ';
		
		$input["dhc_pregnancy_length"] = $data["length_of_preg"];
		$input["dhc_delivery_form"] = $data["form_of_del"];
		$input["dhc_compications"] = $data["complications"];
		$input["dhc_sleep_time"] = $data["go_to_bed"];
		$input["dhc_wakup_time"] = $data["get_up_bed"];
		$input["dhc_sleeping_disturbance_what"] = $data["sleeping_dist"];
		$input["dhc_child_own_room"] = $data["own_room"];
		$input["dhc_room_shares_who"] = $data["room_shares"];
		$input["dhc_child_wet_bed"] = $data["wet_bed"];
		$input["dhc_defect_specify"] = $data["observable_difficulties"];
		$input["dhc_defect_when"] = $data["observable_difficulties_since_when"];
		$input["dhc_defect_actions"] = $data["observable_difficulties_action_taken"];
		$input["dhc_speech_problem_what"] = $data["speech_problems"];
		$input["dhc_speech_problem_when"] = $data["speech_problems_since_when"];
		$input["dhc_speech_problem_actions"] = $data["speech_problems_actions_taken"];
		$input["dhc_hearing_problems"] = $data["hearing_last_checked"];
		$input["dhc_hearing_problems_what"] = $data["hearing_problems"];
		$input["dhc_hearing_problems_when"] = $data["hearing_problems_since_when"];
		$input["dhc_hearing_problem_actions"] = $data["hearing_problems_actions_taken"];
		$input["dhc_vision_checked"] = $data["vision_last_checked"];
		$input["dhc_sight_problems_what"] = $data["vision_specify"];
		$input["dhc_hinder_learning_process"] = $data["hinder_childs_learning"];
		
		$input["ce_trauma_exp_what"] = $data["traumatic_experiences"];
		$input["ce_trauma_exp_age"] = $data["age_trauma"];
		$input["ce_trauma_exp_reaction"] = $data["trauma_reaction"];
		$input["ce_trauma_exp_affected"] = $data["how_trauma_affected_child"];
		$input["ce_special_fears_what"] = $data["special_fears"];
		$input["ce_special_fears_when"] = $data["when_what_triggered_fear"];
		$input["ce_special_fears_handle"] = $data["how_do_you_handle"];
		$input["hhc_past_illness"] = $data["past_illness"];
		$input["hhc_frequent_colds"] = $data["frequent_colds"];
		$input["hhc_frequent_headaches"] = $data["frequent_headaches"];
		$input["hhc_frequent_stomachaches"] = $data["frequent_stomachaches"];
		$input["hhc_dizziness"] = $data["frequent_dizziness"];
		$input["hhc_vomitting"] = $data["frequent_vommiting"];
		$input["hhc_major_injury_specify"] = $data["major_injury"];
		$input["hhc_major_operations_specify"] = $data["major_operations"];
		$input["hhc_diagnosed_specify"] = $data["major_ailment"];
		$input["hhc_medication"] = $data["medication_child_taking"];
		$input["ailment_prec"] = $data["medication_prec"];
		$input["hhc_alergic_med"] = $data["medication_allergy"];
		$input["hhc_general_eating"] = $data["child_eating_habbit"];
		$input["hhc_food_restrictions"] = $data["dietary_restrictions"];
		$input["hhc_school_help_eating"] = $data["school_help_eating_habbit"];
		$input["d_method_of_discipline"] = $data["methods_of_discp"];
		$input["d_handles_discipline"] = $data["who_handles_discp_home"];
		$input["d_discipline_effective"] = $data["how_effective_discp"];
		$input["d_discipline_rod_why"] = $data["rod_discp"];
		$input['d_teacher_use_rod_why'] = $data['allow_teacher_use_rod'];
		$input["d_suggest_discipline"] = $data["what_form_of_discp_use"];
		$input["d_school_help_discipline"] = $data["school_supp_discp"];
		$input["d_provide_information"] = $data["other_info_about_child"];
		$input["phsc_name"] = $data["medication_physc_nme"];
		$input["phsc_cont"] = $data["medication_physc_con"];
		$input["c_blood_type"] = $data["medication_bt"];
		$input["auth_pu_ppl"] = $data["auth_pickup_bt"];
		
		
		
		set_time_limit(0);
		$this->db->set($input)->where('profile_id',$profile_id)->update('profiles');
		return $this->db->affected_rows() >= 1 ? TRUE : FALSE;
	}
	
	public function finish_resumed_enrollment($data,$e_id,$p_id)
	{
		// pp('enrollment id: '.$e_id);
		// pp('profile_id id: '.$p_id);
		// pp('Data:');
		// pd($data);
		
		$this->db->set($data)->where('profile_id',$p_id);
		return $this->db->affected_rows() >=1 ? TRUE : FALSE;
	}
	
	function verify_resume_if_not_from_cache($e_id,$p_id)
	{
		$q = $this->db->select('unfinished')
				 ->where('e_id',$e_id)
				 ->where('e_profile_id',$p_id)
				 ->get('enrollments');
				 
		if($q->num_rows() >=1)
		{
			$row = $q->row();
			
			if($row->unfinished == 1)
			{
				return TRUE;
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
	}

	public function enroll_stud($data)
	{
		$this->db->insert('profiles', $data);
		if($this->db->affected_rows() >0)
		{
			return $this->db->insert_id();
		}
	}

	public function enroll_students($data)
	{
		$this->db->insert('enrollments', $data);
		if($this->db->affected_rows()>0)
		{
			return TRUE;
		}else{
			return false;
		}
	}

	/*
	** Jhunnie
	** 09-17-2015
	** Check Duplicate Enrollees
	** @param $info array student details
	** @return array existing students matching the input information
	*/
	public function check_if_student_exist($info)
	{
		$lname = $info['child_lname'];
		$mname = $info['child_mname'];
		$fname = $info['child_fname'];
		$month = $info['b_month'];
		$year = $info['b_year'];
		$day = $info['b_day'];
		$bdate = $year."-".$month."-".$day;
		$sql = '
		     SELECT 
		     	   p.lastname,
			   p.middlename,
			   p.firstname,
			   p.dob
		     FROM profiles p
		     LEFT JOIN enrollments e 
		     on e.e_profile_id = p.profile_id
		     WHERE p.lastname = ?
		     AND e.e_ay_id = ?
		     AND p.middlename = ?
		     AND p.dob = ?
		     AND p.firstname LIKE ?
		';
		$re = $this->db->query($sql, [$lname, $this->c_sy->id,$mname, $bdate, "%".$fname."%"]);
		if($re->num_rows()>0)
		{
			return $re->result();
		}else{
			return FALSE;
		}
		
	}
}