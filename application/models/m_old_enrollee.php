<?php
class M_old_enrollee Extends MY_Model
{
	private $grade_level_id;
	public function check_student_id($id)
	{
		$sql = "SELECT e_id,e_profile_id,e_ay_id
				FROM enrollments 
				WHERE e_student_id = ? 
				AND e_registered = 1
				AND verified = 1
				ORDER BY e_created_at ASC
				LIMIT 1";
		$query = $this->db->query($sql,array($id));
		
		$row = $query->row();
		if(!empty($row))
		{
			if($row->e_ay_id == $this->c_sy->id)
			{
				return array('enrollment_id'=>NULL,'profile_id'=>NULL,'status' => FALSE,'log'=>"<div class='error'><span class='bold big'>{$id}</span> This student Is Already Registered For the Current Academic Year</div>");
			}else{
				return array('enrollment_id'=>$row->e_id,'profile_id'=>$row->e_profile_id,'status' => TRUE);
			}
		}else{
			return array('enrollment_id'=>NULL,'profile_id'=>NULL,'status' => FALSE,'log'=>"<div class='error'>Student ID <span class='bold big'>{$id}</span> was not found</div>");
		}
	}
	
	public function verify_student_credentials($data,$student_id)
	{
		$data = (object)$data;
		// get password from db
		$get_pass = 'SELECT hashed_password 
					 FROM users 
					 WHERE username = ?
					 LIMIT 1
					 ';
		$q_get_pass = $this->db->query($get_pass,array($student_id));
		$r_get_pass = $q_get_pass->num_rows() >=1 ? $q_get_pass->row()->hashed_password !== NULL ? $q_get_pass->row()->hashed_password : NULL : NULL;
	
		// check if firstname and lastname is correct
		$sql = 'SELECT count(e_id) as total 
				FROM enrollments e
				LEFT JOIN profiles p on e.e_profile_id = p.profile_id
				LEFT JOIN users u ON u.id = p.user_id
				WHERE p.firstname = ? AND p.lastname = ?';
		$query = $this->db->query($sql,array($data->fname,$data->lname));
		
		if($query->num_rows() >=1)
		{
			if($query->row()->total)
			{
				//check if student entered the correct password
				$ci =& get_instance();
				$ci->load->model('M_users','m');
				if($ci->m->validate_password($r_get_pass,$data->password))
				{
					return array('status'=>TRUE);
				}else{
					return array('status'=>FALSE);
				}
			}else{
				return array('status'=>FALSE);
			}
		}else{
			return array('status'=>FALSE);
		}
	}
	
	public function create_old_enrollee($id)
	{
		// get next grade level
		$enrollee_data = "SELECT l.level_hierarchy,e.e_id,e.e_profile_id
							FROM enrollments e
							LEFT JOIN levels l ON e.e_grade_level = l.level_id
							WHERE e_student_id = ?
							ORDER BY e_created_at ASC
							";
		$result = $this->db->query($enrollee_data,array($id));
		$e_data = $result->num_rows() >= 1 ? $result->row() : FALSE;
		
		if($e_data)
		{
			// get grade level to be assigned
			$get_grade_level_to_assign = "SELECT level_id 
										  FROM levels 
										  WHERE level_hierarchy > ?
										  AND level_stat = 'active' 
										  ORDER BY level_hierarchy 
										  ASC LIMIT 1";
			$q_glevel = $this->db->query($get_grade_level_to_assign,array($e_data->level_hierarchy));
			$r_glevel = $q_glevel->num_rows() >=1 ? $q_glevel->row() : FALSE;
			
			if($r_glevel)
			{
				// update student's old enrollment data, set as archived
				$update_enrollments = "update enrollments set e_archived = 1 WHERE e_id = ?";
				$update = $this->db->query($update_enrollments,array($e_data->e_id));
				if($this->db->affected_rows() >=1)
				{
					$for_old_enrollment['e_profile_id'] = $e_data->e_profile_id;
					$for_old_enrollment['e_grade_level'] = $r_glevel->level_id;
					$for_old_enrollment['e_sy_from'] = $this->c_sy->sy_from;
					$for_old_enrollment['e_sy_to'] = $this->c_sy->sy_to;
					$for_old_enrollment['e_created_at'] = NOW;
					$for_old_enrollment['e_enrollee_type'] = 'old';
					$for_old_enrollment['e_ay_id'] = $this->c_sy->id;
					$for_old_enrollment['e_student_id'] = $id;
					$for_old_enrollment['verified'] = 1;
					
					if($this->db->insert('enrollments',$for_old_enrollment))
					{
						$this->grade_level_id = $for_old_enrollment['e_grade_level'];
						$this->last_insert_id =  $this->db->insert_id();
						return (object)array('status'=>TRUE,'log'=>'<div class="success">Student Has been Enrolled Successfully</div>');
					}else{
						return (object)array('status'=>FALSE,'log'=>'<div class="error">An error occurred while registering student</div>');
					}
				}else{
					return (object)array('status'=>FALSE,'log'=>'<div class="error">AN error has occured, system was unable to update your old records.</div>');
				}
			}else{
				return (object)array('status'=>FALSE,'log'=>'<div class="error">An error occurred, System was unable to get your Grade Level</div>');
			}
		}else{
			return (object)array('status'=>FALSE,'log'=>'<div class="error">An error occurred, System was unable to fetch your old data.</div>');
		}
	}
	
	public function return_data()
	{
		return (object)['id'=>$this->last_insert_id,'level_id'=>$this->grade_level_id];
	}
}
