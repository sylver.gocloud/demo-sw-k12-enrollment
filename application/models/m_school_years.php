<?php
class M_school_years extends CI_Model {
	
	private $__table = 'school_years';
	
	public function get_all_school_year($id=FALSE)
	{
		if($id == FALSE){
			$query = $this->db->get($this->__table);
		}else{
			$query = $this->db->where('id',$id)->get($this->__table);
		}
		
		return $query->num_rows() >= 1 ? $query->result() : FALSE;
	}
	
	/*
		Get Set School Year
	*/
	public function get_set_school_year()
	{
		
		$query = $this->db->where('is_set','yes')->get($this->__table);
		
		return $query->num_rows() >= 1 ? $query->row() : FALSE;
	}
	
	public function get_current_sy_format()
	{
		$data = $this->get_set_school_year();
		if(!empty($data))
		{
			$date = $data->sy_from.' - '.$data->sy_to;
			return $date;
		}else{
			return NULL;
		}
	}
	
	/*
		Add School Year
	*/
	public function add_school_year($input)
	{
		if($this->verify_data($input,1) == FALSE)
		{
			$this->db->insert($this->__table,$input);
			return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
		}else
		{
			return array('status'=>'exist');
		}
	}
	
	/*
		query to count all school years for pagnation
	*/	
	public function count_school_year()
	{
		return $this->db->count_all($this->__table);
	}
	
	/*
		query to fetch all school year for pagnation
	*/	
	public function fetch_school_year($limit, $start)
	{
        $this->db->limit($limit, $start);
        $query = $this->db->select(array('id','sy_from','sy_to','is_set'))->get($this->__table);
		return $query->num_rows() >= 1 ? $query->result() : FALSE;
	}
	
	/*
		set School Year
	*/
	public function set_school_year($input,$id)
	{
		$input['is_set'] = '';
		$this->db->set($input)->update($this->__table);
		$input['is_set'] = 'yes';
		$this->db->set($input)->where('id',$id)->update($this->__table);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');			
	}
	
	public function get_id_of_current_sy()
	{
		$query = $this->db->where('is_set','yes')->select('id')->get($this->__table);
		if($query->num_rows () > 0 ){
			$row = $query->row();
			return $row->id;
		}		
	}
	
	/* verify_data
	 * @param array
	 * @param int
	*  verifies if data entered by user is existing already in the database
	*  $strict level 1,2
	*  level 1 = if one field has existing data in database returns true else false
	*  level 2 = if all field has existing data in database returns true else false
	*  12/11/2012
	*/
	public function verify_data($data,$strict_level)
	{		
			if($strict_level == 1)
			{
				$query = $this->db->or_where($data)->get($this->__table);
			}elseif($strict_level == 2){
				$query = $this->db->where($data)->get($this->__table);
			}
			return $query->num_rows() > 0 ? TRUE : FALSE;
	}
}
?>