<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_sections Extends MY_Model
{
	public function get_section_by_grade_level($level_id)
	{
		$detect_if_block = $this->db->select('id,block_name,block_desc')
									->where('block_level',$level_id)
									->get('block_section');
									
									
		if($detect_if_block->num_rows() >=1)
		{
			return $detect_if_block->num_rows() >= 1 ? $detect_if_block->result() : FALSE;
		}else{
			return 'NOBLOCK';
		}

		
	}



}