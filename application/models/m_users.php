<?php
class M_users extends MY_Model {
	
	private $last_insert_id;
	/* 	
		-------------------------------------------------------------------
		add_user
		12-6-12
		perry
		-------------------------------------------------------------------
		Adds a user on the system
		
		Returns true if add success
		Returns false if add unsuccessful
	*/
	public function add_user($attrib){
		$v = (object) $attrib;
		$hashed_password = $this->hash_password_parent($v->hashed_password);
		
		$user_data = array(
				'username' => $v->username,
				'hashed_password' => $hashed_password,
				'userType' => $v->userType,
				'userStatus' => $v->userStatus,
				'created_at' => NOW,
		);
		$this->db->insert('users', $user_data);
		return $this->db->affected_rows() > 0 ? TRUE : FALSE;
	}
	
	
	/* 	
		-------------------------------------------------------------------
		verify_user
		12-6-12
		perry
		-------------------------------------------------------------------
		 Verifies if an existing user is present in the system and checks their login credentials
		
		Returns false if credentials are invalid or userstatus is not active
		Returns true if credentials are valid
	*/
	public function verify_user($attrib)
	{
		$v = (object) $attrib;
		$this->db->where('username', $v->username);
		$query = $this->db->get('users');
	
		if($query->num_rows() >= 1 && $query->row()->userStatus == 'active')
		{
			$row = $query->row();
			
			if($this->validate_password($row->hashed_password, $v->password)){
				$userdata = array(
					'userid' => $row->id,
					'username' => $row->username,
					'userType' => $row->userType,
					'logged_in' => TRUE
				);
				
				$this->session->set_userdata($userdata);
				return true;
			}
		}else{
			return false;
		}
	}
	
	
	/* 	
		-------------------------------------------------------------------
		hash_password
		12-6-12
		perry
		-------------------------------------------------------------------
		hashes the users password
		
		returns the hashed password with salt
	*/
	public function hash_password($password)
    {
        $salt = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
        $hash = hash('sha256', $salt . $password);

        return $salt . $hash;
    }

    /* 	
		-------------------------------------------------------------------
		hash_password
		-------------------------------------------------------------------
	*/
	public function hash_password_parent($password)
    {
        $this->load->helper('passwordcompat');
		return password_hash($password,PASSWORD_BCRYPT);
    }

	/* 	
		-------------------------------------------------------------------
		validate_password
		12-6-12
		perry
		-------------------------------------------------------------------
		validates the users password
		
		returns the hashed password with salt
	*/
	public function validate_password($hashed_password, $password)
    {
        $salt = substr($hashed_password, 0, 64);
        $hash = substr($hashed_password, 64, 64);

        $password_hash = hash('sha256', $salt . $password);
        
        return $password_hash == $hash;
    }
	
	/*
	--------------------------------------------------------------------------
	get_all_users
	12-6-2012
	tom
	--------------------------------------------------------------------------
	@param id
	if parameter is false gets all users from db
	if parameter exists gets specific id from parameter
	*/
	
	
	
	public function get_all_users($id = FALSE)
	{
		if($id == FALSE){
			$query = $this->db->get('users');		
		}else{
			$query = $this->db->where('id',$id)->get('users');	
		}
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}
	
	/*
	--------------------------------------------------------------------------
	update_users
	12-6-2012
	tom
	--------------------------------------------------------------------------
	@param id
	@param data = array
	updates user info
	*/	
	
	public function update_user($id,$data)
	{
		$this->db->set($data)->where('id',$id)->update('users');
		return $this->db->affected_rows() > 0 ? TRUE : FALSE;
	}
	

	/*
	--------------------------------------------------------------------------
	change_pass
	12-6-2012
	tom
	--------------------------------------------------------------------------
	@param id
	@param newpass
	updates password
	*/	
	
	public function change_pass($newpassword,$id){

			$newpassword = $this->hash_password($newpassword);
			if($this->db->set('hashed_password',$newpassword)->where('id',$id)->update('users')){
				return TRUE;
			}else{
				return FALSE;
			}
	}
	
	/*
	--------------------------------------------------------------------------
	verify_password
	12-6-2012
	tom
	--------------------------------------------------------------------------
	@param id
	@param old password
	checks password from database and matches from user input
	*/	
	public function verify_password($password,$id)
	{
		$data = $this->db->select('hashed_password')->where('id',$id)->get('users');
		foreach($data->result() as $data)
		{
			$h_pass = $data->hashed_password;
			break;
		}
		if($this->validate_password($h_pass,$password)){
			return TRUE;	
		}else{
			return FALSE;
		}
	}

		
	public function destroy($id)
	{
		$this->db->update('status','inactive')->where('id',$id)->update('users');
		return $this->db->affected_rows() > 0 ? TRUE : FALSE;
	}
		
	
	function delete_user($user_id){
		$this->db->where('id', $user_id)->delete('users');
		return $this->db->affected_rows() >= 1 ? TRUE : FALSE;
	}
	
	
	public function register_user($profile_data)
	{
		unset($profile_data['level_id']);
		$profile_data = (object)$profile_data;
		
		//format date and get age from birhtdate
		$d = $this->__format($profile_data->b_month.'/'.$profile_data->b_day.'/'.$profile_data->b_year);
		// pd($d);
		$profile_input['firstname'] = $profile_data->child_fname;
		$profile_input['lastname'] = $profile_data->child_lname;
		$profile_input['middlename'] = strtolower($profile_data->child_mname) == 'none' ? NULL : $profile_data->child_mname;
		$profile_input['nickname'] = $profile_data->child_nickname;
		$profile_input['dob'] = $d ? $d->dob : "";
		$profile_input['age'] = $d ? $d->age : "";
		$profile_input['gender'] = $profile_data->gender;
		$profile_input['pob'] = $profile_data->child_placeofbirth;
		$profile_input['nationality'] = $profile_data->child_nationality;
		$profile_input['religion'] = $profile_data->child_religous;
		$profile_input['present_address'] = $profile_data->h_no .', '. $profile_data->st_no .', '. $profile_data->brgy .', '. $profile_data->munc .', '. $profile_data->city;
		$profile_input['contact_no'] = $profile_data->child_telno;
		$profile_input['email'] = $profile_data->email;
		$profile_input['how_long_living_present_address'] = $profile_data->child_adresshowlong;
		$profile_input['last_school_name'] = $profile_data->school_last_attended;
		$profile_input['last_school_level'] = $profile_data->school_level_completed;
		$profile_input['last_school_address'] = $profile_data->school_address;
		$profile_input['fathername'] = $profile_data->father_name;
		$profile_input['father_age'] = $profile_data->father_age;
		$profile_input['father_religion'] = $profile_data->father_relaffil;
		$profile_input['father_nationality'] = $profile_data->father_citizenship;
		$profile_input['father_educ_attain'] = $profile_data->father_educ;
		$profile_input['father_hobby_talent'] = $profile_data->father_talent;
		$profile_input['father_occupation'] = $profile_data->father_occup;
		$profile_input['father_office_add'] = $profile_data->father_office_address;
		$profile_input['father_office_num'] = $profile_data->father_office_tel;
		$profile_input['mothername'] = $profile_data->mother_name;
		$profile_input['mother_age'] = $profile_data->mother_age;
		$profile_input['mother_religion'] = $profile_data->mother_relaffil;
		$profile_input['mother_nationality'] = $profile_data->mother_citizenship;
		$profile_input['mother_educ_attain'] = $profile_data->mother_educ;
		$profile_input['mother_hobby_talent'] = $profile_data->mother_talent;
		$profile_input['mother_occupation'] = $profile_data->mother_occup;
		$profile_input['mother_office_add'] = $profile_data->mother_office_address;
		$profile_input['mother_office_num'] = $profile_data->mother_office_tel;
		$profile_input['guardian_name'] = $profile_data->guardian_name;
		$profile_input['relationship'] = $profile_data->guardian_relation;
		$profile_input['guardian_address'] = $profile_data->guardian_address;
		$profile_input['guardian_contact_no'] = $profile_data->guardian_contact_num;
		$profile_input['guardian_reason'] = $profile_data->guardian_reason;
		$profile_input['parent_status'] = $profile_data->parent_status;
		$profile_input['parent_status_how_long'] = $profile_data->status_how_long;
		$profile_input['parent_rights'] = $profile_data->right_over_child;
		$profile_input['parents_rights_who'] = $profile_data->right_over_child_whom;
		$profile_input['child_pos'] = $profile_data->position_of_child;
		$profile_input['child_adopted'] = $profile_data->is_child_adopted;
		$profile_input['child_age_adopted'] = $profile_data->age_of_adoption;
		$profile_input['child_aware_adopted'] = $profile_data->child_aware_adopted;
		$profile_input['mother_present_pregnant'] = $profile_data->mother_presently_pregnant;
		$profile_input['mother_due_date'] = $profile_data->mother_pregnancy_due_date;
		$profile_input['family_deaths'] = $profile_data->family_deaths;
		$profile_input['family_deaths_child_relation'] = $profile_data->family_deaths_relation;
		$profile_input['family_accidents'] = $profile_data->family_accidents;
		$profile_input['family_accidents_child_relation'] = $profile_data->family_accidents_relation;
		$profile_input['what'] = $profile_data->what;
		$profile_input['when'] = $profile_data->when;
		$profile_input['other_household_member1'] = $profile_data->hhmembers1;
		$profile_input['other_household_member2'] = $profile_data->hhmembers2;
		$profile_input['other_household_member3'] = $profile_data->hhmembers3;
		$profile_input['other_household_member4'] = $profile_data->hhmembers4;
		$profile_input['other_household_member5'] = $profile_data->hhmembers5;
		$profile_input['other_household_member6'] = $profile_data->hhmembers6;
		$profile_input['other_household_age1'] = $profile_data->hhmembersage1;
		$profile_input['other_household_age2'] = $profile_data->hhmembersage2;
		$profile_input['other_household_age3'] = $profile_data->hhmembersage3;
		$profile_input['other_household_age4'] = $profile_data->hhmembersage4;
		$profile_input['other_household_age5'] = $profile_data->hhmembersage5;
		$profile_input['other_household_age6'] = $profile_data->hhmembersage6;
		$profile_input['languages_spoken_at_home'] = $profile_data->language_at_home;
		$profile_input['family_activities'] = $profile_data->family_activities;
		$profile_input['family_activities_frequent'] = $profile_data->family_activities_frequent;


		$profile_input['time_spent_on_tv'] = " ";
		$profile_input['tv_whom'] = " ";
		$profile_input['time_spent_on_radio'] = " ";
		$profile_input['radio_whom'] = " ";
		$profile_input['time_spent_on_pc'] = " ";
		$profile_input['pc_whom'] = " ";
		$profile_input['child_responsibilty'] = " ";
		$profile_input['responsibility_what'] = " ";
		$profile_input['child_playgroup'] = " ";
		$profile_input['child_playgroup_frequent'] = " ";
		$profile_input['child_interest'] = " ";

		// $profile_input['time_spent_on_tv'] = $profile_data->tv_time;
		// $profile_input['tv_whom'] = $profile_data->tv_whom;
		// $profile_input['time_spent_on_radio'] = $profile_data->radio_time;
		// $profile_input['radio_whom'] = $profile_data->radio_whom;
		// $profile_input['time_spent_on_pc'] = $profile_data->computergames_time;
		// $profile_input['pc_whom'] = $profile_data->computergames_whom;
		// $profile_input['child_responsibilty'] = $profile_data->child_responsibilities;
		// $profile_input['responsibility_what'] = $profile_data->child_responsibilities_what;
		// $profile_input['child_playgroup'] = $profile_data->child_play_group;
		// $profile_input['child_playgroup_frequent'] = $profile_data->child_play_group_frequent;
		// $profile_input['child_interest'] = $profile_data->other_interest;

		// $profile_input['sibling_fname1'] = $profile_data->siblingfname1;
		// $profile_input['sibling_lname1'] = $profile_data->siblinglname1;
		// $profile_input['sibling_age1'] = $profile_data->siblingage1;
		// $profile_input['sibling_fname2'] = $profile_data->siblingfname2;
		// $profile_input['sibling_lname2'] = $profile_data->siblinglname2;
		// $profile_input['sibling_age2'] = $profile_data->siblingage2;
		// $profile_input['sibling_fname3'] = $profile_data->siblingfname3;
		// $profile_input['sibling_lname3'] = $profile_data->siblinglname3;
		// $profile_input['sibling_age3'] = $profile_data->siblingage3;
		// $profile_input['sibling_fname4'] = $profile_data->siblingfname4;
		// $profile_input['sibling_lname4'] = $profile_data->siblinglname4;
		// $profile_input['sibling_age4'] = $profile_data->siblingage4;
		// $profile_input['sibling_fname5'] = $profile_data->siblingfname5;
		// $profile_input['sibling_lname5'] = $profile_data->siblinglname5;
		// $profile_input['sibling_age5'] = $profile_data->siblingage5;
		// $profile_input['sibling_fname6'] = $profile_data->siblingfname6;
		// $profile_input['sibling_lname6'] = $profile_data->siblinglname6;
		
		//Additional data if enrollee is primary or preschool
		$profile_input['preschool_age_first_walked'] = isset($profile_data->age_first_walked) ? $profile_data->age_first_walked: NULL;
		$profile_input['preschool_age_first_talked'] = isset($profile_data->age_first_talked) ? $profile_data->age_first_talked: NULL;
		$profile_input['preschool_w_for_urinate'] = isset($profile_data->w_for_urinate) ? $profile_data->w_for_urinate: NULL;
		$profile_input['preschool_w_for_bowel'] = isset($profile_data->w_for_bowel) ? $profile_data->w_for_bowel: NULL;
		$profile_input['preschool_w_usual_time'] = isset($profile_data->w_usual_time) ? $profile_data->w_usual_time: NULL;
		$profile_input['preschool_child_dress'] = isset($profile_data->child_dress) ? $profile_data->child_dress: NULL;
		$profile_input['preschool_child_undress'] = isset($profile_data->child_undress) ? $profile_data->child_undress: NULL;
		$profile_input['preschool_hand_orientation'] = isset($profile_data->hand_orientation) ? $profile_data->hand_orientation: NULL;
		$profile_input['preschool_self_feed'] = isset($profile_data->self_feed) ? $profile_data->self_feed: NULL;
		$profile_input['preschool_stayinplace'] = isset($profile_data->stayinplace) ? $profile_data->stayinplace: NULL;
		$profile_input['preschool_joinothereating'] = isset($profile_data->joinothereating)? $profile_data->joinothereating: NULL;
		$profile_input['preschool_food_preferences'] = isset($profile_data->food_preferences)? $profile_data->food_preferences: NULL;
		$profile_input['preschool_drink_from_bottle'] = isset($profile_data->drink_from_bottle)? $profile_data->drink_from_bottle: NULL;
		$profile_input['preschool_toilet_trained'] = isset($profile_data->toilet_trained)? $profile_data->toilet_trained: NULL;
		$profile_input['preschool_pasttime_watch'] = isset($profile_data->pt_watch)? $profile_data->pt_watch: NULL;
		$profile_input['preschool_pasttime_boardgames'] = isset($profile_data->pt_boardgames)? $profile_data->pt_boardgames: NULL;
		$profile_input['preschool_pasttime_comp'] = isset($profile_data->pt_comp)? $profile_data->pt_comp: NULL;
		$profile_input['preschool_pasttime_reading'] = isset($profile_data->pt_reading)? $profile_data->pt_reading: NULL;
		$profile_input['preschool_pasttime_sports'] = isset($profile_data->pt_sports)? $profile_data->pt_sports: NULL;
		$profile_input['preschool_milkbottle'] = isset($profile_data->pe_dmilk)? $profile_data->pe_dmilk: NULL;
		$profile_input['preschool_studytime'] = isset($profile_data->pe_study)? $profile_data->pe_study: NULL;
		
		//Additional data if enrollee is secondary or gradeschool
		$profile_input['secondary_study_time'] = isset($profile_data->sec_study_time) ? $profile_data->sec_study_time : NULL;
		$profile_input['secondary_study_whom'] = isset($profile_data->sec_study_whom) ? $profile_data->sec_study_whom : NULL;
		$profile_input['secondary_child_pastime'] = isset($profile_data->sec_child_pastime) ? $profile_data->sec_child_pastime : NULL;
		$profile_input['secondary_child_htd'] = isset($profile_data->sec_child_htd) ? $profile_data->sec_child_htd : NULL;
		$profile_input['secondary_child_music'] = isset($profile_data->sec_child_music) ? $profile_data->sec_child_music : NULL;
		$profile_input['secondary_child_hfrus'] = isset($profile_data->sec_child_hfrus) ? $profile_data->sec_child_hfrus : NULL;
		$profile_input['secondary_child_curfew'] = isset($profile_data->sec_child_curfew) ? $profile_data->sec_child_curfew : NULL;
		$profile_input['secondary_talkingl'] = isset($profile_data->sec_talkingl) ? $profile_data->sec_talkingl : NULL;
		$profile_input['secondary_answering'] = isset($profile_data->sec_answering) ? $profile_data->sec_answering : NULL;
		$profile_input['secondary_slamdoor'] = isset($profile_data->sec_slamdoor) ? $profile_data->sec_slamdoor : NULL;
		$profile_input['secondary_misbehave_others'] = isset($profile_data->sec_misbehave_others) ? $profile_data->sec_misbehave_others : NULL;
		$profile_input['secondary_rhle'] = isset($profile_data->sec_rhle) ? $profile_data->sec_rhle : NULL;
		$profile_input['go_home_independently'] = isset($profile_data->al_tghome) ? $profile_data->al_tghome : NULL;
		
		//Additional data if enrollee is intermediate or highschool
		$profile_input['intermediate_watch'] = isset($profile_data->hs_watch) ? $profile_data->hs_watch : NULL;
		$profile_input['intermediate_boardgames'] = isset($profile_data->hs_boardgames) ? $profile_data->hs_boardgames : NULL;
		$profile_input['intermediate_comp'] = isset($profile_data->hs_comp) ? $profile_data->hs_comp : NULL;
		$profile_input['intermediate_reading'] = isset($profile_data->hs_reading) ? $profile_data->hs_reading : NULL;
		$profile_input['intermediate_other_pt'] = isset($profile_data->hs_other_pt) ? $profile_data->hs_other_pt : NULL;
		$profile_input['intermediate_spend_study'] = isset($profile_data->hs_spend_study) ? $profile_data->hs_spend_study : NULL;
		$profile_input['intermediate_first_mens'] = isset($profile_data->hs_first_mens) ? $profile_data->hs_first_mens : NULL;
		$profile_input['intermediate_signal_mens'] = isset($profile_data->hs_signal_mens) ? $profile_data->hs_signal_mens : NULL;
		
		//additional fields for foreign students
		$profile_input['name_ext'] = $profile_data->child_name_ext;
		$profile_input['f_ssp_status'] = $profile_data->nationality_ssp_number;
		$profile_input['f_visa_status'] = $profile_data->nationality_visa_status;
		$profile_input['f_authorized_stay'] = $profile_data->nationality_auth_stay;
		$profile_input['f_passport_no'] = $profile_data->nationality_passport_no;
		$profile_input['f_i_card_no'] = $profile_data->nationality_icard_no;
		$profile_input['f_date_if_issue'] = $profile_data->nationality_date_issued;
		$profile_input['created'] = NOW;
		
		
		//counter for enrollment
		$profile_input['enrollment_resume_key'] = strtolower($this->hs->encrypt(intval(time())+intval(mt_rand(1,99))));
		$profile_input['enrollment_step'] = 'registration form, Personal information';

		// pd($profile_input);
		$this->db->insert('profiles',$profile_input);
		$this->last_insert_id = $this->db->insert_id();
		// $this->_process_siblings($profile_data->sibling,$this->last_insert_id);
		
		if($this->session->set_userdata('forenrollagain') == FALSE)
		{
			$this->session->set_userdata('forenrollagain',$this->last_insert_id);
		}
		
		return $this->db->affected_rows() >= 1 ? TRUE : FALSE;
	}

	private function __format($date)
	{
		$date = DateTime::createFromFormat('m/d/Y', $date);
		if($date)
		{
			$dob = $date->format('Y-m-d');
			$birthday = strtotime($dob);
			$time = time();
			$seconds = $time - $birthday;
			$age = floor($seconds / 31536000);
			return (object)array('dob'=>$dob,'age'=>$age);
		}
	}
	
	private function _process_siblings($data,$p_id = '')
	{
		if(!empty($data))
		{
			foreach($data['firstname']as $k => $v)
			{
				$processed[$k]['firstname'] = $v;
				$processed[$k]['lastname'] = $data['lastname'][$k];
				$processed[$k]['middlename'] = $data['middlename'][$k];
				$processed[$k]['age'] = $data['age'][$k];
				$processed[$k]['p_id'] = $p_id;
				$processed[$k]['ay_id'] = $this->c_sy->id;
				$processed[$k]['created'] = NOW;
			}
			
			$this->db->insert_batch('student_siblings',$processed);
			if($this->db->affected_rows() >= 1 )
			{

			}else{
				log_message('e',"unable to insert student siblings profile id {$p_id}");
			}
		}
	}
	
	public function get_last_insert_id()
	{
		return $this->last_insert_id;
	}
	
	public function create_parent_login($data)
	{
		$for_insert['username'] = $data['parent_username'];
		$for_insert['hashed_password'] = $data['parent_password'];
		$for_insert['userType'] = 'user';
		$for_insert['userStatus'] = 'active';
		$for_insert = (object)$for_insert;
		$res = $this->add_user($for_insert);
		if($res){
			$finished['_username'] = $data['parent_username'];
			$finished['_password'] =  $data['parent_password'];
			$finished['_id'] = $this->last_insert_id;
			$finished['_useremails'][] = (object)['email'=>$data['parent_email']];

			return $finished;
		}else{
			return FALSE;
		}
	}
	
	public function check_if_email_exist($email){
		$em = $this->db->select('username, id')
		->where('username', $email)
		->get('users');
		return $em->num_rows() >= 1 ? $em->row()->id: FALSE;
	}
}
?>