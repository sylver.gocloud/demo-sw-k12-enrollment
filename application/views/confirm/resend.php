<div class="row">
	<div class="large-3 columns">&nbsp;</div>
	
	<div class="large-6 columns enrollment-menu content-views">
		<?=$system_message;?>
		<div class="alert-box">
			Are you already enrolled but confirmation code was not sent? 
			<ul>
				<li>Are you sure you have registered the right email address?</li>
				<li>Have you checked your spam folder?</li>
			</ul>
		</div>
	<?echo form_open('confirm/resend','class="custom"')?>
		<fieldset>
				<legend>Resend Confirmation Code</legend>
				<div class="row collapse">
					<div class="small-3 large-2 columns">
					  <span class="prefix">Email Address</span>
					</div>
					<div class="small-9 large-10 columns">
					  <input type="text" name="email" placeholder="Enter your email Address" required maxlength="45">
					</div>
				</div>
		 </fieldset>
		
		<fieldset>
		<legend>Please Answer Correctly</legend>
		 <div class="row collapse">
			<div class="small-3 large-2 columns">
			  <span class="prefix">Captcha</span>
			</div>
			<div class="small-3 large-5 columns">
			  <input type="text" name="captcha_answer" autocomplete="off" style="border:#ccc 2px solid;" maxlength="5" required>
			</div>
			<div  class="small-3 large-5 columns" >
				<?=$question;?>
			</div>
		</div>
		 </fieldset>
		 <div  class="option">
			<input type="hidden" name="token" value="<?=$token;?>">
			<input type="submit" class="btn btn-primary" name="resend_confirmation_code" value="Resend Confirmation Code">
			<a href="<?=site_url('');?>" class="btn btn-mini btn-warning">Go Back to main menu</a>
		 </div>
	</div>
	
	<?echo form_close()?>
	<div class="large-3 columns">&nbsp;</div>
</div>