<div style="border:3px solid #c0c0c0;padding:10px;">
<h1 style="text-align:center;"><?=ucwords($school_name);?></h1>
<hr>
<p>Congratualtions, Your own parent page is now ready</p>
<p>You can now monitor your childs status and performance online.</p>
<p>The Parents page offers you information on your childs current:
	<ul>
		<li>Grades</li>
		<li>Schedule of subjects.</li>
		<li>Section</li>
		<li>Room</li>
		<li>Billing</li>
		<li>Other Info.</li>
	</ul>
</p>
<p>
	Note: Please do not give away or let anyone use your Page. For security we advise you to change your password or username once you have logged in.
</p>
<div style="border:3px solid #c0c0c0;padding:20px;">
	Your Username is: <strong><?=$username;?></strong><br>
	Your Password is: <strong><?=$password;?></strong>&nbsp;&nbsp;<span>This is case sensitive.</span><br><br>
	Student Portal is : <a href="<?=$link;?>" style="color:#fff;border:1px solid #145F0C;padding:2px;font-size:14px;font-weight:bold;background:#209812;">Go To Student Portal.</a>
</div>
<hr>
<div>
	<p style="text-align:center;"><strong>Notice.</strong></p>
	<p>
	The Management:<br>
	<span style="font-weight:bold;font-size:14px;"><?=ucwords($school_name);?></span></p>
	<p style="text-indent:15px;">This email and any files transmitted with it are confidential and intended solely for the use of
	the individual or entity to whom they are addressed. If you have received this email in error
	please notify the system manager. This message contains confidential information and is
	intended only for the individual named. If you are not the named addressee you should not
	disseminate, distribute or copy this e-mail. Please notify the sender immediately by e-mail if
	you have received this e-mail by mistake and delete this e-mail from your system. If you are not
	the intended recipient you are notified that disclosing, copying, distributing or taking any action
	in reliance on the contents of this information is strictly prohibited.
	</p>
	<p>
	</p>
</div>
<br>
<p>This is a System Generated Message. Do Not Reply.</p>
</div>