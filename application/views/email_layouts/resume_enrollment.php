<div style="border:3px solid #c0c0c0;padding:10px;">
<h1 style="text-align:center;"><?=ucwords($school_name);?></h1>
<hr>
<p>Dear <strong><?=ucwords($name);?></strong></p>
<p>Greetings from the management <strong><?=ucwords($school_name);?></strong></p>
<p style="text-indent:15px;">
Thank you for enrolling at <strong><?=ucwords($school_name);?></strong>. You just made the right choice!
</p>
<br>
<p>
	Your enrollment was successfully saved, you can continue it anytime by clicking the link below.
</p>
<p>
	You will be asked to enter a <i>Security Code</i> to verify you are the right person, your <i>Security Code</i> is:
	<br>
	<div style="display:inline-block;padding:10px 5px;border:2px solid #000;background:#c0c0c0;font-weight:bold;"><?=$code;?></div>
</p>
<div style="text-align:center;margin:5px;">
</h1><a href="<?=$link;?>" style="color:#fff;border:1px solid #145F0C;padding:10px;font-size:14px;font-weight:bold;background:#209812;">Resume Saved Enrollment.</a>
</div>
<br>
<hr>
<br>
<div>
	<p><strong>Notice:</strong></p>
	<p>
	The Management:<br>
	<span style="font-weight:bold;font-size:14px;"><?=ucwords($school_name);?></span></p>
	<p style="text-indent:15px;">This email and any files transmitted with it are confidential and intended solely for the use of
	the individual or entity to whom they are addressed. If you have received this email in error
	please notify the system manager. This message contains confidential information and is
	intended only for the individual named. If you are not the named addressee you should not
	disseminate, distribute or copy this e-mail. Please notify the sender immediately by e-mail if
	you have received this e-mail by mistake and delete this e-mail from your system. If you are not
	the intended recipient you are notified that disclosing, copying, distributing or taking any action
	in reliance on the contents of this information is strictly prohibited.
	</p>
	<p>
	</p>
</div>
<br><br><br>
<p>This is a System Generated Message. Do Not Reply.</p>
</div>