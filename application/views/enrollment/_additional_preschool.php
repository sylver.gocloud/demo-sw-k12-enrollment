<div>
	<legend class="label" style="margin:0 auto;">Student Additional Information</legend>
	<hr style="border:1px solid #2BA6CB;">
	<div  class="row">
		<div class="large-4 columns">
			<label class="radius secondary label">Age First Walked</label>
			<?='<br>'.form_error('age_first_walked');?>
			<input type="text" name="age_first_walked" other="Age First Walked" value="<?=set_value('age_first_walked',isset($sse)? $sse['preschool_age_first_walked']:NULL);?>"   patter="[0-9]">
			<small class="error">This field is required.</small>
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Age First Talked</label>
			<?='<br>'.form_error('age_first_talked');?>
			<input type="text" name="age_first_talked" other="Age First Talked"value="<?=set_value('age_first_talked',isset($sse)? $sse['preschool_age_first_talked']:NULL);?>"  patter="[0-9]">
			<small class="error">This field is required.</small>
		</div>
		<div class="large-4 columns"></div>
	</div>
	
	<div  class="row">
		<div class="large-4 columns">
			<label class="radius secondary label">Words Used For Urination</label>
			<?='<br>'.form_error('w_for_urinate');?>
			<input type="text" name="w_for_urinate" other="Words Used For Urination" value="<?=set_value('w_for_urinate',isset($sse)? $sse['preschool_w_for_urinate']:NULL);?>" >
			<small class="error">This field is required.</small>
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Words Used For Bowel Movement?</label>
			<?='<br>'.form_error('w_for_bowel');?>
			<input type="text" name="w_for_bowel" other="Words Used For Bowel Movement?" value="<?=set_value('w_for_bowel',isset($sse)? $sse['preschool_w_for_bowel']:NULL);?>" >
			<small class="error">This field is required.</small>
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Usual Time</label>
			<?='<br>'.form_error('w_usual_time');?>
			<input type="text" name="w_usual_time" other="Usual Time" value="<?=set_value('w_usual_time',isset($sse)? $sse['preschool_w_usual_time']:NULL);?>" > 
			<small class="error">This field is required.</small>
		</div>
	</div>
	
	<div  class="row">
		<div class="large-6 columns">
			<label class="radius secondary label">Does Child Dress Self</label>
			<?='<br>'.form_error('child_dress');?>
			<?=form_dropdown('child_dress',array(''=>'-- select answer --','no'=>'No','yes'=>'Yes'),set_value('child_dress',isset($sse)? $sse['preschool_child_dress']:NULL),'other="Does Child Dress Self" ');?>
			<small class="error">This field is required.</small>
		</div>
		<div class="large-6 columns">
			<label class="radius secondary label">Does Child Undress Self</label>
			<?='<br>'.form_error('child_undress');?>
			<?=form_dropdown('child_undress',array(''=>'-- select answer --','no'=>'No','yes'=>'Yes'),set_value('child_undress',isset($sse)? $sse['preschool_child_undress']:NULL),'other="Does Child Undress Self" ');?>
			<small class="error">This field is required.</small>
		</div>
	</div>
	
	<div  class="row">
		<div class="large-4 columns">
			<label class="radius secondary label">Child Is </label>
			<?='<br>'.form_error('hand_orientation');?>
			<?=form_dropdown('hand_orientation',array(''=>'-- select answer --','left-handed'=>'Left Handed','right-handed'=>'Right Handed'),set_value('hand_orientation',isset($sse)? $sse['preschool_hand_orientation']:NULL),'other="CHild\'s hand orientation" ');?>
			<small class="error">This field is required.</small>
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Does Child Self Feed</label>
			<?='<br>'.form_error('self_feed');?>
			<?=form_dropdown('self_feed',array(''=>'-- select answer --','no'=>'No','yes'=>'Yes'),set_value('self_feed',isset($sse)? $sse['preschool_self_feed']:NULL),'other="Does Child Self Feed" ');?>
			<small class="error">This field is required.</small>
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Does Child stay in place while eating?</label>
			<?='<br>'.form_error('stayinplace');?>
			<?=form_dropdown('stayinplace',array(''=>'-- select answer --','no'=>'No','yes'=>'Yes'),set_value('stayinplace',isset($sse)? $sse['preschool_stayinplace']:NULL),'other="Does Child stay in place while eating?" ');?>
			<small class="error">This field is required.</small>
		</div>
	</div>
	
	<div  class="row">
		<div class="large-4 columns">
			<label class="radius secondary label">Join Other Family Members When eating?</label>
			<?='<br>'.form_error('joinothereating');?>
			<?=form_dropdown('joinothereating',array(''=>'-- select answer --','no'=>'No','yes'=>'Yes'),set_value('joinothereating',isset($sse)? $sse['preschool_joinothereating']:NULL),'other="Join Other Family Members When eating?" ');?>
			<small class="error">This field is required.</small>
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Food Preferences</label>
			<?='<br>'.form_error('food_preferences');?>
			<input type="text" name="food_preferences" other="Food Preferences" value="<?=set_value('food_preferences',isset($sse)? $sse['preschool_food_preferences']:NULL);?>"> 
			<small class="error">This field is required.</small>
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Does Child still drink from the bottle?</label>
			<?='<br>'.form_error('drink_from_bottle');?>
			<?=form_dropdown('drink_from_bottle',array(''=>'-- select answer --','no'=>'No','yes'=>'Yes'),set_value('drink_from_bottle',isset($sse)? $sse['preschool_drink_from_bottle']:NULL),'other="Does Child still drink from the bottle?" ');?>
			<small class="error">This field is required.</small>
		</div>
	</div>
	
	<div  class="row">
		<div class="large-4 columns">
			<label class="radius secondary label">Is the Child Toilet Trained</label>
			<?='<br>'.form_error('toilet_trained');?>
			<?=form_dropdown('toilet_trained',array(''=>'-- select answer --','no'=>'No','yes'=>'Yes'),set_value('toilet_trained',isset($sse)? $sse['preschool_toilet_trained']:NULL),'other="Is the Child Toilet Trained" ');?>
			<small class="error">This field is required.</small>
		</div>
	</div>
</div>