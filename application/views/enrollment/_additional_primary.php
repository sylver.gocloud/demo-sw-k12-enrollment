<div>	
	<legend class="label" style="margin:0 auto;">Student Additional Information</legend>
	<hr style="border:1px solid #2BA6CB;">
	<div class="large-12 columns panel">
		<h6>FAMILY'S FAVORITE PASTIME: What Exactly?</h1>
	</div>
	<div class="row">
		<div class="large-4 columns">
			<p class="label radius secondary ">Watching T.V./movies/videos</p>
		</div>
		<div class="large-8 columns">
			<?='<br>'.form_error('pt_watch');?>
			<input type="text" name="pt_watch" other="Watching T.V./movies/videos" value="<?=set_value('pt_watch',isset($sse)? $sse['intermediate_watch']:NULL);?>" >
			<small class="error">This field is required.</small>
		</div>
	</div>
	<div class="row">
		<div class="large-4 columns">
			<p class="label radius secondary ">Playing Board Games</p>
		</div>
		<div class="large-8 columns">
			<?='<br>'.form_error('pt_boardgames');?>
			<input type="text" name="pt_boardgames" other="Playing Board Games" value="<?=set_value('pt_boardgames',isset($sse)? $sse['intermediate_boardgames']:NULL);?>" >
			<small class="error">This field is required.</small>
		</div>
	</div>
	
	<div class="row">
		<div class="large-4 columns">
			<p class="label radius secondary ">Computer</p>
		</div>
		<div class="large-8 columns">
			<?='<br>'.form_error('pt_comp');?>
			<input type="text" name="pt_comp" other="Computer" value="<?=set_value('pt_comp',isset($sse)? $sse['intermediate_comp']:NULL);?>" >
			<small class="error">This field is required.</small>
		</div>
	</div>
	
	<div class="row">
		<div class="large-4 columns">
			<p class="label radius secondary ">Reading</p>
		</div>
		<div class="large-8 columns">
			<?='<br>'.form_error('pt_reading');?>
			<input type="text" name="pt_reading" other="Reading" value="<?=set_value('pt_reading',isset($sse)? $sse['intermediate_reading']:NULL);?>" >
			<small class="error">This field is required.</small>
		</div>
	</div>
	
	<div class="row">
		<div class="large-4 columns">
			<p class="label radius secondary ">Physical Activites/Sports</p>
		</div>
		<div class="large-8 columns">
			<?='<br>'.form_error('pt_sports');?>
			<input type="text" name="pt_sports" other="Physical Activites/Sports" value="<?=set_value('pt_sports',isset($sse)? $sse['intermediate_other_pt']:NULL);?>" >
			<small class="error">This field is required.</small>
		</div>
	</div>
	
	<div class="row">
		<div class="large-4 columns">
			<p class="label-good">Does Your child drink milk from the bottle? if Yes Specify how often</p>
		</div>
		<div class="large-8 columns">
			<?='<br>'.form_error('pe_dmilk');?>
			<input type="text" name="pe_dmilk" other="Does Your child drink milk from the bottle? if Yes Specify how often" value="<?=set_value('pe_dmilk',isset($sse)? $sse['preschool_milkbottle']:NULL);?>" >
			<small class="error">This field is required.</small>
		</div>
	</div>
	
	<div class="row">
		<div class="large-4 columns">
			<p class="label-good">Does your child have a regular study time? if Yes specify what time</p>
		</div>
		<div class="large-8 columns">
			<?='<br>'.form_error('pe_study');?>
			<input type="text" name="pe_study" other="Does your child have a regular study time? if Yes specify what time" value="<?=set_value('pe_study',isset($sse)? $sse['intermediate_spend_study']:NULL);?>" >
			<small class="error">This field is required.</small>
		</div>
	</div>
</div>