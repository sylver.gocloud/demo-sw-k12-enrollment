<div>
	<legend class="label" style="margin:0 auto;">Student Additional Information</legend>
	<hr style="border:1px solid #2BA6CB;">
	<div  class="row">
		<div class="large-4 columns">
			<p class="label-good">Does your child have a regular study time? if Yes specify what time and with whom.</p>
		</div>
		<div class="large-4 columns">
			<label class="label">Time:</label><?=form_error('sec_study_time');?>
			<input type="text" other="Does your child have a regular study time?" name="sec_study_time" value="<?=set_value('sec_study_time',isset($sse)? $sse['intermediate_reading']:NULL);?>">
			
		</div>
		<div class="large-4 columns">
			<label class="label">With Whom?:</label><?=form_error('sec_study_whom');?>
			<input type="text" other="Does your child have a regular study time? With Whom" name="sec_study_whom" value="<?=set_value('sec_study_whom',isset($sse)? $sse['intermediate_reading']:NULL);?>">
		</div>
	</div>
	
	<div  class="row">
		<div class="large-4 columns">
			<p class="label-good">Your child's favorite pastime</p>
			
		</div>
		<div class="large-8 columns">
			<?=form_error('sec_child_pastime');?>
			<input type="text" other="Child favorite pastime" name="sec_child_pastime" value="<?=set_value('sec_child_pastime',isset($sse)? $sse['intermediate_reading']:NULL);?>" >
			<small class="error">This field is required.</small>
		</div>
	</div>

	<div  class="row">
		<div class="large-4 columns">
			<p class="label-good">What does your child hate to do:</p>
		</div>
		<div class="large-8 columns">
			<?=form_error('sec_child_htd');?>
			<input type="text" other="What Does your child hate to do" name="sec_child_htd" value="<?=set_value('sec_child_htd',isset($sse)? $sse['intermediate_reading']:NULL);?>" >
			<small class="error">This field is required.</small>
		</div>
	</div>

	<div  class="row">
		<div class="large-4 columns">
			<p class="label-good">Music your child likes to listen to:</p>
		</div>
		<div class="large-8 columns">
			<?=form_error('sec_child_music');?>
			<input type="text" other="Music your child likes to listen" name="sec_child_music" value="<?=set_value('sec_child_music',isset($sse)? $sse['intermediate_reading']:NULL);?>" >
			<small class="error">This field is required.</small>
		</div>
	</div>

	<div  class="row">
		<div class="large-4 columns">
			<p class="label-good">How does your child handle frustration?:</p>
		</div>
		<div class="large-8 columns">
			<?=form_error('sec_child_hfrus');?>
			<input type="text" other="How does your child handle frustration" name="sec_child_hfrus" value="<?=set_value('sec_child_hfrus',isset($sse)? $sse['intermediate_reading']:NULL);?>" >
			<small class="error">This field is required.</small>
		</div>
	</div>
	
	<div  class="row">
		<div class="large-4 columns">
			<p class="label-good">Does your child have a curfew? if Yes please specify:</p>
		</div>
		<div class="large-4 columns">
			<?=form_error('sec_child_curfew');?>
			<input type="text" other="Child Curfew" name="sec_child_curfew" value="<?=set_value('sec_child_curfew',isset($sse)? $sse['intermediate_reading']:NULL);?>">
		</div>
		<div class="large-4 columns">
			<br>
		</div>
	</div>
	
	<div  class="row">
		<div class="large-3 columns">
			<p class="label-good">What do you consider as misbehavior, please check</p>
		</div>
		<div class="large-2 columns">
				<label>Talking Out Loud</label>
				<input type="checkbox" other="Is talking out loud a misbehavior" name="sec_talkingl" value="yes<?php echo set_checkbox('sec_talkingl', 'yes'); ?>">
		</div>
		<div class="large-2 columns">
				<label>Answering Back</label>
				<input type="checkbox" other="Is answering a misbehavior" name="sec_answering" value="yes<?php echo set_checkbox('sec_answering', 'yes'); ?>">
		</div>
		<div class="large-2 columns">
				<label>Slamming The Door</label>
				<input type="checkbox" other="Is slamdoor a misbehavior" name="sec_slamdoor" value="yes<?php echo set_checkbox('sec_slamdoor', 'yes'); ?>">
		</div>
		<div class="large-3 columns">
			<label>Others please specify</label><?=form_error('sec_misbehave_others');?>
			<input type="text" other="Other Misbehavior" name="sec_misbehave_others" value="<?=set_value('sec_misbehave_others',isset($sse)? $sse['intermediate_reading']:NULL);?>">
			
		</div>
	</div>
	
	<div  class="row">
		<div class="large-4 columns">
			<p class="label-good">Rule/s at home that you like the school to reinforce:</p>
		</div>
		<div class="large-8 columns">
			<?=form_error('sec_rhle');?>
			<textarea name="sec_rhle" other="Rules at home you like to reinforce" ><?=set_value('sec_rhle',isset($sse)? $sse['intermediate_reading']:NULL);?></textarea>
			<small class="error">This field is required.</small>
		</div>
	</div>

	<div  class="row">
		<div class="large-4 columns">
			<p class="label-good">Do you allow your child to go home independently after class? Why?</p>
		</div>
		<div class="large-8 columns">
			<?=form_error('al_tghome');?>
			<textarea name="al_tghome" other="Do you allow your child to go home independently after class?" ><?=set_value('al_tghome',isset($sse)? $sse['intermediate_reading']:NULL);?></textarea>
			<small class="error">This field is required.</small>
		</div>
	</div>
</div>