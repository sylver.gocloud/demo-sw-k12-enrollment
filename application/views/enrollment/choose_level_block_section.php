<script type="text/javascript" src="<?=script_url('custom/get_section');?>"></script>
<div class="row">
	<div class="large-2 columns">&nbsp;</div>
	<div class="large-8 columns enrollment-menu content-views">
		<?=$system_message;?>
		<?if($levels):?>
		<?=form_open('level','class="custom" id="check-level-form" data-abide');?>
		<fieldset>
			<p class="small-info">
				Please select a Grade Level. If selected Grade Level does not show a block/ section, click on Next.
			</p>
			<legend class="label">Select Grade Level</legend>
			<label class="label secondary">Grade Level.</label>
			<?=form_dropdown('grade_level',$levels,NULL,'id="level-section" url="'.site_url('output/get_section_data').'" required');?>
			<small class="error">This field is required.</small>
			<!--
			<div class="ajax">
				
			</div>-->
		</fieldset>
		
		<fieldset>
			<legend class="label">Select Block/Section</legend>
				<table class="table" style="width:100%;">
					<thead>
						<tr>
							<td class="center">&nbsp;</td>
							<td class="center">Section Name</td>
							<td class="center">Current Students</td>
							<td class="center">Available Slot</td>
						</tr>
					</thead>
					<tbody class="dynamictable">
						<tr>
							<td colspan="4"><p class="alert alert-info">Please Select a Grade level First.</p></td>
						</tr>
					</tbody>
				</table>
				<input type="hidden" name="sfe_8892" value="<?=$token;?>">
				<input type="hidden" name="level_section" value="true">
				<input type="submit" name="level_section" value="Next" class="btn btn-primary">
		</fieldset>
		<?=form_close();?>
		<?else:?>
			<div class="alert alert-info">
				<p style="font-weight:bold;font-size:18px;">Notice.</p>
				
				<p>We have encountered an unexpected error. <br>
				   System was unable to fetch Grade Levels. <br>
				   We are very sorry for the inconvenience.<br>
				   please contact us at <b><?=$this->school_telephone;?></b> or email us at <b><?=$this->school_email;?></b>
				</p>
				   
			</div>
		<?endif;?>
	</div>
	<div class="large-2 columns">&nbsp;</div>
</div>