<div class="row">
<div class="large-12 columns content-views">
<? echo $system_message;?>
<div class="alert-box">REGISTRATION FORM Part 3: Health History of Child And Discipline</div>
<?$this->load->view('enrollment/notice')?>
<?echo form_open('health','class="custom" data-abide  autocomplete="off"');?>
	<legend class="label" style="margin:0 auto;">HEALTH HISTORY OF CHILD</legend>
	<div class="row">
		<div class="large-12 columns">
			<label class="radius secondary label">What PAST ILLNESSES has the child had?<br>(Please enumerate and specify age it occured)</label>
			<?=form_error('past_illness');?>
			<textarea  name="past_illness" ><?=set_value('past_illness');?></textarea>
			
		</div>
	</div>
	<hr class="clearfix">
	
	<legend class="radius label" style="margin:0 auto;">How frequent does the child have the following?</legend>

	<div class="large-4 columns">
		<label class="radius secondary label">COLDS</label>
		<?=form_error('frequent_colds');?>
		<input type="text" name="frequent_colds" value="<?=set_value('frequent_colds');?>" >
		
	</div>
	<div class="large-4 columns">
		<label class="radius secondary label">HEADACHES</label>
		<?=form_error('frequent_headaches');?>
		<input type="text" name="frequent_headaches" value="<?=set_value('frequent_headaches');?>" >
		
	</div>
	<div class="large-4 columns">
		<label class="radius secondary label">STOMACH ACHES</label>
		<?=form_error('frequent_stomachaches');?>
		<input type="text" name="frequent_stomachaches" value="<?=set_value('frequent_stomachaches');?>" >
		
	</div>
	<div class="large-4 columns">
		<label class="radius secondary label">DIZZINESS</label>
		<?=form_error('frequent_dizziness');?>
		<input type="text" name="frequent_dizziness" value="<?=set_value('frequent_dizziness');?>" >
		
	</div>
	<div class="large-4 columns">
		<label class="radius secondary label">VOMITING</label>
		<?=form_error('frequent_vommiting');?>
		<input type="text" name="frequent_vommiting" value="<?=set_value('frequent_vommiting');?>" >
		
	</div>

	<div class="large-12 columns">
		<label class="radius secondary label">Has your child had any major injury in the past? (eg. limbs,joints,head,spine) if yes,specify</label>
		<?=form_error('major_injury');?>
		<textarea name="major_injury"><?=set_value('major_injury');?></textarea>
	</div>
	<div class="large-12 columns">
		<label class="radius secondary label">Has your child had any major operations? if yes specify.</label>
		<?=form_error('major_operations');?>
		<textarea name="major_operations"><?=set_value('major_operations');?></textarea>
	</div>
	<div class="large-12 columns">
		<label class="radius secondary label">Has your child been diagnosed with any major ailment (eg. heart,epilepsy,asthma) if Yes specify</label>
		<?=form_error('major_ailment');?>
		<textarea name="major_ailment"><?=set_value('major_ailment');?></textarea>
	</div>
	<div class="large-4 columns">
		<label class="radius secondary label">What medication/remedy is your child taking?</label>
		<?=form_error('medication_child_taking');?>
		<input type="text" name="medication_child_taking" value="<?=set_value('medication_child_taking');?>" >
		
	</div>
	<div class="large-12 columns">
		<label class="radius secondary label-good">If your child is suffering from any ailment?, please provide precautionary measures to take.</label>
		<?=form_error('medication_prec');?>
		<input type="text" name="medication_prec" value="<?=set_value('medication_prec');?>" >
		
	</div>
	<div class="large-12 columns">
		<label class="radius secondary label">Is your child allergic to penicillin? aspirin? other medication (specify)?</label>
		<?=form_error('medication_allergy');?>
		<input type="text" name="medication_allergy" value="<?=set_value('medication_allergy');?>" >
		
	</div>
	<div class="large-6 columns">
		<label class="radius secondary label">What is the general eating habit of the child?</label>
		<?=form_error('child_eating_habbit');?>
		<input type="text" name="child_eating_habbit" value="<?=set_value('child_eating_habbit');?>" >
		
	</div>
	<div class="large-6 columns">
		<label class="radius secondary label">Are there any DIETARY RESTRICTIONS or FOOD ALLERGIES?</label>
		<?=form_error('dietary_restrictions');?>
		<input type="text" name="dietary_restrictions" value="<?=set_value('dietary_restrictions');?>" >
		
	</div>
	<div class="large-6 columns">
		<label class="radius secondary label">How do you want the school to help in his/her eating habit?</label>
		<?=form_error('school_help_eating_habbit');?>
		<input type="text" name="school_help_eating_habbit" value="<?=set_value('school_help_eating_habbit');?>" >
		
	</div>
	
	<hr class="clearfix">
	<legend class="label" style="margin:0 auto;">Discipline</legend>
	
	<div>
		<label class="radius secondary label">Methods of discipline used at home (please be specific)</label>
		<?=form_error('methods_of_discp');?>
		<textarea name="methods_of_discp" ><?=set_value('methods_of_discp');?></textarea>
		
	</div>
	<div>
		<label class="radius secondary label">Who handles the discipline at home?</label>
		<?=form_error('who_handles_discp_home');?>
		<input type="text" name="who_handles_discp_home" value="<?=set_value('who_handles_discp_home');?>" >
		
	</div>
	<div>
		<label class="radius secondary label">How effective is the discipline on your child?</label>
		<?=form_error('how_effective_discp');?>
		<input type="text" name="how_effective_discp" value="<?=set_value('how_effective_discp');?>" >
		
	</div>
	<div>
		<label class="radius secondary label">Do you believe in the use of the rod as a form of discipline? Why?</label>
		<?=form_error('rod_discp');?>
		<input type="text" name="rod_discp" value="<?=set_value('rod_discp');?>">
	</div>
	<div>
		<label class="radius secondary label">Will you allow the TEACHER to use the rod in school ONLY WHEN NECESSARY?</label>
		<?=form_error('allow_teacher_use_rod');?>
		<input type="text" name="allow_teacher_use_rod" value="<?=set_value('allow_teacher_use_rod');?>" >
		
	</div>
	<div>
		<label class="radius secondary label">If Not What form of discipline do you suggest we use on your child?</label>
		<?=form_error('what_form_of_discp_use');?>
		<input type="text" name="what_form_of_discp_use" value="<?=set_value('what_form_of_discp_use');?>" >
		
	</div>
	<div>
		<label class="radius secondary label">How can the school be of help in supporting your mode of discipline?</label>
		<?=form_error('school_supp_discp');?>
		<input type="text" name="school_supp_discp" value="<?=set_value('school_supp_discp');?>" >
		
	</div>
	<div>
		<label class="radius label-good">What other information can you provide us regarding your child's behavior, personality or development that you believe would aid us in dealing or enhancing his development?</label>
		<?=form_error('other_info_about_child');?>
		<textarea name="other_info_about_child" ><?=set_value('other_info_about_child');?></textarea>
		
	</div>
	<hr style="border:2px solid #2BA6CB;">
	<div class="row ">
		<div class="large-6 columns">
			<label class="label secondary">Physician</label>
			<input type="text" name="medication_physc_nme" value="<?=set_value('medication_physc_nme');?>" >
			<?=form_error('medication_physc_nme');?>
			
		</div>
		<div class="large-3 columns">
			<label class="label secondary">Telephone No</label>
			<input type="text" name="medication_physc_con" value="<?=set_value('medication_physc_con');?>" >
			<?=form_error('medication_physc_con');?>
			
		</div>
		<div class="large-3 columns">
			<label class="label secondary">Blood Type</label>
			<input type="text" name="medication_bt" value="<?=set_value('medication_bt');?>" >
			<?=form_error('medication_bt');?>
			
		</div>
	</div>
	<div class="row ">
		<div class="large-8 columns">
			<label class="label secondary">Persons authorized to pick up child</label>
			<span class="small-info">Please provide, relationship to the person.</span>
			<?=form_error('auth_pickup_bt');?>
			<textarea name="auth_pickup_bt" ><?=set_value('auth_pickup_bt');?></textarea>
			
		</div>
		<div class="large-4 columns">
		</div>
	</div>
	<div class="clearfix"></div>
	<div>
		<input type="hidden" name="sdj_lefg" value="<?=$token;?>">
		<input type="submit" name="fillup_health" value="Finish Enrollment" class="btn btn-primary">
	</div>
<?php echo form_close(); ?>

</div>
</div>