<script>
	var main = function(){
	    
	   $("#date").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: "yy-mm-dd"
	    });
	    $("#grade").on("change", function(){
	    	var x = $(this);
			if(x.val() == 1 || x.val() == 2)
			{
				var cont = "<div class = 'panel'>"
				    	   		+ "<div class = ''>p0oogee"

				    	   		+ "</div>"
				    	   + "</div>";
				$(".ilagay").append(cont);
			}else{
				$(".ilagay").remove();
			}
	    });
	 //   $("#your_head").append("name");
	    
	//    $("#first").on("keypress", function(e){
	//	var w = String.fromCharCode(e.which);
	//	if(e.which != 8)
	//	{
	//		var x = $("#name").val();
	//    		$("#name").val(x+''+w);
	//	}
	//	else
	//	{
	//		e.preventDefault();	
	//	}
	  //  });

	//    $("#middle").on("keypress", function(e){
	//	var w = String.fromCharCode(e.which);
	//	if(e.which != 8)
	//	{
	//		var x = $("#name").val();
	//    		$("#name").val(x+''+w);
	//	}
	//	else
	//	{
	//		e.preventDefault();	
	//	}
	//    }); 

	//     $("#last").on("keypress", function(e){
	//	var w = String.fromCharCode(e.which);
	//	if(e.which != 8)
	//	{
	//		var x = $("#name").val();
	//    		$("#name").val(x+''+w);
	//		var y = $("#name").val();
	//	}
	//	else
	//	{
	//		e.preventDefault();	
	//	}
	//	
	//	
	//	$("#name").val(y+',');
	//    });
	};
	$(document).ready(main);
</script>
<?php echo form_open('') ?>
	<div class = "panel">
		<?php echo isset($system_message) ? $system_message : '' ?>
		<?=validation_errors()?>
	     <!--<h1 id = "your_head"></h1>-->
	     <div class = "row">
	     	  <div class = "large-2 columns text-right">
		       Name : 
		  </div>

		  <div class = "large-3 columns">
		       <input id = "first" type = "text" class = "form-control" placeholder = "First" name = "first" value = "<?=set_value('first')?>">
		  </div>

		  <div class = "large-3 columns">
		       <input type = "text" id = "middle" class = "form-control" placeholder = "Middle" name = "middle" value = "<?=set_value('middle')?>">
		  </div>

		  <div class = "large-3 columns">
		       <input type = "text" id = "last" class = "form-control" placeholder = "Last" name = "last" value = "<?=set_value('last')?>">
		  </div>
		  <div class = "large-1 columns">
		       <input id = "suffix" type = "text" class = "form-control" placeholder = "Suffix" name = "suffix" value = "<?=set_value('suffix')?>">
		  </div>
	     </div>
	     <!--<div class = "row">
	     	  <div class = "large-2 columns text-right">
		       Display Name : 
		  </div>
		  <div class = "large-6 columns">
		       <input type = "text" id = "name" disabled>
		  </div>

		  <div class = "large-4 columns">
		       Status : New Student Record
		  </div>
	     </div>-->
	     <div class = "row">
	     	  <div class = "large-2 columns text-right">
		       Nickname : 
		  </div>
		  <div class = "large-6 columns">
		       <input type = "text" name = "nick" class = "form-control" value = "<?=set_value('nick')?>">
		  </div>
		  <div class = "large-4 columns">

		  </div>
		  
	     </div>
	     <div class = "row">
	     	  <div class = "large-2 columns text-right">
		       Student Number :
		  </div>
		  <div class = "large-6 columns">
		       <input type = "text" name = "stud_no" class = "form-control"  value = "<?=set_value('stud_no')?>">
		  </div>
		  <div class = "large-4 columns">

		  </div>
	     </div>
	     <div class = "row">
	     	  <div class = "large-2 columns text-right">
		       Birthdate : 
		  </div>
		  
		  <div class = "large-6 columns">
		       <input id = "date" type = "text" name = "bdate" class = "form-control"  value = "<?=set_value('bdate')?>">
		  </div>
		  <div class = "large-4 columns">

		  </div>
	     </div>
	     <div class = "row">
	     	  <div class = "large-2 columns text-right">
		       Gender : 
		  </div>
		  <div class = "large-2 columns">
		       <?php
				$opt = array(
				     'male' => 'Male',
				     'female' => 'Female'
				);
				echo form_dropdown('gender', $opt, set_value('gender'), 'class = "form-control"');
		       ?>
		  </div>
		  <div class = "large-8 columns">

		  </div>
	     </div>
	     <div class = "row">
	     	  <div class = "large-2 columns text-right">
		       Nationality : 
		  </div>
		  <div class = "large-4 columns">
		       <input type = "text" name = "natl" class = "form-control" value = "<?=set_value('natl')?>">
		  </div>

		  <div class = "large-6 colums">

		  </div>
	     </div>
	     <div class = "row">
	     	  <div class = "large-2 columns text-right">
		       Grade : 
		  </div>
		  <div class = "large-3 columns">
		       <?=form_dropdown('level', $level, set_value('level'), 'class = "form-control" id = "grade"') ?>
		  </div>

		  <div class = "large-7 columns">
		  </div>
	     </div>

	</div>

	<div class = "panel">
	     <div class = "row">
	     	  <div class = "large-2 columns text-right">
		       Home Address :
		  </div>
		  <div class = "large-2 columns">
		      <input type = "text" class = "form-control" name = "h_no" placeholder = "House #" value = "<?=set_value('h_no')?>">
		  </div>
		  <div class = "large-2 columns text-right">
		      <input type = "text" class = "form-control" name = "st_no" placeholder = "Street #"  value = "<?=set_value('st_no')?>">	       
		  </div>
		  <div class = "large-3 columns">
		      <input type = "text" class = "form-control" name = "brgy" placeholder = "Barangay" value = "<?=set_value('brgy')?>">
		  </div>
		  <div class = "large-3 columns">
		      <input type = "text" class = "form-control" name = "city" placeholder = "City" value = "<?=set_value('city')?>">
		  </div>
	     </div>
	     <div class = "row">
	     	  <div class = "large-2 columns text-right">
		       Province : 
		  </div>
		  <div class = "large-4 columns">
		       <input type = "text" name = "prov" class = "form-control" value = "<?=set_value('prov')?>">
		  </div>
		  <div class = "large-2 columns text-right">
		       Country : 
		  </div>
		  <div class = "large-4 columns">
		       <input type = "text" name = "country" class = "form-control" value = "<?=set_value('country')?>">
		  </div>
	     </div>
	     <!--
	     <div class = "row">
	     	  <div class = "large-2 columns text-right">
		       State :
		  </div>
		  <div class = "large-2 columns">
		       <input type = "text" name = "state" class = "form-control" value = "<?=set_value('state')?>">
		  </div>
		  <div class = "large-1 columns text-right" style = "padding:2px">
		       Zip Code : 
		  </div>
		  <div class = "large-2 columns">
		       <input type = "text" class = "form-control" name = "zip">
		  </div>
		  <div class = "large-4 columns">
		  </div>
	     </div>
	     -->
	     <div class = "row">
	     	  <div class = "large-2 columns text-right">
		       Email : 
		  </div>
		  
		  <div class = "large-6 columns">
		       <input type = "email" name = "email" class = "form-control" value = "<?=set_value('email')?>">
		  </div>
		  <div class = "large-4 columns">
		  </div>
	     </div>
	     <!--<div class = "row">
	     	  <div class = "large-2 columns text-right">
		       Country : 
		  </div>
		  <div class = "large-4 columns">
		       <input type = "text" name = "country" class = "form-control">
		  </div>
		  <div class = "large-6 columns">
		  </div>
	     </div>-->

	     <div class = "row">
	     	  <div class = "large-12 columns text-right">
		       <button type = "submit" class = "btn btn-success" name = "enroll">Enroll</button>
		  </div>
	     </div>
	</div>
	
	<div class = "ilagay">

	</div>
<?php echo form_close('') ?>