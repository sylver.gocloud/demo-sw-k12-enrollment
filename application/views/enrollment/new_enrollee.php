<style>
	div.enrollment-menu
	{
		padding:20px;
		border:2px dashed #c0c0c0;
	}
	div.option{
		margin:20px 5px;
	}
	ul li{
		list-style:none;
	}
</style>

<?php
$this->load->view('enrollment/enrollment_modal');
$genderAttrib = array('' => 'Select Gender...', 'male' => 'Male', 'female' => 'Female' );
$child_birth_position = array('first'=>'First','Second'=>'Second','third'=>'Third','fourth'=>'Fourth','fifth'=>'Fifth','sixth'=>'Sixth','youngest'=>'youngest','only'=>'Only');
?>
<div class="row">
<div class="alert-box">REGISTRATION FORM</div>
<div class="large-2 columns">&nbsp;</div>
<div class="large-8 columns enrollment-menu content-views">
<? echo $system_message;?>

<?echo form_open('new');?>
<div class="alert-box">
	<i class="icon-info-sign"></i>&nbsp;&nbsp;<strong>Things To Remember</strong>
	<ul>
		<li>&#62; <strong>Please use a valid email Address since your account credentials will be sent their.</strong></li>
		<li>&#62; <strong>New Students Will be required to visit the School for Personal Interview.</strong></li>
</div>
<fieldset>
	<legend class="label">Student Personal Information</legend>

	<div>
		<label class="radius secondary label">Child's Firstname</label>
		<input type="text" name="child_fname" value="<?=set_value('child_fname');?>">
	</div>

	<div>
		<label class="radius secondary label">Child's Middle name</label>
		<input type="text" name="child_mname" value="<?=set_value('child_mname');?>">
	</div>
	<div>
		<label class="radius secondary label">Child's last Name</label>
		<input type="text" name="child_lname" value="<?=set_value('child_lname');?>">
	</div>
	<div>
		<label class="radius secondary label">Child's Nickname</label>
		<input type="text" name="child_nickname" value="<?=set_value('child_nickname');?>">
	</div>
	<div>
		<label class="radius secondary label">Grade Level</label>
		<?php
			echo form_dropdown('level_id', $levels, set_value('level_id'));
			echo form_error('level_id');
		?>
	</div>
	<div>
		<label class="radius secondary label">Childs Birthdate:</label>
		<input type="text" name="child_bdate" class="datepicker" value="<?=set_value('child_bdate');?>">
	</div>
	<div>
		<label class="radius secondary label">Gender</label>
		<?=form_dropdown('gender', $genderAttrib, set_value('gender'));?>
	</div>
	<div>
		<label class="radius secondary label">Place of Birth</label>
		<input type="text" name="child_placeofbirth" value="<?=set_value('child_placeofbirth');?>">
	</div>
	<div>
		<label class="radius secondary label">Nationality</label>
		<input type="text" name="child_nationality" value="<?=set_value('child_nationality');?>">
	</div>
	<div>
		<label class="radius secondary label">Religious Affiliation</label>
		<input type="text" name="child_religous" value="<?=set_value('child_religous');?>">
	</div>
	<div>
		<label class="radius secondary label">City Address</label>
		<input type="text" name="child_city_address" value="<?=set_value('child_city_address');?>">
	</div>
	<div>
		<label class="radius secondary label">Telephone Number</label>
		<input type="text" name="child_telno" value="<?=set_value('child_telno');?>">
	</div>
	<div>
		<label class="radius secondary label">How Long has Child Lived in This Address?</label>
		<input type="text" name="child_adresshowlong" value="<?=set_value('child_adresshowlong');?>">
	</div>
	<div>
		<label class="radius secondary label">School Last Attended</label>
		<input type="text" name="school_last_attended" value="<?=set_value('school_last_attended');?>">
	</div>
	<div>
		<label class="radius secondary label">School Level Completed</label>
		<input type="text" name="school_level_completed" value="<?=set_value('school_level_completed');?>">
	</div>
	<div>
		<label class="radius secondary label">Address Of School</label>
		<input type="text" name="school_address" value="<?=set_value('school_address');?>">
	</div>
	<div>
		<label class="radius secondary label">Father's Full Name</label>
		<input type="text" name="father_name" value="<?=set_value('father_name');?>">
	</div>
	<div>
		<label class="radius secondary label">Father's Age</label>
		<input type="text" name="father_age" value="<?=set_value('father_age');?>">
	</div>
	<div>
		<label class="radius secondary label">Father's Citizenship:</label>
		<input type="text" name="father_citizenship" value="<?=set_value('father_citizenship');?>">
	</div>
	<div>
		<label class="radius secondary label">Fathers's Educational Attainment</label>
		<input type="text" name="father_educ" value="<?=set_value('father_educ');?>">
	</div>
	<div>
		<label class="radius secondary label">Father's Hobbies/Talent</label>
		<input type="text" name="father_talent" value="<?=set_value('father_talent');?>">
	</div>
	<div>
		<label class="radius secondary label">Father's Occupation/Position</label>
		<input type="text" name="father_occup" value="<?=set_value('father_occup');?>">
	</div>
	<div>
		<label class="radius secondary label">Father's Office Address</label>
		<input type="text" name="father_office_address" value="<?=set_value('father_office_address');?>">
	</div>
	<div>
		<label class="radius secondary label">Father's Office Telephone Number</label>
		<input type="text" name="father_office_tel" value="<?=set_value('father_office_tel');?>">
	</div>
	<div>
		<label class="radius secondary label">Mother's Full Name</label>
		<input type="text" name="mother_name" value="<?=set_value('mother_name');?>">
	</div>
	<div>
		<label class="radius secondary label">Mother's Age</label>
		<input type="text" name="mother_age" value="<?=set_value('mother_age');?>">
	</div>
	<div>
		<label class="radius secondary label">Mother's Citizenship</label>
		<input type="text" name="mother_citizenship" value="<?=set_value('mother_citizenship');?>">
	</div>
	<div>
		<label class="radius secondary label">Mother's Educational Attainment</label>
		<input type="text" name="mother_educ" value="<?=set_value('mother_educ');?>">
	</div>
	<div>
		<label class="radius secondary label">Mother's Hobbies/Talent</label>
		<input type="text" name="mother_talent" value="<?=set_value('mother_talent');?>">
	</div>
	<div>
		<label class="radius secondary label">Mother's Occupation/Position</label>
		<input type="text" name="mother_occup" value="<?=set_value('mother_occup');?>">
	</div>
	<div>
		<label class="radius secondary label">Mother's Office Address</label>
		<input type="text" name="mother_office_address" value="<?=set_value('mother_office_address');?>">
	</div>
	<div>
		<label class="radius secondary label">Mother's Office Telephone Number</label>
		<input type="text" name="mother_office_tel" value="<?=set_value('mother_office_tel');?>">
	</div>
	<div>
		<label class="radius secondary label">Guardian's Full Name</label>
		<input type="text" name="guardian_name" value="<?=set_value('guardian_name');?>">
	</div>
	<div>
		<label class="radius secondary label">Relation To Child</label>
		<input type="text" name="guardian_relation" value="<?=set_value('guardian_relation');?>">
	</div>
	<div>
		<label class="radius secondary label">Guardian's Address</label>
		<input type="text" name="guardian_address" value="<?=set_value('guardian_address');?>">
	</div>
	<div>
		<label class="radius secondary label">Guardian's Contact Number</label>
		<input type="text" name="guardian_contact_num" value="<?=set_value('guardian_contact_num');?>">
	</div>
	<div>
		<label class="radius secondary label">Reason for Need of Guardian</label>
		<input type="text" name="guardian_reason" value="<?=set_value('guardian_reason');?>">
	</div>
	<!-- End of personal information -->
</fieldset>


<fieldset>
	<legend class="label">Family History</legend>
	
	<div>
		<label class="radius secondary label">Status of Parents</label>
		<? echo form_dropdown('parent_status',array('married'=>'married',
									   'remarried'=>'remarried',
									   'living together'=>'living together',
									   'separated'=>'separated',
									   'Single Parent'=>'Single Parent'),set_value('parent_status')); ?>
	</div>
	<div>
		<label class="radius secondary label">For How Long</label>
		<input type="text" name="status_how_long" value="<?=set_value('status_how_long');?>">
	</div>
	<div>
		<label class="radius secondary label">(if single parent) Only</label>
		<input type="text" name="right_over_child" value="<?=set_value('right_over_child');?>">
	</div>
	<div>
		<label class="radius secondary label">(separated or single) Do parents have rights over child?</label>
		<input type="text" name="right_over_child" value="<?=set_value('right_over_child');?>">
	</div>
	
		<fieldset>
			<legend>Other Family Members (SIBLINGS ONLY)</legend>
			
			<div>
				<div class="large-8 columns">
					<label class="radius secondary label">Name</label>
					<input type="text" name="sibling1" value="<?=set_value('sibling1');?>">
				</div>
				<div class="large-4 columns">
					<label class="radius secondary label">Age</label>
					<input type="text" name="siblingage1" value="<?=set_value('siblingage1');?>">
				</div>
			</div>
			<div>
				<div class="large-8 columns">
					<label class="radius secondary label">Name</label>
					<input type="text" name="sibling2" value="<?=set_value('sibling2');?>">
				</div>
				<div class="large-4 columns">
					<label class="radius secondary label">Age</label>
					<input type="text" name="siblingage2" value="<?=set_value('siblingage2');?>">
				</div>
			</div>
			<div>
				<div class="large-8 columns">
					<label class="radius secondary label">Name</label>
					<input type="text" name="sibling3" value="<?=set_value('sibling3');?>">
				</div>
				<div class="large-4 columns">
					<label class="radius secondary label">Age</label>
					<input type="text" name="siblingage3" value="<?=set_value('siblingage3');?>">
				</div>
			</div>
			<div>
				<div class="large-8 columns">
					<label class="radius secondary label">Name</label>
					<input type="text" name="sibling4" value="<?=set_value('sibling4');?>">
				</div>
				<div class="large-4 columns">
					<label class="radius secondary label">Age</label>
					<input type="text" name="siblingage4" value="<?=set_value('siblingage4');?>">
				</div>
			</div>
			<div>
				<div class="large-8 columns">
					<label class="radius secondary label">Name</label>
					<input type="text" name="sibling5" value="<?=set_value('sibling5');?>">
				</div>
				<div class="large-4 columns">
					<label class="radius secondary label">Age</label>
					<input type="text" name="siblingage5" value="<?=set_value('siblingage5');?>">
				</div>
			</div>
			<div>
				<div class="large-8 columns">
					<label class="radius secondary label">Name</label>
					<input type="text" name="sibling6" value="<?=set_value('sibling6');?>">
				</div>
				<div class="large-4 columns">
					<label class="radius secondary label">Age</label>
					<input type="text" name="siblingage6" value="<?=set_value('siblingage6');?>">
				</div>
			</div>
		</fieldset>

	<div>
		<label class="radius secondary label">Position of Child in Family</label>
		<?=form_dropdown('position_of_child',$child_birth_position,set_value('position_of_child'));?>
	</div>
	
	<fieldset>
		<div class="large-5 columns">
			<label class="radius secondary label">Is Child Adopted</label>
			<?=form_dropdown('is_child_adopted',array('no'=>'No','yes'=>'Yes'),set_value('is_child_adopted'));?>
		</div>
		<div class="large-5 columns">
			<label class="radius secondary label">if Yes Age of Adoption</label>
			<input type="text" name="age_of_adoption" value="<?=set_value('age_of_adoption');?>">
		</div>
		<div class="clearfix"></div>
		<div class="large-4 columns">
			<label class="radius secondary label">Is the child aware he/she is adopted?</label>
			<?=form_dropdown('child_aware_adopted',array('no'=>'No','yes'=>'Yes'),set_value('child_aware_adopted'));?>
		</div>
	</fieldset>
	
	<fieldset>
		<div class="large-8 columns">
			<label class="radius secondary label">Is the mother presently pregnant?</label>
			<?=form_dropdown('mother_presently_pregnant',array('no'=>'No','yes'=>'Yes'),set_value('mother_presently_pregnant'));?>
		</div>
		<div class="large-3 columns">
			<label class="radius secondary label">if Yes Due date</label>
			<input type="text" name="mother_pregnancy_due_date" value="<?=set_value('mother_pregnancy_due_date');?>">
		</div>
	</fieldset>
	
	<fieldset>
		<div class="large-9 columns">
			<label class="radius secondary label">Has there been any deaths in the immediate family?</label>
			<?=form_dropdown('family_deaths',array('no'=>'No','yes'=>'Yes'),set_value('family_deaths'));?>
		</div>
		<div class="large-3 columns">
			<label class="radius secondary label">Relation to child</label>
			<input type="text" name="family_deaths_relation" value="<?=set_value('family_deaths_relation');?>">
		</div>
		
		
		
		<div class="large-9 columns">
			<label class="radius secondary label">Has there been any serious accidents in the family?</label>
			<?=form_dropdown('family_accidents',array('no'=>'No','yes'=>'Yes'),set_value('family_accidents'));?>
		</div>
		<div class="large-3 columns">
			<label class="radius secondary label">Relation to child</label>
			<input type="text" name="family_accidents_relation" value="<?=set_value('family_accidents_relation');?>">
		</div>
		
		<div>
			<label class="radius secondary label">What?:</label>
			<input type="text" name="what" value="<?=set_value('what');?>">
		</div>
		<div>
			<label class="radius secondary label">When?:</label>
			<input type="text" name="when" value="<?=set_value('when');?>">
		</div>
	</fieldset>
	
	<fieldset>
			<legend>Other Household Members (Relatives,Helpers, etc..)</legend>
			
			<div>
				<div class="large-8 columns">
					<label class="radius secondary label">Name</label>
					<input type="text" name="hhmembers1" value="<?=set_value('hhmembers1');?>">
				</div>
				<div class="large-4 columns">
					<label class="radius secondary label">Age</label>
					<input type="text" name="hhmembersage1" value="<?=set_value('hhmembersage1');?>">
				</div>
			</div>
			<div>
				<div class="large-8 columns">
					<label class="radius secondary label">Name</label>
					<input type="text" name="hhmembers2" value="<?=set_value('hhmembers2');?>">
				</div>
				<div class="large-4 columns">
					<label class="radius secondary label">Age</label>
					<input type="text" name="hhmembers2" value="<?=set_value('hhmembers2');?>">
				</div>
			</div>
			<div>
				<div class="large-8 columns">
					<label class="radius secondary label">Name</label>
					<input type="text" name="hhmembers3" value="<?=set_value('hhmembers3');?>">
				</div>
				<div class="large-4 columns">
					<label class="radius secondary label">Age</label>
					<input type="text" name="hhmembersage3" value="<?=set_value('hhmembersage3');?>">
				</div>
			</div>
			<div>
				<div class="large-8 columns">
					<label class="radius secondary label">Name</label>
					<input type="text" name="hhmembers4" value="<?=set_value('hhmembers4');?>">
				</div>
				<div class="large-4 columns">
					<label class="radius secondary label">Age</label>
					<input type="text" name="hhmembersage4" value="<?=set_value('hhmembersage4');?>">
				</div>
			</div>
			<div>
				<div class="large-8 columns">
					<label class="radius secondary label">Name</label>
					<input type="text" name="hhmembers5" value="<?=set_value('hhmembers5');?>">
				</div>
				<div class="large-4 columns">
					<label class="radius secondary label">Age</label>
					<input type="text" name="hhmembersage5" value="<?=set_value('hhmembersage5');?>">
				</div>
			</div>
			<div>
				<div class="large-8 columns">
					<label class="radius secondary label">Name</label>
					<input type="text" name="hhmembers6" value="<?=set_value('hhmembers6');?>">
				</div>
				<div class="large-4 columns">
					<label class="radius secondary label">Age</label>
					<input type="text" name="hhmembersage6" value="<?=set_value('hhmembersage6');?>">
				</div>
			</div>
		</fieldset>
		
		<div>
			<label class="radius secondary label">Languages Spoken at home?</label>
			<input type="text" name="language_at_home" value="<?=set_value('language_at_home');?>">
		</div>
		
		<div>
			<div class="large-6 columns">
				<label class="radius secondary label">Activites Family Engages in?</label>
				<textarea name="family_activities"><?=set_value('family_activities');?></textarea>
			</div>
			<div class="large-6 columns">
				<label class="radius secondary label">How frequent?</label>
				<textarea name="family_activities_frequent"><?=set_value('family_activities_frequent');?></textarea>
			</div>
		</div>
		
		<fieldset>
			<legend>How much time does the child spend in</legend>
			<div>
				<div class="large-6 columns">
					<label class="radius secondary label">Watching TV or videos in a day?</label>
					<input type="text" name="tv_time" value="<?=set_value('tv_time');?>">
				</div>
				<div class="large-6 columns">
					<label class="radius secondary label">With Whom</label>
					<input type="text" name="tv_whom" value="<?=set_value('tv_whom');?>">
				</div>
			</div>
			<div>
				<div class="large-6 columns">
					<label class="radius secondary label">Listening to radio in a day?</label>
					<input type="text" name="radio_time" value="<?=set_value('radio_time');?>">
				</div>
				<div class="large-6 columns">
					<label class="radius secondary label">With Whom</label>
					<input type="text" name="radio_whom" value="<?=set_value('radio_whom');?>">
				</div>
			</div>
			<div>
				<div class="large-6 columns">
					<label class="radius secondary label">Playing computer games in a day?</label>
					<input type="text" name="computergames_time" value="<?=set_value('');?>">
				</div>
				<div class="large-6 columns">
					<label class="radius secondary label">With Whom</label>
					<input type="text" name="computergames_whom" value="<?=set_value('computergames_whom');?>">
				</div>
			</div>
		</fieldset>
		
		<div>
			<div>
				<label class="radius secondary label">Does the child have any Responsibilities/Duties in home?</label>
				<input type="text" name="child_responsibilities" value="<?=set_value('child_responsibilities');?>">
			</div>
			<div class="large-6 columns">
					<label class="radius secondary label">What are his/her responsibilities?</label>
					<input type="text" name="child_responsibilities_what" value="<?=set_value('child_responsibilities_what');?>">
			</div>
		</div>
		
		<div>
			<div>
				<label class="radius secondary label">Does child have a play group?</label>
				<input type="text" name="child_play_group" value="<?=set_value('child_play_group');?>">
			</div>
			<div class="large-6 columns">
					<label class="radius secondary label">How frequent does child play with group</label>
					<input type="text" name="child_play_group_frequent" value="<?=set_value('child_play_group_frequent');?>">
			</div>
		</div>
		
		<div>
			<label class="radius secondary label">What are the other activites/interest of your child?</label>
			<textarea name="other_interest"><?=set_value('other_interest');?></textarea>
		</div>	
</fieldset>
<div>
	<input type="submit" name="fillup_profile" value="Next" class="btn btn-primary">
</div>
<?php echo form_close(); ?>

</div>
<div class="large-2 columns">&nbsp;</div>
</div>