<?
//prep data for enroll again if enrolled again is enabled
$bdate = dateTime::createFromFormat('Y-m-d',isset($sse)? $sse['dob']:NULL);
if($bdate)
{
	$bd = (object)['month'=>$bdate->format('n'),
			'year'=>$bdate->format('Y'),
			'day'=>$bdate->format('d'),
		   ];

}else{
	$bd = (object)['month'=>1,'year'=>'1990','day'=>'1'];
}

$year = range(1990,2010);
$year_d[''] = 'Year';
foreach($year as $k => $v)
{
	$year_d[$v] = $v;
}
$month = array('jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec');
$count = 1;
$month_d[''] = 'Month';
foreach($month as $v)
{
	$month_d[$count++] = ucwords($v);
}

$genderAttrib = array('' => 'Select Gender...', 'male' => 'Male', 'female' => 'Female' );
$child_birth_position = array(''=>'-- Select --','first'=>'First','Second'=>'Second','third'=>'Third','fourth'=>'Fourth','fifth'=>'Fifth','sixth'=>'Sixth','youngest'=>'youngest','only'=>'Only');
?>
<div class="large-12 columns enrollment-menu content-views">
<?=$system_message;?>
<div class="alert-box">REGISTRATION FORM Part 1: Student Personal Information And Family History</div>
<?echo form_open('enrollment/resume/'.$hash.'/'.$hash2,'id="check-form-submit-resume" class="custom" data-abide autocomplete="off"');?>
<?$this->load->view('enrollment/notice')?>
<!-- start student personal info -->
	<legend class="label" style="margin:0 auto;">Student Personal Information</legend>
	<hr style="border:1px solid #2BA6CB;">
	<div>
		<div class="large-4 columns">
			<label class="radius secondary label">Valid Email Address<span>&#10033;</span></label>
			<?='<br>'.form_error('email');?>
			<input type="email" data-tooltip other="Email Address" class="has-tip tip-top" title="E-Mail Already Set for this student" value="<?=set_value('email',isset($sse)? $sse['email']:NULL);?>" name="email" disabled>
			<input type="hidden" name="email" value="<?=isset($sse)? $sse['email']:NULL?>">
			<small class="error">Email is required and must be a valid email.</small>
		</div>
	</div>
	<div class="clearfix"></div>
	<div>
		<div class="large-4 columns">
			<label class="radius secondary label">First Name<span>&#10033;</span></label>
			<?='<br>'.form_error('child_fname');?>
			<input type="text" name="child_fname" other="childs First name" value="<?=set_value('child_fname',isset($sse)? $sse['firstname']:NULL);?>" required>
			<small class="error">Firstname is required.</small>
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Middle Name<span>&#10033;</span></label>
			<?='<br>'.form_error('child_mname') == ''?'<span style="font-size:10px;font-weight:bold;">If No middle name just put "none"</span>':'<br>'.form_error('child_mname');?>
			<input type="text" name="child_mname" other="Child's Middle name" id="Middlename" value="<?=set_value('child_mname',isset($sse)? $sse['middlename']:NULL);?>" required>
			<small class="error">Middle name is required.</small>
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Last Name<span>&#10033;</span></label>
			<?='<br>'.form_error('child_lname');?>
			<input type="text" name="child_lname" other="Child's Last Name" value="<?=set_value('child_lname',isset($sse)? $sse['lastname']:NULL);?>" required>
			<small class="error">Middle name is required.</small>
		</div>
	</div>
	<div>
		<div class="large-4 columns">
			<label class="radius secondary label">Nickname</label>
			<?='<br>'.form_error('child_nickname');?>
			<input type="text" name="child_nickname" other="child's Nickname/Preffered Name" value="<?=set_value('child_nickname',isset($sse)? $sse['nickname']:NULL);?>">
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Name Ext (e.g jr,II,III)</label>
			<?='<br>'.form_error('child_name_ext');?>
			<input type="text" name="child_name_ext" other="Name Ext" value="<?=set_value('child_name_ext',isset($sse)? $sse['name_ext']:NULL);?>">
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Place of Birth<span>&#10033;</span></label>
			<?='<br>'.form_error('child_placeofbirth');?>
			<input type="text" name="child_placeofbirth" other="Place of Birth" value="<?=set_value('child_placeofbirth',isset($sse)? $sse['pob']:NULL);?>" required>
			<small class="error">Place of Birth is required.</small>
		</div>
	</div>
	<div>
		<div class="large-6 columns">
			<label class="radius secondary label">Childs Birthdate:<span>&#10033;</span></label>
			<?='<br>'.form_error('b_day');?>
			<div class="row">
				<div class="large-4 small-4 columns">
					<?=form_dropdown('b_month',$month_d,set_value('b_month',$bd->month),'required');?>
					<small class="error">Month is required</small>
				</div>
				<div class="large-4 small-4  columns">
					<input type="text" name="b_day" value="<?=set_value('b_day',$bd->day);?>" placeholder="Day" pattern=[0-9] required>
					<small class="error">Day us required and must be numeric</small>
				</div>
				<div class="large-4  small-4  columns">
					<?=form_dropdown('b_year',$year_d,set_value('b_year',$bd->year),'required');?>
					<small class="error">Year is required</small>
				</div>
			</div>
		</div>
		<div class="large-3 columns">
			<label class="radius secondary label">Gender<span>&#10033;</span></label>
			<?='<br>'.form_error('gender');?>
			<?=form_dropdown('gender', $genderAttrib, set_value('gender',isset($sse)? strtolower($sse['gender']):NULL),'other="Gender" required');?>
			<small class="error">Gender is required.</small>
		</div>
		<div class="large-3 columns">
			<label class="radius secondary label">Religious Affiliation<span>&#10033;</span></label>
			<?='<br>'.form_error('child_religous');?>
			<input type="text" name="child_religous" other="Religous Affilation" value="<?=set_value('child_religous',isset($sse)? $sse['religion']:NULL);?>" required>
			<small class="error">Religous Affilation is required.</small>
		</div>
	</div>
	<span class="clearfix"></span>
	<legend class="label" style="margin:15px 0;">Nationality Information</legend>
	<div>
		<div>
			<?=form_error('child_nationality');?>
		</div>
		<div class="clearfix"></div>
		<div class="large-4 columns">
			
			<label class="radius secondary label">Nationality<span>&#10033;</span></label>
			<input type="text" id="nationality" name="child_nationality" other="nationality"  value="<?=set_value('child_nationality',isset($sse)? $sse['nationality']:NULL);?>" required>
			<small class="error">Nationality is required.</small>
		</div>
		
		<div class="clearfix"></div>
		<div id="for-foreign-students" class="hidden">
			<hr class="clearfix" style="border:2px solid #2BA6CB;">
			<legend class="label" style="margin:0 auto;">For Foreign Students ONLY</legend>
			<br class="clearfix">
			<div>
				<div class="large-4 columns">
					<label class="radius secondary label">SSP Number<span>&#10033;</span></label>
					<?='<br>'.form_error('nationality_ssp_number');?>
					<input type="text" name="nationality_ssp_number" class="nationality_must" other="SSP Number (if foreign only)" value="<?=set_value('nationality_ssp_number',isset($sse)? $sse['f_ssp_status']:NULL);?>">
					<small class="error">SSP Number is required.</small>
				</div>
				<div class="large-4 columns">
					<label class="radius secondary label">VISA Status<span>&#10033;</span></label>
					<?='<br>'.form_error('nationality_visa_status');?>
					<input type="text" name="nationality_visa_status" class="nationality_must" other="Visa Status(if foreign only)"value="<?=set_value('nationality_visa_status',isset($sse)? $sse['f_visa_status']:NULL);?>">
					<small class="error">VISA Status is required.</small>
				</div>
				<div class="large-4 columns">
					<label class="radius secondary label">Authorized Stay<span>&#10033;</span></label>
					<?='<br>'.form_error('nationality_auth_stay');?>
					<input type="text" name="nationality_auth_stay" class="nationality_must" other="Authorized stay (if foreign only)" value="<?=set_value('nationality_auth_stay',isset($sse)? $sse['f_authorized_stay']:NULL);?>">
					<small class="error">Authorized stay is required.</small>
				</div>
			</div>
			<div>
				<div class="large-4 columns">
					<label class="radius secondary label">Passport No.<span>&#10033;</span></label>
					<?='<br>'.form_error('nationality_passport_no');?>
					<input type="text" name="nationality_passport_no" class="nationality_must" other="Passport number(if foreign only)" value="<?=set_value('nationality_passport_no',isset($sse)? $sse['f_passport_no']:NULL);?>">
					<small class="error">Passport No. is required.</small>
				</div>
				<div class="large-4 columns">
					<label class="radius secondary label">I-Card No.<span>&#10033;</span></label>
					<?='<br>'.form_error('nationality_icard_no');?>
					<input type="text" name="nationality_icard_no" class="nationality_must" other="I-Card (if foreign only)" value="<?=set_value('nationality_icard_no',isset($sse)? $sse['f_i_card_no']:NULL);?>">
					<small class="error">I-card is required.</small>
				</div>
				<div class="large-4 columns">
					<label class="radius secondary label">Date of Issue<span>&#10033;</span></label>
					<?='<br>'.form_error('nationality_date_issued');?>
					<input type="text" name="nationality_date_issued" class="nationality_must" other="Date of Issue (if foreign only)" class="datepicker" value="<?=set_value('nationality_date_issued',isset($sse)? $sse['f_date_if_issue']:NULL);?>">
					<small class="error">Date of issue is required and must be a valid date MM/DD/YYYY</small>
				</div>
			</div>
			<hr class="clearfix" style="border:2px solid #2BA6CB;">
		</div>
	</div>
	<legend class="label">Current Address Information</legend>
	<div style="margin:10px 0;">
		<div  class="large-4 columns">
			<label class="radius secondary label">City Address<span>&#10033;</span></label>
			<?='<br>'.form_error('child_city_address');?>
			<input type="text" name="child_city_address" other="Current Address" value="<?=set_value('child_city_address',isset($sse)? $sse['present_address']:NULL);?>" required>
			<small class="error">City Address is required.</small>
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Telephone Number<span>&#10033;</span></label>
			<?='<br>'.form_error('child_telno');?>
			<input type="text" name="child_telno" other="Contact Number" value="<?=set_value('child_telno',isset($sse)? $sse['contact_no']:NULL);?>" required>
			<small class="error">Contact Number is required.</small>
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">How Long has Child Lived in This Address?<span>&#10033;</span></label>
			<?='<br>'.form_error('child_adresshowlong');?>
			<input type="text" name="child_adresshowlong" other="How long has child been living in this address" value="<?=set_value('child_adresshowlong',isset($sse)? $sse['how_long_living_present_address']:NULL);?>" required>
			<small class="error">How long child has been living in this address is required.</small>
		</div>
	</div>
	<legend class="label" style="margin:10px 0;">Recent School Information</legend>
	<div>
		<div class="large-4 columns">
			<label class="radius secondary label">School Last Attended<span>&#10033;</span></label>
			<?='<br>'.form_error('school_last_attended');?>
			<input type="text" name="school_last_attended" other="School last attended" value="<?=set_value('school_last_attended',isset($sse)? $sse['last_school_name']:NULL);?>" required>
			<small class="error">This field is required.</small>
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">School Level Completed<span>&#10033;</span></label>
			<?='<br>'.form_error('school_level_completed');?>
			<input type="text" name="school_level_completed" other="School level completed" value="<?=set_value('school_level_completed',isset($sse)? $sse['last_school_level']:NULL);?>" required>
			<small class="error">This field is required.</small>
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Address Of School<span>&#10033;</span></label>
			<?='<br>'.form_error('school_address');?>
			<input type="text" name="school_address" other="School Address" value="<?=set_value('school_address',isset($sse)? $sse['last_school_address']:NULL);?>" required>
			<small class="error">This field is required.</small>
		</div>
	</div>
	<div>
		<legend class="label" style="margin:10px 0;">Information About the Father</legend>
		<div class="row">
			<div class="large-4 columns">
				<label class="radius secondary label">Father's Full Name<span>&#10033;</span></label>
				<?='<br>'.form_error('father_name');?>
				<input type="text" name="father_name" other="Father's Name" value="<?=set_value('father_name',isset($sse)? $sse['fathername']:NULL);?>" required>
				<small class="error">This field is required.</small>
			</div class="large-4 columns">
			<div class="large-4 columns">
				<label class="radius secondary label">Father's Age <span>&#10033;</span></label>
				<?='<br>'.form_error('father_age');?>
				<input type="text" name="father_age"other="Father's Age" maxlength="3" value="<?=set_value('father_age',isset($sse)? $sse['father_age']:NULL);?>" required pattern="[0-9]">
				<small class="error">This field is required and must be numeric.</small>
			</div>
			<div class="large-4 columns">
				<label class="radius secondary label">Father's Religous Affilation:<span>&#10033;</span></label>
				<?=form_error('father_relaffil');?>
				<input type="text" name="father_relaffil" other="Father's Religious Affilation" value="<?=set_value('father_relaffil',isset($sse)? $sse['father_religion']:NULL);?>" required>
				<small class="error">This field is required.</small>
			</div>
		</div>
		<div class="row">
			<div class="large-4 columns">
				<label class="radius secondary label">Father's Citizenship:<span>&#10033;</span></label>
				<?='<br>'.form_error('father_citizenship');?>
				<input type="text" name="father_citizenship" other="Fathers Citizenship" value="<?=set_value('father_citizenship',isset($sse)? $sse['father_nationality']:NULL);?>" required>
				<small class="error">This field is required.</small>
			</div>
			<div class="large-4 columns">
				<label class="radius secondary label">Fathers's Educational Attainment</label>
				<?='<br>'.form_error('father_educ');?>
				<input type="text" name="father_educ" other="Father's Educational Attainment" value="<?=set_value('father_educ',isset($sse)? $sse['father_educ_attain']:NULL);?>" required>
				<small class="error">This field is required.</small>
			</div>
			<div class="large-4 columns">
				<label class="radius secondary label">Father's Hobbies/Talent</label>
				<?='<br>'.form_error('father_talent');?>
				<input type="text" name="father_talent" other="Father's Hobbies/Talent" value="<?=set_value('father_talent',isset($sse)? $sse['father_hobby_talent']:NULL);?>">
			</div>
		</div>
		<div class="row">
			<div class="large-4 columns">
				<label class="radius secondary label">Father's Occupation/Position<span>&#10033;</span></label>
				<?='<br>'.form_error('father_occup');?>
				<input type="text" name="father_occup" other="Father's Occupation/Position" value="<?=set_value('father_occup',isset($sse)? $sse['father_occupation']:NULL);?>" required>
				<small class="error">This field is required.</small>
			</div>
			<div class="large-4 columns">
				<label class="radius secondary label">Father's Office Address<span>&#10033;</span></label>
				<?='<br>'.form_error('father_office_address');?>
				<input type="text" name="father_office_address" other="Father's Office Address" value="<?=set_value('father_office_address',isset($sse)? $sse['father_office_add']:NULL);?>" required>
				<small class="error">This field is required.</small>
			</div>
			<div class="large-4 columns">
				<label class="radius secondary label">Father's Office Telephone Number</label>
				<?='<br>'.form_error('father_office_tel');?>
				<input type="text" name="father_office_tel" other="Father's Office Contact Number" value="<?=set_value('father_office_tel',isset($sse)? $sse['father_office_num']:NULL);?>">
			</div>
		</div>
	</div>
	
	<div>
		<legend class="label" style="margin:10px 0;">Information about the Mother</legend>
		<div class="row">
			<div class="large-4 columns">
				<label class="radius secondary label">Mother's Full Name<span>&#10033;</span></label>
				<?='<br>'.form_error('mother_name');?>
				<input type="text" name="mother_name" other="Mother's Name" value="<?=set_value('mother_name',isset($sse)? $sse['mothername']:NULL);?>" required>
				<small class="error">This field is required.</small>
			</div>
			<div class="large-4 columns">
				<label class="radius secondary label">Mother's Age<span>&#10033;</span></label>
				<?='<br>'.form_error('mother_age');?>
				<input type="text" name="mother_age" other="Mother Age" maxlength="3" value="<?=set_value('mother_age',isset($sse)? $sse['mother_age']:NULL);?>" required pattern="[0-9]">
				<small class="error">This field is required and must be numeric.</small>
			</div>
			<div class="large-4 columns">
				<label class="radius secondary label">Mother's Religious Affilation<span>&#10033;</span></label>
				<?=form_error('mother_relaffil');?>
				<input type="text" name="mother_relaffil" other="Mother's Religious Affilation" value="<?=set_value('mother_relaffil',isset($sse)? $sse['mother_religion']:NULL);?>" required>
				<small class="error">This field is required.</small>
			</div>
		</div>
		<div class="row">
			<div class="large-4 columns">
				<label class="radius secondary label">Mother's Citizenship<span>&#10033;</span></label>
				<?='<br>'.form_error('mother_citizenship');?>
				<input type="text" name="mother_citizenship" other="Mother's Citizenship" value="<?=set_value('mother_citizenship',isset($sse)? $sse['mother_nationality']:NULL);?>" required>
				<small class="error">This field is required.</small>
			</div>
			<div class="large-4 columns">
				<label class="radius secondary label">Mother's Educational Attainment<span>&#10033;</span></label>
				<?='<br>'.form_error('mother_educ');?>
				<input type="text" name="mother_educ" other="Mother's Educational Attainment" value="<?=set_value('mother_educ',isset($sse)? $sse['mother_educ_attain']:NULL);?>" required>
				<small class="error">This field is required.</small>
			</div>
			<div class="large-4 columns">
				<label class="radius secondary label">Mother's Hobbies/Talent</label>
				<?='<br>'.form_error('mother_talent');?>
				<input type="text" name="mother_talent" other="Mother's Hobbies/Talent" value="<?=set_value('mother_talent',isset($sse)? $sse['mother_hobby_talent']:NULL);?>">
			</div>
		</div>
		<div class="row">
			<div class="large-4 columns">
				<label class="radius secondary label">Mother's Occupation/Position<span>&#10033;</span></label>
				<?='<br>'.form_error('mother_occup');?>
				<input type="text" name="mother_occup" other="Mother's Occupation/Position" value="<?=set_value('mother_occup',isset($sse)? $sse['mother_occupation']:NULL);?>" required>
				<small class="error">This field is required.</small>
			</div>
			<div class="large-4 columns">
				<label class="radius secondary label">Mother's Office Address<span>&#10033;</span></label>
				<?='<br>'.form_error('mother_office_address');?>
				<input type="text" name="mother_office_address" other="Mother's Office Address" value="<?=set_value('mother_office_address',isset($sse)? $sse['mother_office_add']:NULL);?>" required>
				<small class="error">This field is required.</small>
			</div>
			<div class="large-4 columns">
				<label class="radius secondary label">Mother's Office Telephone Number</label>
				<?='<br>'.form_error('mother_office_tel');?>
				<input type="text" name="mother_office_tel" other="Mother's Office Telephone Number" value="<?=set_value('mother_office_tel',isset($sse)? $sse['mother_office_num']:NULL);?>">
			</div>
		</div>
	</div>
	
	<div>
		<legend class="label" style="margin:10px 0;">Information about the guardian</legend>
		<div class="row">
			<div class="large-4 columns">
				<label class="radius secondary label">Guardian's Full Name<span>&#10033;</span></label>
				<?='<br>'.form_error('guardian_name');?>
				<input type="text" name="guardian_name" other="Guardian's Full Name" value="<?=set_value('guardian_name',isset($sse)? $sse['guardian_name']:NULL);?>" required>
				<small class="error">This field is required.</small>
			</div>
			<div class="large-4 columns">
				<label class="radius secondary label">Relation To Child<span>&#10033;</span></label>
				<?='<br>'.form_error('guardian_relation');?>
				<input type="text" name="guardian_relation"other="Guardian's Relation to child" value="<?=set_value('guardian_relation',isset($sse)? $sse['relationship']:NULL);?>" required>
				<small class="error">This field is required.</small>
			</div>
			<div class="large-4 columns">
				<label class="radius secondary label">Guardian's Address<span>&#10033;</span></label>
				<?='<br>'.form_error('guardian_address');?>
				<input type="text" name="guardian_address" other="Guardian's Address" value="<?=set_value('guardian_address',isset($sse)? $sse['guardian_address']:NULL);?>" required>
				<small class="error">This field is required.</small>
			</div>
		</div>
		<div class="row">
			<div class="large-4 columns">
				<label class="radius secondary label">Guardian's Contact Number<span>&#10033;</span></label>
				<?='<br>'.form_error('guardian_contact_num');?>
				<input type="text" name="guardian_contact_num" other="Guardian's Contact Number" value="<?=set_value('guardian_contact_num',isset($sse)? $sse['guardian_contact_no']:NULL);?>"required>
				<small class="error">This field is required.</small>
			</div>
			<div class="large-4 columns">
				<label class="radius secondary label">Reason for Need of Guardian<span>&#10033;</span></label>
				<?='<br>'.form_error('guardian_reason');?>
				<input type="text" name="guardian_reason" other="Reason for Need of Guardian" value="<?=set_value('guardian_reason',isset($sse)? $sse['guardian_reason']:NULL);?>"required>
				<small class="error">This field is required.</small>
			</div>
		</div>
	</div>
	<!-- End of personal information -->

	<legend class="label" style="margin:0 auto;">Family History: Submit legal documents to office to validate answer.</legend>
	<hr style="border:1px solid #2BA6CB;">
	<div class="row">
		<div class="large-4 columns">
			<label class="radius secondary label">Status of Parents<span>&#10033;</span></label>
			<?='<br>'.form_error('parent_status');?>
			<? echo form_dropdown('parent_status',array(''=>'--select--','married'=>'married',
										   'remarried'=>'remarried',
										   'living together'=>'living together',
										   'separated'=>'separated',
										   'single parent'=>'Single Parent'),set_value('parent_status',isset($sse)? strtolower($sse['parent_status']):NULL),'others="Parent\'s Status" required'); ?>
			<small class="error">This field is required.</small>							   
		</div>
		<div  class="large-4 columns">
			<label class="radius secondary label">For How Long<span>&#10033;</span></label>
			<?='<br>'.form_error('status_how_long');?>
			<input type="text" name="status_how_long" other="Parent's Status how long" value="<?=set_value('status_how_long',isset($sse)? $sse['parent_status_how_long']:NULL);?>" required>
			<small class="error">This field is required.</small>
		</div>
		<div class="large-4 columns">
				<label class="radius secondary label">Position of Child in Family</label>
				<?='<br>'.form_error('position_of_child');?>
				<?=form_dropdown('position_of_child',$child_birth_position,set_value('position_of_child',isset($sse)? $sse['child_pos']:NULL),'other="Position of Child in the family" required');?>
				<small class="error">This field is required.</small>
		</div>
	</div>
	<div class="row">
		<div class="large-8 columns">
			<label class="label-good">(separated or single) Do parents have rights over child?</label>
			<?=form_error('right_over_child');?>
			<?=form_dropdown('right_over_child',array(''=>'--select--','yes'=>'Yes Both parents have rights','no'=>'no'),set_value('right_over_child',isset($sse)? $sse['parent_rights']:NULL),'other="Rights over child"')?>
		</div>
		<div class="large-4 columns">
			<label class="label-good">If NO Please specifiy who only has right.</label>
			<?=form_error('right_over_child_whom');?>
			<input type="text" name="right_over_child_whom" other="if NO Please specifiy who only has right." value="<?=set_value('right_over_child_whom',isset($sse)? $sse['parents_rights_who']:NULL);?>">
		</div>
	</div>
	
	<div class="row">
		<div class="large-5 columns">
			<label class="radius secondary label">Is Child Adopted</label>
			<?='<br>'.form_error('is_child_adopted');?>
			<?=form_dropdown('is_child_adopted',array(''=>' -- select --','no'=>'No','yes'=>'Yes'),set_value('is_child_adopted',isset($sse)? $sse['child_adopted']:NULL),'other="Is Child adopted" required');?>
			<small class="error">This field is required.</small>
		</div>
		<div class="large-2 columns">
			<label class="radius secondary label">Age of Adoption</label>
			<?='<br>'.form_error('age_of_adoption');?>
			<input type="text" name="age_of_adoption" maxlength="3" other="If adopted age of adoption?" value="<?=set_value('age_of_adoption',isset($sse)? $sse['child_age_adopted']:NULL);?>" pattern="[0-9]">
			<small class="error">Age Must be numeric</small>
		</div>
		<div class="large-5 columns">
			<label class="radius secondary label">Is the child aware he/she is adopted?</label>
			<?='<br>'.form_error('child_aware_adopted');?>
			<?=form_dropdown('child_aware_adopted',array(''=>'-- select --','no'=>'No','yes'=>'Yes'),set_value('child_aware_adopted',isset($sse)? $sse['child_aware_adopted']:NULL),'other="If adopted is child aware he/she is adopted"');?>
		</div>
	</div>
	
	<div class="row">
		<div class="large-6 columns">
			<label class="radius secondary label">Is the mother presently pregnant?</label>
			<?='<br>'.form_error('mother_presently_pregnant');?>
			<?=form_dropdown('mother_presently_pregnant',array(''=>'-- select --','no'=>'No','yes'=>'Yes'),set_value('mother_presently_pregnant',isset($sse)? $sse['mother_present_pregnant']:NULL),'other="Mother presently pregnant" required');?>
			<small class="error">This field is required.</small>
		</div>
		<div class="large-6 columns">
			<label class="radius secondary label">If Yes Due date</label>
			<?='<br>'.form_error('mother_pregnancy_due_date');?>
			<input type="text" name="mother_pregnancy_due_date" other="If mother presently pregnant, due date" value="<?=set_value('mother_pregnancy_due_date',isset($sse)? $sse['mother_due_date']:NULL);?>">
		</div>
	</div>
	
	<div>
		<div class="row">
			<div class="large-6 columns">
				<label class="radius secondary label">Has there been any deaths in the immediate family?</label>
				<?='<br>'.form_error('family_deaths');?>
				<?=form_dropdown('family_deaths',array(''=>'-- select --','no'=>'No','yes'=>'Yes'),set_value('family_deaths',isset($sse)? $sse['family_deaths']:NULL),'other="Any family deaths?"');?>
			</div>
			<div class="large-6 columns">
				<label class="radius secondary label">Relation to child</label>
				<?='<br>'.form_error('family_deaths_relation');?>
				<input type="text" name="family_deaths_relation" other="Relation to child" value="<?=set_value('family_deaths_relation',isset($sse)? $sse['family_deaths_child_relation']:NULL);?>">
			</div>
		</div>
		<div class="row">
			<div class="large-6 columns">
				<label class="radius secondary label">Has there been any serious accidents in the family?</label>
				<?='<br>'.form_error('family_accidents');?>
				<?=form_dropdown('family_accidents',array(''=>'--select--','no'=>'No','yes'=>'Yes'),set_value('family_accidents',isset($sse)? $sse['family_accidents']:NULL),'other="Any serious accidents in the family"');?>
			</div>
			<div class="large-6 columns">
				<label class="radius secondary label">Relation to child</label>
				<?='<br>'.form_error('family_accidents_relation');?>
				<input type="text" name="family_accidents_relation" other="Relation to child" value="<?=set_value('family_accidents_relation',isset($sse)? $sse['family_accidents_child_relation']:NULL);?>">
			</div>
		</div>
		
		<div class="clearfix"></div>
		<div class="row">
			<div class="large-6 columns">
				<label class="radius secondary label">What?:</label>
				<?='<br>'.form_error('what');?>
				<input type="text" name="what" other="what" value="<?=set_value('what',isset($sse)? $sse['what']:NULL);?>">
			</div>
			<div  class="large-6 columns">
				<label class="radius secondary label">When?:</label>
				<?='<br>'.form_error('when');?>
				<input type="text" name="when" other="when" value="<?=set_value('when',isset($sse)? $sse['when']:NULL);?>">
			</div>
		</div>
	</div>
	
	<div>
	<legend class="label" style="margin:10px 0;">Other Household Members (Relatives,Helpers, etc..)</legend>
	<div class="row">
		<div class="large-8 columns">
			<label class="radius secondary label">Name</label>
			<?='<br>'.form_error('hhmembers1');?>
			<input type="text" name="hhmembers1" other="Other house hold members name" value="<?=set_value('hhmembers1',isset($sse)? $sse['other_household_member1']:NULL);?>">
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Age</label>
			<?='<br>'.form_error('hhmembersage1');?>
			<input type="text" name="hhmembersage1" other="Other house hold members age" maxlength="3" value="<?=set_value('hhmembersage1',isset($sse)? $sse['other_household_age1']:NULL);?>" pattern="[0-9]">
			<small class="error">Age must be numeric</small>
		</div>
	</div>
	<div class="row">
		<div class="large-8 columns">
			<label class="radius secondary label">Name</label>
			<?='<br>'.form_error('hhmembers2');?>
			<input type="text" name="hhmembers2" other="Other house hold members name" value="<?=set_value('hhmembers2',isset($sse)? $sse['other_household_member2']:NULL);?>">
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Age</label>
			<?='<br>'.form_error('hhmembersage2');?>
			<input type="text" name="hhmembersage2" other="Other house hold members age" maxlength="3" value="<?=set_value('hhmembersage2',isset($sse)? $sse['other_household_age2']:NULL);?>" pattern="[0-9]">
			<small class="error">Age must be numeric</small>
		</div>
	</div>
	<div class="row">
		<div class="large-8 columns">
			<label class="radius secondary label">Name</label>
			<?='<br>'.form_error('hhmembers3');?>
			<input type="text" name="hhmembers3" other="Other house hold members name" value="<?=set_value('hhmembers3',isset($sse)? $sse['other_household_member3']:NULL);?>">
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Age</label>
			<?='<br>'.form_error('hhmembersage3');?>
			<input type="text" name="hhmembersage3" other="Other house hold members age" maxlength="3" value="<?=set_value('hhmembersage3',isset($sse)? $sse['other_household_age3']:NULL);?>">
		</div>
	</div>
	<div class="row">
		<div class="large-8 columns">
			<label class="radius secondary label">Name</label>
			<?='<br>'.form_error('hhmembers4');?>
			<input type="text" name="hhmembers4" other="Other house hold members name" value="<?=set_value('hhmembers4',isset($sse)? $sse['other_household_member4']:NULL);?>">
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Age</label>
			<?='<br>'.form_error('hhmembersage4');?>
			<input type="text" name="hhmembersage4" other="Other house hold members age" maxlength="3" value="<?=set_value('hhmembersage4',isset($sse)? $sse['other_household_age4']:NULL);?>" pattern="[0-9]">
			<small class="error">Age must be numeric.</small>
		</div>
	</div>
	<div class="row">
		<div class="large-8 columns">
			<label class="radius secondary label">Name</label>
			<?='<br>'.form_error('hhmembers5');?>
			<input type="text" name="hhmembers5" other="Other house hold members name" value="<?=set_value('hhmembers5',isset($sse)? $sse['other_household_member5']:NULL);?>">
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Age</label>
			<?='<br>'.form_error('hhmembersage5');?>
			<input type="text" name="hhmembersage5"other="Other house hold members age" maxlength="3" value="<?=set_value('hhmembersage5',isset($sse)? $sse['other_household_age5']:NULL);?>"pattern="[0-9]">
			<small class="error">Age must be numeric.</small>
		</div>
	</div>
	<div class="row">
		<div class="large-8 columns">
			<label class="radius secondary label">Name</label>
			<?='<br>'.form_error('hhmembers6');?>
			<input type="text" name="hhmembers6" other="Other house hold members name" value="<?=set_value('hhmembers6',isset($sse)? $sse['other_household_member6']:NULL);?>">
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Age</label>
			<?='<br>'.form_error('hhmembersage6');?>
			<input type="text" name="hhmembersage6" other="Other house hold members age" maxlength="3" value="<?=set_value('hhmembersage6',isset($sse)? $sse['other_household_age6']:NULL);?>" pattern="[0-9]">
			<small class="error">Age must be numeric.</small>
		</div>
	</div>
	</div>
	<div>
		<label class="radius secondary label">Languages Spoken at home?</label>
		<?='<br>'.form_error('language_at_home');?>
		<input type="text" name="language_at_home"other="Languages spoken at home" value="<?=set_value('language_at_home',isset($sse)? $sse['languages_spoken_at_home']:NULL);?>" required>
		<small class="error">This field is required.</small>
	</div>
	
	<div class="row">
	<div>
		<div class="large-6 columns">
			<label class="radius secondary label">Activites Family Engages in?</label>
			<?='<br>'.form_error('family_activities');?>
			<textarea name="family_activities" other="Activites Family Engages in?"><?=set_value('family_activities',isset($sse)? $sse['family_activities']:NULL);?></textarea>
		</div>
		<div class="large-6 columns">
			<label class="radius secondary label">How frequent?</label>
			<?='<br>'.form_error('family_activities_frequent');?>
			<textarea name="family_activities_frequent" other="How frequent?"><?=set_value('family_activities_frequent',isset($sse)? $sse['family_activities_frequent']:NULL);?></textarea>
		</div>
	</div>
	</div>
		
	<div>
		<legend class="label" style="margin:15px 0;">How much time does the child spend in</legend>
		<div>
			<div class="large-6 columns">
				<label class="radius secondary label">Watching TV or videos in a day?</label>
				<?='<br>'.form_error('tv_time');?>
				<input type="text" name="tv_time" other="Child spend watching TV" value="<?=set_value('tv_time',isset($sse)? $sse['time_spent_on_tv']:NULL);?>">
			</div>
			<div class="large-6 columns">
				<label class="radius secondary label">With Whom</label>
				<?='<br>'.form_error('tv_whom');?>
				<input type="text" name="tv_whom" other="Child spend watching TV with whom" value="<?=set_value('tv_whom',isset($sse)? $sse['tv_whom']:NULL);?>">
			</div>
		</div>
		<div>
			<div class="large-6 columns">
				<label class="radius secondary label">Listening to radio in a day?</label>
				<?='<br>'.form_error('radio_time');?>
				<input type="text" name="radio_time" other="Listening to radio in a day?" value="<?=set_value('radio_time',isset($sse)? $sse['time_spent_on_radio']:NULL);?>">
			</div>
			<div class="large-6 columns">
				<label class="radius secondary label">With Whom</label>
				<?='<br>'.form_error('radio_whom');?>
				<input type="text" name="radio_whom" other="Listening to radio in a day? with whom" value="<?=set_value('radio_whom',isset($sse)? $sse['radio_whom']:NULL);?>">
			</div>
		</div>
		<div>
			<div class="large-6 columns">
				<label class="radius secondary label">Playing computer games in a day?</label>
				<?='<br>'.form_error('computergames_time');?>
				<input type="text" name="computergames_time" other="Playing computer games in a day?" value="<?=set_value('computergames_time',isset($sse)? $sse['time_spent_on_pc']:NULL);?>">
			</div>
			<div class="large-6 columns">
				<label class="radius secondary label">With Whom</label>
				<?='<br>'.form_error('computergames_whom');?>
				<input type="text" name="computergames_whom"  other="Playing computer games in a day? with whom"  value="<?=set_value('computergames_whom',isset($sse)? $sse['pc_whom']:NULL);?>">
			</div>
		</div>
	</div>
		
		<div>
			<div class="large-6 columns">
				<label class="radius secondary label">Does the child have any Responsibilities/Duties in home?</label>
				<?='<br>'.form_error('child_responsibilities');?>
				<input type="text" name="child_responsibilities"  other="Child have any responsibilities at home"  value="<?=set_value('child_responsibilities',isset($sse)? $sse['child_responsibilty']:NULL);?>">
			</div>
			<div class="large-6 columns">
					<label class="radius secondary label">What are his/her responsibilities?</label>
					<?='<br>'.form_error('child_responsibilities_what');?>
					<input type="text" name="child_responsibilities_what" other="What are his/her responsibilities" value="<?=set_value('child_responsibilities_what',isset($sse)? $sse['responsibility_what']:NULL);?>">
			</div>
		</div>
		<div>
			<div class="large-6 columns">
				<label class="radius secondary label">Does child have a play group?</label>
				<?='<br>'.form_error('child_play_group');?>
				<?=form_dropdown('child_play_group',array(''=>'-- select --','no'=>'No','yes'=>'Yes'),set_value('child_play_group',isset($sse)? $sse['child_playgroup']:NULL),'other="Child has playgroup?"');?>
			</div>
			<div class="large-6 columns">
					<label class="radius secondary label">How frequent does child play with group</label>
					<?='<br>'.form_error('child_play_group_frequent');?>
					<input type="text" name="child_play_group_frequent" other="How frequent does child play with playgroup" value="<?=set_value('child_play_group_frequent',isset($sse)? $sse['child_playgroup_frequent']:NULL);?>">
			</div>
		</div>
		<div class="clearfix"></div>
		<div>
			<label class="radius secondary label">What are the other activites/interest of your child?</label>
			<?='<br>'.form_error('other_interest');?>
			<textarea name="other_interest" other="What are the other activites/interest of your child?"><?=set_value('other_interest',isset($sse)? $sse['child_interest']:NULL);?></textarea>
		</div>	

<!-- 
==============================================================
ADDITIONAL FIELDS IF Preschool
==============================================================
-->

<?if(isset($preschool)):?>
	<?$this->load->view('enrollment/_additional_preschool');?>
<?endif;?>
<!-- 
==============================================================
ADDITIONAL FIELDS IF Primary
==============================================================
-->

<?if(isset($primary)):?>
	<?$this->load->view('enrollment/_additional_primary');?>
<?endif;?>

<!-- 
==============================================================
ADDITIONAL FIELDS IF Grade School OR Secondary
==============================================================
-->
<?if(isset($gradeschool)):?>
	<?$this->load->view('enrollment/_additional_secondary');?>
<?endif;?>


<!-- 
==============================================================
ADDITIONAL FIELDS IF High School or Intermediate
==============================================================
-->
<?if(isset($highschool)):?>
	<?$this->load->view('enrollment/_additional_highschool');?>
<?endif;?>
<hr class="clearfix">
<!--
---
---
DEVELOPMENTAL HISTORY
---
---
-->
<div class="alert-box">REGISTRATION FORM Part 2: Developmental History of Child And Childhood Experiences</div>
	<legend class="label" style="margin:0 auto;">Developmental History Of Child</legend>		
	<div class="row">
		<div class="large-8 small-12 columns">
			<label class="radius secondary label">Length of Pregnancy<span>&#10033;</span></label>
			<?=form_error('length_of_preg');?>
			<input type="text" name="length_of_preg" value="<?=set_value('length_of_preg',isset($sse)? $sse['dhc_pregnancy_length']:NULL);?>" required>
			<small class="error">This field is required.</small>
		</div>
		<div class="large-4 small-12 columns">
			<label class="radius secondary label">Form of Delivery<span>&#10033;</span></label>
			<?=form_error('form_of_del');?>
			<input type="text" name="form_of_del" value="<?=set_value('form_of_del',isset($sse)? $sse['dhc_delivery_form']:NULL);?>" required>
			<small class="error">This field is required.</small>
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns">
		<label class="radius secondary label">Complications before? during? after? delivery?<span>&#10033;</span></label>
		<?=form_error('complications');?>
		<input type="text" name="complications" value="<?=set_value('complications',isset($sse)? $sse['dhc_compications']:NULL);?>" required>
		<small class="error">This field is required.</small>
		</div>
	</div>
	<div class="row">
		<div class="large-6 columns">
			<label class="radius secondary label">What time does the child go to bed?<span>&#10033;</span></label>
			<?=form_error('go_to_bed');?>
			<input type="text" name="go_to_bed" value="<?=set_value('go_to_bed',isset($sse)? $sse['dhc_sleep_time']:NULL);?>" required>
			<small class="error">This field is required.</small>
		</div>
		<div class="large-6 columns">
			<label class="radius secondary label">Get up from bed?<span>&#10033;</span></label>
			<?=form_error('get_up_bed');?>
			<input type="text" name="get_up_bed" value="<?=set_value('get_up_bed',isset($sse)? $sse['dhc_wakup_time']:NULL);?>" required>
			<small class="error">This field is required.</small>
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns">
			<label class="radius secondary label">Does the child have any sleeping disturbance? if yes what?</label>
			<?=form_error('sleeping_dist');?>
			<input type="text" name="sleeping_dist" value="<?=set_value('sleeping_dist',isset($sse)? $sse['dhc_sleeping_disturbance_what']:NULL);?>" >
		</div>
	</div>
	
	<div class="row">
			<div class="large-6 columns">
				<label class="radius secondary label">Does the child have own room?</label>
				<?=form_error('own_room');?>
				<?=form_dropdown('own_room',array(''=>'-- select --','yes'=>'Yes','no'=>'No'),set_value('own_room',isset($sse)? $sse['dhc_child_own_room']:NULL),'required');?>
				<small class="error">This field is required.</small>
			</div>
			<div class="large-6 columns">
				<label class="radius secondary label">If not, shares with whom?</label>
				<?=form_error('room_shares');?>
				<input type="text" name="room_shares" value="<?=set_value('room_shares',isset($sse)? $sse['dhc_room_shares_who']:NULL);?>">
			</div>
	</div>
	<div class="row">
		<div class="large-4 columns">
			<label class="radius secondary label">Does the child bed wet?<span>&#10033;</span></label>
			<?=form_error('wet_bed');?>
			<?=form_dropdown('wet_bed',array(''=>'-- select --','yes'=>'Yes','no'=>'No'),set_value('wet_bed',isset($sse)? $sse['dhc_child_wet_bed']:NULL),'required');?>
			<small class="error">This field is required.</small>
		</div>
		<div class="clearfix"></div>
	</div>
	<hr class="clearfix">
	<div class="row">
		<legend class="radius label" style="margin:0 auto;">Are there any observable difficulties or defects in your child's growth?<span>&#10033;</span></legend>
		<div class="large-12 columns">
			<label class="radius secondary label">Specify:<span>&#10033;</span></label>
			<?=form_error('observable_difficulties');?>
			<input type="text"  name="observable_difficulties" value="<?=set_value('observable_difficulties',isset($sse)? $sse['dhc_defect_specify']:NULL);?>" >
		</div>
		<div class="large-12 columns">
			<label class="radius secondary label">Since When?<span>&#10033;</span></label>
			<?=form_error('observable_difficulties_since_when');?>
			<input type="text"  name="observable_difficulties_since_when" value="<?=set_value('since_when',isset($sse)? $sse['dhc_defect_when']:NULL);?>">
		</div>
		<div class="large-12 columns">
			<label class="radius secondary label">Action Taken:<span>&#10033;</span></label>
			<?=form_error('observable_difficulties_action_taken');?>
			<input type="text"  name="observable_difficulties_action_taken" value="<?=set_value('observable_difficulties_action_taken',isset($sse)? $sse['dhc_defect_actions']:NULL);?>">
		</div>
	</div>
	<hr class="clearfix">
	<div class="row">
		<legend class="radius label" style="margin:0 auto;">Are there any speech problems?</legend>
		<div class="large-12 columns">
			<label class="radius secondary label">Specify:<span>&#10033;</span></label>
			<?=form_error('speech_problems');?>
			<input type="text"  name="speech_problems" value="<?=set_value('speech_problems',isset($sse)? $sse['dhc_speech_problem_what']:NULL);?>" >
		</div>
		<div class="large-12 columns">
			<label class="radius secondary label">Since When?<span>&#10033;</span></label>
			<?=form_error('speech_problems_since_when');?>
			<input type="text"  name="speech_problems_since_when" value="<?=set_value('speech_problems_since_when',isset($sse)? $sse['dhc_speech_problem_when']:NULL);?>" >
		</div>
		<div class="large-12 columns">
			<label class="radius secondary label">Action Taken:<span>&#10033;</span></label>
			<?=form_error('speech_problems_actions_taken');?>
			<input type="text"  name="speech_problems_actions_taken" value="<?=set_value('speech_problems_actions_taken',isset($sse)? $sse['dhc_speech_problem_actions']:NULL);?>">
		</div>
	</div>
	<hr class="clearfix">
	<div class="row">
		<legend class="radius label" style="margin:0 auto;">Are there any hearing problems?</legend>
		<div class="large-12 columns">
			<label class="radius secondary label">When was the hearing of Child last checked?<span>&#10033;</span></label>
			<?=form_error('hearing_last_checked');?>
			<input type="text"  name="hearing_last_checked" value="<?=set_value('hearing_last_checked',isset($sse)? $sse['dhc_hearing_last_check']:NULL);?>">
		</div>
		<div class="large-12 columns">
			<label class="radius secondary label">Specify:<span>&#10033;</span></label>
			<?=form_error('hearing_problems');?>
			<input type="text"  name="hearing_problems" value="<?=set_value('hearing_problems',isset($sse)? $sse['dhc_hearing_problems_what']:NULL);?>">
		</div>
		<div class="large-12 columns">
			<label class="radius secondary label">Since When?<span>&#10033;</span></label>
			<?=form_error('hearing_problems_since_when');?>
			<input type="text"  name="hearing_problems_since_when" value="<?=set_value('hearing_problems_since_when',isset($sse)? $sse['dhc_hearing_problems_when']:NULL);?>">
		</div>
		<div class="large-12 columns">
			<label class="radius secondary label">Action Taken:<span>&#10033;</span></label>
			<?=form_error('hearing_problems_actions_taken');?>
			<input type="text"  name="hearing_problems_actions_taken" value="<?=set_value('hearing_problems_actions_taken',isset($sse)? $sse['dhc_hearing_problem_actions']:NULL);?>">
		</div>
	</div>
	<hr class="clearfix">		
	<div class="row">
		<legend class="radius label" style="margin:0 auto;">Are there any sight problems?<span>&#10033;</span></legend>
		<div class="large-12 columns">
			<label class="radius secondary label" >When was the vision of Child last checked?</label>
			<?=form_error('vision_last_checked');?>
			<input type="text"  name="vision_last_checked" value="<?=set_value('vision_last_checked',isset($sse)? $sse['dhc_vision_checked']:NULL);?>">
		</div>
		<div class="large-12 columns">
			<label class="radius secondary label">Specify:<span>&#10033;</span></label>
			<?=form_error('vision_specify');?>
			<input type="text"  name="vision_specify" value="<?=set_value('vision_specify',isset($sse)? $sse['dhc_sight_problems_what']:NULL);?>">
		</div>
	</div>
		
		<div>
			<label class="radius secondary label-good">Are there any information that we should be aware of that may hinder the child's learning process or general development?<span>&#10033;</span></label>
			<?=form_error('hinder_childs_learning');?>
			<textarea name="hinder_childs_learning" required><?=set_value('hinder_childs_learning',isset($sse)? $sse['dhc_hinder_learning_process']:NULL);?></textarea>
			<small class="error">This field is required.</small>
		</div>

<hr class="clearfix">	
	<legend class="label" style="margin:0 auto;">Childhood Experiences</legend>
	<br>
	<div>
		<label class="radius secondary label">Has the child encountered any traumatic experiences? if yes specify<span>&#10033;</span></label>
		<?=form_error('traumatic_experiences');?>
		<textarea name="traumatic_experiences"><?=set_value('traumatic_experiences',isset($sse)? $sse['ce_trauma_exp_what']:NULL);?></textarea>
	</div>
	<div>
		<div class="large-6 columns">
			<label class="radius secondary label">Age<span>&#10033;</span></label>
			<?=form_error('age_trauma');?>
			<input type="text" name="age_trauma" value="<?=set_value('age_trauma',isset($sse)? $sse['ce_trauma_exp_age']:NULL);?>">
		</div>
		<div class="large-6 columns">
			<label class="radius secondary label">Reaction<span>&#10033;</span></label>
			<?=form_error('trauma_reaction');?>
			<textarea name="trauma_reaction" ><?=set_value('trauma_reaction',isset($sse)? $sse['ce_trauma_exp_reaction']:NULL);?></textarea>
		</div>
	</div>
	
	<div>
		<label class="radius secondary label">How has it affected the child?<span>&#10033;</span></label>
		<?=form_error('how_trauma_affected_child');?>
		<textarea name="how_trauma_affected_child" ><?=set_value('how_trauma_affected_child',isset($sse)? $sse['ce_trauma_exp_affected']:NULL);?></textarea>
	</div>
	
	<div>
		<label class="radius secondary label">does the child have any special fears?if yes what?<span>&#10033;</span></label>
		<?=form_error('special_fears');?>
		<textarea name="special_fears" ><?=set_value('special_fears',isset($sse)? $sse['ce_special_fears_what']:NULL);?></textarea>
	</div>
	<div>
		<label class="radius secondary label">Since when? What has triggered the fear?<span>&#10033;</span></label>
		<?=form_error('when_what_triggered_fear');?>
		<textarea name="when_what_triggered_fear"><?=set_value('when_what_triggered_fear',isset($sse)? $sse['ce_special_fears_when']:NULL);?></textarea>
	</div>
	<div>
		<label class="radius secondary label">How do you handle it?<span>&#10033;</span></label>
		<?=form_error('how_do_you_handle');?>
		<textarea name="how_do_you_handle"><?=set_value('how_do_you_handle',isset($sse)? $sse['ce_special_fears_handle']:NULL);?></textarea>
	</div>
<!--
---
---
HEALTH HISTORY OF CHILD AND DISCIPLINE
---
---
-->


<div class="alert-box">REGISTRATION FORM Part 3: Health History of Child And Discipline</div>

	<legend class="label" style="margin:0 auto;">HEALTH HISTORY OF CHILD</legend>
	<div class="row">
		<div class="large-12 columns">
			<label class="radius secondary label">What PAST ILLNESSES has the child had?<br>(Please enumerate and specify age it occured)</label>
			<?=form_error('past_illness');?>
			<textarea  name="past_illness" required><?=set_value('past_illness',isset($sse)? $sse['hhc_past_illness']:NULL);?></textarea>
			<small class="error">This Field is required.</small>
		</div>
	</div>
	<hr class="clearfix">
	
	<legend class="radius label" style="margin:0 auto;">How frequent does the child have the following?</legend>

	<div>
		<label class="radius secondary label">COLDS</label>
		<?=form_error('frequent_colds');?>
		<input type="text" name="frequent_colds" value="<?=set_value('frequent_colds',isset($sse)? $sse['hhc_frequent_colds']:NULL);?>" required>
		<small class="error">This Field is required.</small>
	</div>
	<div>
		<label class="radius secondary label">HEADACHES</label>
		<?=form_error('frequent_headaches');?>
		<input type="text" name="frequent_headaches" value="<?=set_value('frequent_headaches',isset($sse)? $sse['hhc_frequent_headaches']:NULL);?>" required>
		<small class="error">This Field is required.</small>
	</div>
	<div>
		<label class="radius secondary label">STOMACH ACHES</label>
		<?=form_error('frequent_stomachaches');?>
		<input type="text" name="frequent_stomachaches" value="<?=set_value('frequent_stomachaches',isset($sse)? $sse['hhc_frequent_stomachaches']:NULL);?>" required>
		<small class="error">This Field is required.</small>
	</div>
	<div>
		<label class="radius secondary label">DIZZINESS</label>
		<?=form_error('frequent_dizziness');?>
		<input type="text" name="frequent_dizziness" value="<?=set_value('frequent_dizziness',isset($sse)? $sse['hhc_dizziness']:NULL);?>" required>
		<small class="error">This Field is required.</small>
	</div>
	<div>
		<label class="radius secondary label">VOMITING</label>
		<?=form_error('frequent_vommiting');?>
		<input type="text" name="frequent_vommiting" value="<?=set_value('frequent_vommiting',isset($sse)? $sse['hhc_vomitting']:NULL);?>" required>
		<small class="error">This Field is required.</small>
	</div>

	<div>
		<label class="radius secondary label">Has your child had any major injury in the past? (eg. limbs,joints,head,spine) if yes,specify</label>
		<?=form_error('major_injury');?>
		<textarea name="major_injury"><?=set_value('major_injury',isset($sse)? $sse['hhc_major_injury_specify']:NULL);?></textarea>
	</div>
	<div>
		<label class="radius secondary label">Has your child had any major operations? if yes specify.</label>
		<?=form_error('major_operations');?>
		<textarea name="major_operations"><?=set_value('major_operations',isset($sse)? $sse['hhc_major_operations_specify']:NULL);?></textarea>
	</div>
	<div>
		<label class="radius secondary label">Has your child been diagnosed with any major ailment (eg. heart,epilepsy,asthma) if Yes specify</label>
		<?=form_error('major_ailment');?>
		<textarea name="major_ailment"><?=set_value('major_ailment',isset($sse)? $sse['hhc_diagnosed_specify']:NULL);?></textarea>
	</div>
	<div>
		<label class="radius secondary label">What medication/remedy is your child taking?</label>
		<?=form_error('medication_child_taking');?>
		<input type="text" name="medication_child_taking" value="<?=set_value('medication_child_taking',isset($sse)? $sse['hhc_medication']:NULL);?>" required>
		<small class="error">This Field is required.</small>
	</div>
	<div>
		<label class="radius secondary label-good">If your child is suffering from any ailment?, please provide precautionary measures to take.</label>
		<?=form_error('medication_prec');?>
		<input type="text" name="medication_prec" value="<?=set_value('medication_prec',isset($sse)? $sse['ailment_prec']:NULL);?>" required>
		<small class="error">This Field is required.</small>
	</div>
	<div>
		<label class="radius secondary label">Is your child allergic to penicillin? aspirin? other medication (specify)?</label>
		<?=form_error('medication_allergy');?>
		<input type="text" name="medication_allergy" value="<?=set_value('medication_allergy',isset($sse)? $sse['hhc_alergic_med']:NULL);?>" required>
		<small class="error">This Field is required.</small>
	</div>
	<div>
		<label class="radius secondary label">What is the general eating habit of the child?</label>
		<?=form_error('child_eating_habbit');?>
		<input type="text" name="child_eating_habbit" value="<?=set_value('child_eating_habbit',isset($sse)? $sse['hhc_general_eating']:NULL);?>" required>
		<small class="error">This Field is required.</small>
	</div>
	<div>
		<label class="radius secondary label">Are there any DIETARY RESTRICTIONS or FOOD ALLERGIES? if yes Specify</label>
		<?=form_error('dietary_restrictions');?>
		<input type="text" name="dietary_restrictions" value="<?=set_value('dietary_restrictions',isset($sse)? $sse['hhc_food_restrictions']:NULL);?>" required>
		<small class="error">This Field is required.</small>
	</div>
	<div>
		<label class="radius secondary label">How do you want the school to help in his/her eating habit?</label>
		<?=form_error('school_help_eating_habbit');?>
		<input type="text" name="school_help_eating_habbit" value="<?=set_value('school_help_eating_habbit',isset($sse)? $sse['hhc_school_help_eating']:NULL);?>" required>
		<small class="error">This Field is required.</small>
	</div>
	
	<hr class="clearfix">
	<legend class="label" style="margin:0 auto;">Discipline</legend>
	
	<div>
		<label class="radius secondary label">Methods of discipline used at home (please be specific)</label>
		<?=form_error('methods_of_discp');?>
		<textarea name="methods_of_discp" required><?=set_value('methods_of_discp',isset($sse)? $sse['d_method_of_discipline']:NULL);?></textarea>
		<small class="error">This Field is required.</small>
	</div>
	<div>
		<label class="radius secondary label">Who handles the discipline at home?</label>
		<?=form_error('who_handles_discp_home');?>
		<input type="text" name="who_handles_discp_home" value="<?=set_value('who_handles_discp_home',isset($sse)? $sse['d_handles_discipline']:NULL);?>" required>
		<small class="error">This Field is required.</small>
	</div>
	<div>
		<label class="radius secondary label">How effective is the discipline on your child?</label>
		<?=form_error('how_effective_discp');?>
		<input type="text" name="how_effective_discp" value="<?=set_value('how_effective_discp',isset($sse)? $sse['d_discipline_effective']:NULL);?>" required>
		<small class="error">This Field is required.</small>
	</div>
	<div>
		<label class="radius secondary label">Do you believe in the use of the rod as a form of discipline? Why?</label>
		<?=form_error('rod_discp');?>
		<input type="text" name="rod_discp" value="<?=set_value('rod_discp',isset($sse)? $sse['d_discipline_rod_why']:NULL);?>">
	</div>
	<div>
		<label class="radius secondary label">Will you allow the TEACHER to use the rod in school ONLY WHEN NECESSARY?</label>
		<?=form_error('allow_teacher_use_rod');?>
		<input type="text" name="allow_teacher_use_rod" value="<?=set_value('allow_teacher_use_rod',isset($sse)? $sse['d_teacher_use_rod_why']:NULL);?>" required>
		<small class="error">This Field is required.</small>
	</div>
	<div>
		<label class="radius secondary label">If Not What form of discipline do you suggest we use on your child?</label>
		<?=form_error('what_form_of_discp_use');?>
		<input type="text" name="what_form_of_discp_use" value="<?=set_value('what_form_of_discp_use',isset($sse)? $sse['d_suggest_discipline']:NULL);?>" required>
		<small class="error">This Field is required.</small>
	</div>
	<div>
		<label class="radius secondary label">How can the school be of help in supporting your mode of discipline?</label>
		<?=form_error('school_supp_discp');?>
		<input type="text" name="school_supp_discp" value="<?=set_value('school_supp_discp',isset($sse)? $sse['d_school_help_discipline']:NULL);?>" required>
		<small class="error">This Field is required.</small>
	</div>
	<div>
		<label class="radius label-good">What other information can you provide us regarding your child's behavior, personality or development that you believe would aid us in dealing or enhancing his development?</label>
		<?=form_error('other_info_about_child');?>
		<textarea name="other_info_about_child" required><?=set_value('other_info_about_child',isset($sse)? $sse['d_provide_information']:NULL);?></textarea>
		<small class="error">This Field is required.</small>
	</div>
	<hr style="border:2px solid #2BA6CB;">
	<div class="row ">
		<div class="large-6 columns">
			<label class="label secondary">Physician</label>
			<input type="text" name="medication_physc_nme" value="<?=set_value('medication_physc_nme',isset($sse)? $sse['phsc_name']:NULL);?>" required>
			<?=form_error('medication_physc_nme');?>
			<small class="error">This Field is required.</small>
		</div>
		<div class="large-3 columns">
			<label class="label secondary">Telephone No</label>
			<input type="text" name="medication_physc_con" value="<?=set_value('medication_physc_con',isset($sse)? $sse['phsc_cont']:NULL);?>" required>
			<?=form_error('medication_physc_con');?>
			<small class="error">This Field is required.</small>
		</div>
		<div class="large-3 columns">
			<label class="label secondary">Blood Type</label>
			<input type="text" name="medication_bt" value="<?=set_value('medication_bt',isset($sse)? $sse['c_blood_type']:NULL);?>" required>
			<?=form_error('medication_bt');?>
			<small class="error">This Field is required.</small>
		</div>
	</div>
	<div class="row ">
		<div class="large-8 columns">
			<label class="label secondary">Persons authorized to pick up child</label>
			<span class="small-info">Please provide, relationship to the person.</span>
			<?=form_error('auth_pickup_bt');?>
			<textarea name="auth_pickup_bt" required><?=set_value('auth_pickup_bt',isset($sse)? $sse['auth_pu_ppl']:NULL);?></textarea>
			<small class="error">This Field is required.</small>
		</div>
		<div class="large-4 columns">
		</div>
	</div>
	<div class="clearfix"></div>

	<div class="form-values">
		<input type="hidden" name="sdf_loc" value="<?=$token;?>">
		<input type="submit" name="fillup_profile" id="submit_fillup_profile" value="Finish Enrollment" class="btn btn-success">
		<input type="submit" name="submit_save_later" id="save-enrollment" value="Save and Continue Later" class="btn btn-primary">
	</div>
<?php echo form_close(); ?>

</div>