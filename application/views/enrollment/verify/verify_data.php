<div class="verify">
	<div class="wrapper">
		<img src="<?=base_url('assets/css/custom-images/dialog_question.png');?>">
		<div class="content">
		<table class="data-table">
			<?if(!empty($verify_profile_data)):?>
				<tbody class="tabular">
						<tr><td>Firstname:</td><td><?php echo $verify_profile_data['firstname'];?></td></tr>
						<tr><td>Lastname</td><td><?php echo $verify_profile_data['lastname'];?></td></tr>
						<tr><td>Middlename</td><td><?php echo $verify_profile_data['middlename'];?></td></tr>
						<tr><td>gender</td><td><?php echo $verify_profile_data['gender'];?></td></tr>
						<tr><td>Birthdate</td><td><?php echo $verify_profile_data['dob'];?></td></tr>
						<tr><td>Birthplace</td><td><?php echo $verify_profile_data['pob'];?></td></tr>
						<tr><td>Age</td><td><?php echo $verify_profile_data['age'];?></td></tr>
						<tr><td>Present Add.</td><td><?php echo $verify_profile_data['present_address'];?></td></tr>
						<tr><td>Father's Name</td><td><?php echo $verify_profile_data['fathername'];?></td></tr>
						<tr><td>Contact #</td><td><?php echo $verify_profile_data['father_contact_no'];?></td></tr>
						<tr><td>Mother's Name</td><td><?php echo $verify_profile_data['mothername'];?></td></tr>
						<tr><td>Contact #</td><td><?php echo $verify_profile_data['mother_contact_no'];?></td></tr>
						<tr><td>Parent's Add</td><td><?php echo $verify_profile_data['parent_address'];?></td></tr>
						<tr><td>Last School Att.</td><td><?php echo $verify_profile_data['last_school_name'];?></td></tr>
						<tr><td>School Add.</td><td><?php echo $verify_profile_data['last_school_address'];?></td></tr>
						<tr><td>Level</td><td><?php echo $verify_profile_data['last_school_level'];?></td></tr>
						<tr><td>School Year</td><td><?php echo $verify_profile_data['last_school_year'];?></td></tr>
				</tbody>
			<?endif;?>
		</table>
		<div class="menu">
			<?$formAtrrib = array('class'=>'verify-form');?>
			<?echo form_open('enrollment/input_data',$formAtrrib);?>
				<input type="hidden" name="firstname" value="<?php echo $verify_profile_data['firstname'];?>">
				<input type="hidden" name="lastname" value="<?php echo $verify_profile_data['lastname'];?>">
				<input type="hidden" name="middlename" value="<?php echo $verify_profile_data['middlename'];?>">
				<input type="hidden" name="gender" value="<?php echo $verify_profile_data['gender'];?>">
				<input type="hidden" name="dob" value="<?php echo $verify_profile_data['dob'];?>">
				<input type="hidden" name="pob" value="<?php echo $verify_profile_data['pob'];?>">
				<input type="hidden" name="age" value="<?php echo $verify_profile_data['age'];?>">
				<input type="hidden" name="present_address" value="<?php echo $verify_profile_data['present_address'];?>">
				<input type="hidden" name="fathername" value="<?php echo $verify_profile_data['fathername'];?>">
				<input type="hidden" name="father_contact_no" value="<?php echo $verify_profile_data['father_contact_no'];?>">
				<input type="hidden" name="mothername" value="<?php echo $verify_profile_data['mothername'];?>">
				<input type="hidden" name="mother_contact_no" value="<?php echo $verify_profile_data['mother_contact_no'];?>">
				<input type="hidden" name="parent_address" value="<?php echo $verify_profile_data['parent_address'];?>">
				<input type="hidden" name="last_school_name" value="<?php echo $verify_profile_data['last_school_name'];?>">
				<input type="hidden" name="last_school_address" value="<?php echo $verify_profile_data['last_school_address'];?>">
				<input type="hidden" name="last_school_level" value="<?php echo $verify_profile_data['last_school_level'];?>">
				<input type="hidden" name="last_school_year" value="<?php echo $verify_profile_data['last_school_year'];?>">
				<input type="hidden" name="level_id" value="<?php echo $verify_profile_data['level_id'];?>">
				<input type="hidden" name="idno" value="<?php echo $verify_profile_data['idno'];?>">
				<input type="hidden" name="student_type" value="new">
				<input type="submit" name="check" value="Continue Enroll">
			<?echo form_close()?>
			
		</div>
		
		  <a href="javascript:history.go(-1)" class="add">GO BACK</a>
		</div>
	</div>
	
</div>
