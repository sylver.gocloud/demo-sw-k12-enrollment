<?							
if(isset($page)){ $body_class = 'style="overflow:hidden"';}	
echo doctype();
?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width" />
    <title>school system</title>
	
		<!-- start css files for Grocery CRUD-->
	<?php if(!empty($output->css_files)):?>
		<?php  foreach($output->css_files as $file): ?>
			<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
		<?php endforeach; ?>
	<?php endif;?>
	<!-- end css files for Grocery CRUD-->
	
	<link rel="stylesheet" href="<?=style_url('normalize');?>">
	<link rel="stylesheet" href="<?=style_url('foundation.min');?>">
	<link rel="stylesheet" href="<?=style_url('bootstrap.min');?>">
	<link rel="stylesheet" href="<?=style_url('style');?>">
	<link rel="stylesheet" href="<?=style_url('jquery.smoothness.ui');?>">
	
   <script src="<?php echo script_url('custom/jquery1.9.1'); ?>">
	<script>window.jQuery || document.write('<script src="<?php echo script_url('vendor/jquery'); ?>"><\/script>')</script>
    <script type="text/javascript" src="<?php echo script_url('vendor/custom.modernizr');?>"></script>
    </head>
	

    <body>
        <!-- message dialog box -->
        <!-- start contents -->
		<div>
			<?$this->load->view('layouts/_application_top_nav');?>
		</div>
		
		<div class="row">

			<noscript>
				<div class="error">Javascript has been disabled on your browser.some Functions may not work</div>
			</noscript>
			<!--User Menus-->
				<div class="small-3 columns  content-menus">
						<?$this->load->view('layouts/_student_menu')?>
					<!--start Echo the page-->
				</div>
				<div class="small-9 columns content-views">
						<?php echo $yield;?>
					<!--end echo the page-->
				</div>
		</div>
		<div id="msgDialog"><p></p></div>
		<footer>
			<div class="row footer-contents">
				copyright &copy; gocloudasia 2013
			</div>
		</footer>
		<!-- End contents -->
		<script>
		  document.write('<script src=' +
		  ('__proto__' in {} ? '<?=script_url("vendor/zepto");?>' : '<?=script_url("vendor/jquery");?>') +
		  '><\/script>')
		</script>
		<script type="text/javascript" src="<?php echo script_url('custom/jquery-ui-1.10.2');?>"></script>
		<script>window.jQuery.ui || document.write('<script src="<?php echo script_url('vendor/jquery.ui'); ?>"><\/script>')</script>
		<script type="text/javascript" src="<?php echo script_url('foundation.min');?>"></script>
		<script type="text/javascript" src="<?php echo script_url('vendor/timpicker.addon');?>"></script>
		<script type="text/javascript" src="<?php echo script_url('custom/myjs'); ?>"></script>
		<script type="text/javascript" src="<?php echo script_url('custom/calendarscript'); ?>"></script>	
		<script type="text/javascript" src="<?php echo script_url('custom/cash'); ?>"></script>
		<script src="<?=script_url('foundation/foundation');?>"></script>
		<script src="<?=script_url('foundation/foundation.alerts');?>"></script>
		<script src="<?=script_url('foundation/foundation.dropdown');?>"></script>
		<script src="<?=script_url('foundation/foundation.forms');?>"></script>
		<script src="<?=script_url('foundation/foundation.reveal');?>"></script>
		<script src="<?=script_url('foundation/foundation.tooltips');?>"></script>
		<script src="<?=script_url('foundation/foundation.topbar');?>"></script>
		<script>
		  $(document).foundation();
		</script>
		<script type="text/javascript" src="<?php echo script_url('vendor/bootstrap.min'); ?>"></script>
	</body>
</html>