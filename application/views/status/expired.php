<div class="row">
	<div class="large-12 small-12 columns">
		<h2 class="label alert"><i class="icon-warning-sign"></i>&nbsp;&nbsp;Verification Link Expired.</h2>
		<p>Sorry, for the inconvinience. The Verification link you have given to us is already EXPIRED.</p>
		<p>To validate student <strong><?=ucwords($name);?></strong>'s enrollment, we ask you to please generate a new confirmation link. This verification Link will be sent back to your email address. Please click the link below.</p>
		<div style="margin:10px 0;text-align:center;"><a href="<?=site_url('confirm/resend');?>" class="btn btn-success">Resend Confirmation Link</a></div>
	</div>
	<div class="large-12 small-12 columns">
		<div style="border:3px solid #c0c0c0;padding:10px;">
			<p style="font:15px bold;"><i class="icon-warning-sign"></i>&nbsp;&nbsp;NOTE</p>
			<p style="font:14px bold;">What to do if no message received?</p>
			<ul style="font:12px bold;">
				<li><i class="icon-chevron-right"></i>&nbsp;&nbsp;Please Check your spam folder.</li>
				<li><i class="icon-chevron-right"></i>&nbsp;&nbsp;Check for other Tabs.</li>
				<li><i class="icon-chevron-right"></i>&nbsp;&nbsp;(If email forwarding is enabled on your email) Please Check the email address it was forwarded to.</li>
			</ul>
		</div>
	</div>
</div>