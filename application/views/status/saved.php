<div class="panel" style="border:1px solid #000;">
	<div class="row" style="margin-top:50px;">
		<div class="large-12 columns">
			<h1 class="alert-box success">Your Data has been saved. </h1>
			<p>A Security key has been sent to your email.</p>
			<p>To resume Enrollment please follow the instructions on your email.</p>
			<p>Thank You.</p>
		</div>
	</div>
</div>

<div class="row">
	<div class="large-12 columns">
		<div class="panel">
			<div class="row">
				<div class="large-2 columns">
					<a href="<?=site_url('enrollment/quitenrollment');?>" class="btn btn-warning" style="width:150px;">
						<i class="icon-remove"></i>
						<strong>Exit</strong>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>