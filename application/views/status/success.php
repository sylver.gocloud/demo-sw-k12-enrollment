<?$mode =trim($system_message);?>
<div class="row">
	<div class="large-12 columns">
		<div class="panel">
			<p><strong>Would you like to enroll another child?</strong></p>
			<div class="row">
				<div class="large-2 columns">
					<a href="<?=site_url('enrollment/enrollagain/'.$link);?>" class="btn btn-success" style="width:150px;">
					<i class="icon-user"></i>
					<strong>Yes Enroll another Child.</strong></a>
				</div>
				<div class="large-10 columns">
					<span class="alert-box">Please Choose this if you plan to enroll again. All basic fields are filled up from previous enrollment.</span>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="large-2 columns">
					<a href="<?=site_url('enrollment/quitenrollment');?>" class="btn btn-warning" style="width:150px;">
						<i class="icon-remove"></i>
						<strong>No, Finish enrollment.</strong>
					</a>
				</div>
				<div class="large-10 columns">
					<span class="alert-box">Please choose this if you are finished with the enrollment. This will remove all saved data.</span>
				</div>
			</div>
		</div>
	</div>
	<div class="large-12 columns">
		<div  class="panel">
		<?php if($message) :?>
			<?=html_entity_decode($message->message,ENT_COMPAT);?>
		<?php endif; ?>
		</div>
	</div>
</div>