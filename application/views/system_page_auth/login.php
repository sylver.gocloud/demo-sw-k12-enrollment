<div id="login-overlay" class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel"><img src="<?=site_url('assets/img/logo.png')?>" width="60" height="30"> School System Enrollment Demo</h4>
      </div>
      <div class="modal-body">
      		<div class="row"><?=isset($system_message)?$system_message:""?></div>
          <div class="row">
              <div class="col-xs-6">
                  <div class="well">
                      <?=form_open('')?>
                          <div class="form-group">
                              <label for="username" class="control-label">Username</label>
                              <input type="text" class="form-control" id="username" name="username" value="<?=set_value('username')?>"  title="Please enter you username" placeholder="Username" autocomplete="off" required>
                              <span class="text-danger"><?=form_error('username')?></span>
                          </div>
                          <div class="form-group">
                              <label for="password" class="control-label">Password</label>
                              <input type="password" class="form-control" id="password" name="password" value="<?=set_value('password')?>"  title="Please enter your password" placeholder="Password" autocomplete="off" required>
                              <span class="text-danger"><?=form_error('password')?></span>
                          </div>
                          <div id="loginErrorMsg" class="alert alert-error hide">Wrong username og password</div>
                          <div class="form-group">
                              <label for="captcha" class="control-label">Captcha(Enter Words Below)</label>
                              <input type="text" class="form-control" id="captcha" name="captcha" value=""  placeholder="captcha" autocomplete="off" required>
                              <span class="text-danger"><?=form_error('captcha')?></span>
                          </div>
                          <div class="form-group">
                              <div>
                              	<?=$captcha_image;?>
																<input type="hidden" name="fit" value="<?=$form_token;?>"/>
                              </div>
                              <span class="help-block"></span>
                          </div>
                          
                          <button type="submit" name="login_me" value="login_me" class="btn btn-success btn-block">Login</button>
                          <!-- <a href="/forgot/" class="btn btn-default btn-block">Help to login</a> -->
                      </form>
                  </div>
              </div>
              <div class="col-xs-6">
                  <p class="lead">Try our demo now for <span class="text-success">FREE</span></p>
                  <ul class="list-unstyled" style="line-height: 2">
                      <li><span class="fa fa-check text-success"></span> Experience using our actual demo system.</li>
                      <li class="divider"><hr></li>
                      <li><span class="fa fa-check text-success"></span> Your demo username and password will be sent directly to your email by request. If you haven't received your username and password yet, feel free to send us a request by sending us an email to <code>solutions@gocloud.asia</code>.</li>
                  </ul>
              </div>
          </div>
      </div>
  </div>
</div>

<?/** LAYOUT REF : http://bootsnipp.com/snippets/featured/login-form-in-modal */?>