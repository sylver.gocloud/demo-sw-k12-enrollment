$(document).ready(function(){
	
	//higlight a selected cell in a calendar
	$(".this_day").click(function(){
		$(".this_day").removeClass("selected");
		$(this).addClass("selected");
	});
	
	//adds data in database.
	$("#button").click(function()
	{
		var selected = $(".selected .day_num").text(); // gets the numerical "day" in the calendar.
		var event = $("#new_event").val(); // gets the event. input by user
		if((selected == '') || (event == '')){ //checks if "day" or event is empty and performs proper window alert.
			if(selected == ''){
				alert("Choose proper day.");
				$(".this_day").removeClass("selected");
				exit;
			}
			if(event == ''){
				alert("Enter event.");
			}
		}else{
			$.ajax({ // if data is OK, ajax is performed.
				url: window.location, // data passed to same page.
				type: "POST",
				data:{ 
					day		: selected,
					event	: event
				},
				success: function(msg){
					alert(selected + " " + event);
					location.reload(); // if successful, page is reloaded
				}
			});
		}
	});
	
	//deletes data from database.
	$("#delete").click(function()
	{
		var selected = $(".selected .day_num").text(); // gets the numerical "day" in the calendar.
		if(selected == ''){
			alert("No date selected.");
		}else{
			var f = confirm("Are you sure?");
			if(f == true){
				$.ajax({
					url:window.location, // data passed to same page.
					type:"POST",
					data:{ 
						flag	: f,
						a_day	: selected
					},
					success: function(msg){
						location.reload(); // if successful, page is reloaded
					}
				});
			}
		}
	});
	
	$('#birthdate').datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		onSelect: function(dateText, inst) 
		{  
			$(this).val(dateText);
			alert($(this).val());
		}
		
	});
	
	// datepicker for attendance
	$('#datepicker').datepicker({
		dateFormat: 'MM dd, yy',
		changeMonth: true,
		showButtonPanel: true,
		onSelect: function(dateText, inst) 
		{  
			$(this).val(dateText);
		}
	});
	
	// function for deleting attendance in m.k12/teacher/attendance
	$(".confirm_btns").on("click", function(e){
		var link = this;
		
		e.preventDefault();
		
		$("<div title='Delete'>Are you sure you want to continue?</div>").dialog({
			buttons: {
				"OK" : function(){
					window.location = link.href;
				},
				"Cancel" : function(){
					$(this).dialog("close");
				}
			}
		});
	});
	
	//for viewing subjects of teachers
	$(function(){
		$("#subj_accordion").accordion({
			collapsible: true,
			icons: false,
			heightStyle: "content"
		});
	});
	
	//for submitting remarks in report card
	$("input.remarks_in").blur(function(){
		$("form.remarks_form").submit();
	});
	
	//for username generation in hrd or admin (employee_settings/change_settings)
	var charArray = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
	$('#gen_uname').click(function(){
		var rand_uname = "";
		while(rand_uname.length != 10)
		{
			rand_uname += charArray.charAt(Math.floor(Math.random()*62)+1);
		}
		$("<div title='Generated Username'>Use " + rand_uname + " as username?</div>").dialog({
			resizable: false,
			buttons: {
				"OK" : function(){
					$('#username').val(rand_uname);
					$(this).dialog("close");
				},
				"Cancel" : function(){
					$(this).dialog("close");
				}
			}
		});
	});
	
	//for password generation in hrd or admin (employee_settings/change_settings)
	$('#gen_pword').click(function(){
		var rand_pword = "";
		while(rand_pword.length != 7)
		{
			rand_pword += charArray.charAt(Math.floor(Math.random()*62)+1);
		}
		$("<div title='Generated Password'>Use " + rand_pword + " as password?</div>").dialog({
			resizable: false,
			buttons: {
				"OK" : function(){
					$('#password').val(rand_pword);
					$(this).dialog("close");
				},
				"Cancel" : function(){
					$(this).dialog("close");
				}
			}
		});
	});
	
	$('#submit_changes').click(function(){
		if($('#username').val() == '' && $('#password').val() == ''){
			$("<div title='No entry found'>Please enter at least one entry</div>").dialog({
				resizable: false,
				modal: true,
				buttons: {
					"OK" : function(){
						$(this).dialog("close");	
					}
				}
			});
		}else
		{
			$("<div title='Settings Changed'><p>Are you sure?</p><br /><p>Note: Please give changes to employee concerned IMMEDIATELY.</p></div>").dialog({
				resizable: false,
				modal: true,
				buttons: {
					"Yes" : function(){
						$('#settings_form').submit();
						$(this).dialog("close");
					},
					"Cancel" : function(){
						$(this).dialog("close");
					}
				}
			});
		}
	});
	
	$('.submit_btns').click(function(){
		$("<div title='Submit'>Are you sure?</div>").dialog({
			resizable: false,
			modal: true,
			buttons: {
				"Yes" : function(){
					$(".submit_forms").submit();
					$(this).dialog("close");
				},
				"Cancel" : function(){
					$(this).dialog("close");
				}
			}
		});
	});
});


