$(document).ready(function(){
	$('input#payment_input').focus(function(){
		var amount = $(this).attr('original_fee_value');
		var number = $(this).attr('fee_number');
		$(this).keyup(function(){
			var value = $(this).val();
			$('input#amount_balance_'+number).val(numberWithCommas((+amount - +value).toFixed(2)));
			$('input#amount_balance_hidden'+number).val(+amount - +value);
		});
	});
	
	
	$('input.breakdown_sum').blur(function(){
		var sum = 0;
		var remaining;
		var max = $('input#payment_recieved').attr('rate');
		$('input.breakdown_sum').each(function() {
			sum += Number($(this).val());
		remaining = +max - +sum;	
			$('input#breakdown_total').val(numberWithCommas(sum.toFixed(2)));
			$('input#breakdown_remaining').val(numberWithCommas(remaining.toFixed(2)));
			
			if(sum > max)
			{
				$('div#message').html('<div class="error">Warning total Payment Breakdown is greater than recieved Payment</div>');
			}else{
				$('div#message').html('');
			}
	
		});
	
	});

});

function numberWithCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}