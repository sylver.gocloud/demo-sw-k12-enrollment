$(document).ready(function(){
	$('#level-section').change(function(){

		var url = $(this).attr('url');
		var opt = $(this).val();		
		$.get( url, { id: opt } ).done(function( data ){
			 $('.dynamictable').html(data);
	    });
	});
});